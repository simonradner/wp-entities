<?php
namespace Sef\WpEntities\Annotation;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @Annotation
 */
class TermOptions extends Options{

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(

      'wpEntityConstructor'       => 'Sef\\WpEntities\\Components\\WpEntity\\Constructor\\TermConstructorByName', // only class
      'wpNamingStrategy'       => null, // only class
      'taxonomy'  => 'category', // only class
      'entity'  => null,
      'repository'  => null,

      'type'    => null,
      'foo'    => null,
      'fetcher'   => null,
      'saver'   => null,
      'getterConverter'   => null,
      'setterConverter'   => null,
      'wpname'   => null,


    ));

    $resolver->setAllowedValues('type', [
      null,
      'term',
      'meta',
      'entity',
      'parent'
    ]);
  }

  public function hasType()
  {
    $options = $this->options;
    if(null === $which)
    {
      return $this->options['type'];
    }
    return ($which == $this->options['type']);
  }

  public function getType()
  {
    return $this->hasType();
  }

  public function isMeta()
  {
    return ($this->getType() == 'meta');
  }

  public function isTerm()
  {
    return ($this->getType() == 'term');
  }
}
