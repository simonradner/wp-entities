<?php
namespace Sef\WpEntities\Annotation;       
use Symfony\Component\OptionsResolver\OptionsResolver;


class EntityConfigOptions extends Options {

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'entityManager'    => \Sef\WpEntities\WpEntities::getInstance()->entityManager,
      'validator'    => \Sef\WpEntities\WpEntities::getInstance()->validator,
      'optionClass'       => 'Sef\\WpEntities\\Annotation\\PostOptions',
      'annotationReader'  => \Sef\WpEntities\WpEntities::getInstance()->annotationReader,
      'propertyOptions' => [],
      'classOptions'    => [],
      'foo'   => 'barInOptionclass'
    ));
    $resolver->setRequired([
      'entityManager',
      'annotationReader'
    ]);

    $resolver->setAllowedTypes('entityManager', 'Sef\WpEntities\EntityManager' );
    $resolver->setAllowedTypes('optionClass', 'string' );
    $resolver->setAllowedTypes('annotationReader', 'Doctrine\Common\Annotations\Reader' ); // Interface: Doctrine\Common\Annotations\Reader
    $resolver->setAllowedTypes('classOptions', 'array' );
    $resolver->setAllowedTypes('propertyOptions', 'array' );
  }
}