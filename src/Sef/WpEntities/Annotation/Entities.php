<?php
namespace Sef\WpEntities\Annotation;

/**
 * @Annotation
 */
class Entities extends Options {

  protected $options = [];

  public function __construct($options)
  {
    if(isset($options['value'])) {  // the annotation args are given as array without a key
      $this->options = $options['value'];
    } else {
      $this->options = $options;
    }
  }

  public function get($key = null)
  {
    if ( $key && array_key_exists( $key, $this->options)) {
      return $this->options[$key];
    }
    return $this->options;
  }

  public function getFirst()
  {
    $options = $this->options;
    reset($options);
    $firstKey = key($options);
    return $this->get($firstKey);
  }

  public function merge(array $arrayOfOptions = [])
  {
    $arrayToResolve = call_user_func_array('array_replace', $arrayOfOptions );
    $this->options = array_replace($this->options, $arrayOfOptions);
  }

  public function mergeRecusrsive(array $arrayOfOptions = [])
  {
    $arrayToResolve = call_user_func_array('array_replace_recursive', $arrayOfOptions );
    $this->options = array_replace_recursive($this->options, $arrayToResolve);
  }

  public function hasType($type)
  {
    return (array_key_exists($type, $this->options )) ? $this->options[$type] : false;
  }

  public function hasTaxonomy($taxonomy)
  {
    return $this->hasType($taxonomy);
  }

  public function hasPosttype($posttype)
  {
    return $this->hasType($posttype);
  }

  public function getTypes()
  {
    return array_keys($this->options);
  }

  public function getPosttypes()
  {
    return $this->getTypes();
  }

  public function getTaxonomies()
  {
    return $this->getTypes();
  }
}
