<?php
namespace Sef\WpEntities\Annotation;
use Symfony\Component\OptionsResolver\OptionsResolver;


// http://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/

/**
 * @Annotation
 */
class SimpleEntityOptions extends PostOptions {

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'wpNamingStrategy'       => null, // only class

      'type'    => null,
      'repository'  => null,
      'entity'      => null,
      'childentity' => null,
      'format'    => null,
      'fetcher'   => null,
      'saver'   => null,
      'getterConverter'   => null,
      'setterConverter'   => null,
      'size'     => [],   // imagesizes
      'wpname'   => null,   // for example the meta id in wordpress
      'foo'    => null,
      'importerStrategy'  => null, // class
    ));
    $resolver->setAllowedValues('type', [
      null,
      'meta',
      'entity',
      'entities',
      'attachmenturl',
      'imagesizes',
      'imagesize',
      'parent',

      // depricated
      'parentpost', 'term' , 'post', 'terms', 'posts'
    ]);
  }
}
