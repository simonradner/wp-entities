<?php
namespace Sef\WpEntities\Annotation;       
use Symfony\Component\OptionsResolver\OptionsResolver;


class RepositoryConfigOptions extends Options {

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults([
      'entityManager'    => \Sef\WpEntities\WpEntities::getInstance()->entityManager,
      'optionClass'       => 'Sef\\WpEntities\\Annotation\\RepositoryOptions',
      'annotationEntityClass'    => 'Sef\\WpEntities\\Annotation\\Entities',
      'annotationReader'  => \Sef\WpEntities\WpEntities::getInstance()->annotationReader,
      'classOptions'    => [],
      'queryDefaults'   => [],
      'entities'   => [],
      'foo'   => 'barInOptionclass'
    ]);
    $resolver->setRequired([
      'entityManager',
      'optionClass',
      'annotationReader',
      'annotationEntityClass',
      'entities'
    ]);

    $resolver->setAllowedTypes('entityManager', 'Sef\WpEntities\EntityManager' );
    $resolver->setAllowedTypes('optionClass', 'string' );
    $resolver->setAllowedTypes('annotationEntityClass', 'string' );
    $resolver->setAllowedTypes('annotationReader', 'Doctrine\Common\Annotations\Reader' ); // Interface: Doctrine\Common\Annotations\Reader
    $resolver->setAllowedTypes('classOptions', 'array' );
    $resolver->setAllowedTypes('queryDefaults', 'array' ); 
    $resolver->setAllowedTypes('entities', 'array' ); 
    
  }
}