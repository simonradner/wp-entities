<?php
namespace Sef\WpEntities\Annotation;       

/**
 * @Annotation
 */
class Groups {
  
  protected $groups = [];

  public function __construct( $groups = [] ) 
  {     
    if( ! isset($groups['value']))
      return;
    
    if ( is_array($groups['value']))
      $this->groups = $groups['value'];

    if ( is_string($groups['value']) )
      $this->groups = [$groups['value']];
  }
  
  public function get()
  {
    return $this->groups;
  }
}