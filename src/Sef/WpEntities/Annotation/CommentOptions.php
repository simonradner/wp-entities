<?php
namespace Sef\WpEntities\Annotation;
use Symfony\Component\OptionsResolver\OptionsResolver;


// http://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/

/**
 * @Annotation
 */
class CommentOptions extends Options {

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(

      'wpEntityConstructor'       => 'Sef\\WpEntities\\Components\\WpEntity\\Constructor\\CommentConstructor', // only class
      'wpNamingStrategy'       => null, // only class

      'type'    => null,
      'repository'  => null,
      'entity'      => null,
      'fetcher'   => null,
      'saver'   => null,
      'getterConverter'   => null,
      'setterConverter'   => null,
      'wpname'   => null,   // for example the meta id in wordpress
      'foo'    => null,
      'importerStrategy' => null,  // simpleEntity
    ));
    $resolver->setAllowedValues('type', [
      null,
      'meta',
      'entity',
      'entities',
      'parent',

    ]);
  }

  public function hasType($which = null )
  {
    $options = $this->options;
    if(null === $which)
    {
      return $this->options['type'];
    }
    return ($which == $this->options['type']);
  }

  public function getType()
  {
    return $this->hasType();
  }

  public function isMeta()
  {
    return ($this->getType() == 'meta');
  }

  public function isEntity()
  {
    return ($this->getType() == 'entity');
  }

  public function isEntities()
  {
    return ($this->getType() == 'entities');
  }

  public function hasFetcher()
  {
    return (null !== $this->get('fetcher'));
  }

  public function hasSaver()
  {
    return (null !== $this->get('saver'));
  }

}
