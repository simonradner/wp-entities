<?php
namespace Sef\WpEntities\Annotation;
use Sef\WpEntities\Interfaces\AnnotationOptionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @Annotation
 */
abstract class Options implements AnnotationOptionInterface {

  protected $options = [];

  public function __construct($options=[])
  {
    $resolver = new OptionsResolver();
    $this->configureOptions($resolver);
    $this->options = $resolver->resolve($options);
  }

  public function merge(array $arrayOfOptions = [])
  {
    $resolver = new OptionsResolver();
    $this->configureOptions($resolver);
    $arrayToResolve = call_user_func_array('array_replace', $arrayOfOptions );
    $this->options = $resolver->resolve(array_replace($this->options, $arrayOfOptions));
  }

  public function mergeRecusrsive(array $arrayOfOptions = [])
  {
    $resolver = new OptionsResolver();
    $this->configureOptions($resolver);
    $arrayToResolve = call_user_func_array('array_replace_recursive', $arrayOfOptions );
    $this->options = $resolver->resolve(array_replace_recursive($this->options, $arrayToResolve));
  }

  public function configureOptions(OptionsResolver $resolver)
  {

  }

  public function set($key, $value )
  {
    $this->options[$key] = $value;
  }

  public function get($key = null)
  {
    if ( $key && array_key_exists( $key, $this->options)) {
      return $this->options[$key];
    }
    $options = array_filter($this->options, function($value) {
      return ( $value === NULL ) ? false : true;
    });

    return $options;
  }

  public static function make( $options = [] )
  {
    return new static($options);
  }

}