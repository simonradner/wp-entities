<?php
namespace Sef\WpEntities\Annotation;
use Symfony\Component\OptionsResolver\OptionsResolver;


// http://php-and-symfony.matthiasnoback.nl/2011/12/symfony2-doctrine-common-creating-powerful-annotations/

/**
 * @Annotation
 */
class PostOptions extends Options {

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(

      'wpEntityConstructor'       => 'Sef\\WpEntities\\Components\\WpEntity\\Constructor\\PostConstructor', // only class
      'wpNamingStrategy'       => null, // only class

      'type'    => null,
      'taxonomy'  => null,
      'taxonomy_format' => null,
      'repository'  => null,
      'entity'      => null,
      'childentity' => null,
      'post_type'   => 'post',   // only class
      'context'    => null,  /// depricated
      'filter'    => null,  // depricated
      'format'    => null,
      'fetcher'   => null,
      'saver'   => null,
      'getterConverter'   => null,
      'setterConverter'   => null,
      'size'     => [],   // imagesizes
      'wpname'   => null,   // for example the meta id in wordpress
      'foo'    => null,
      'mapping' => 'uni',
      'importerStrategy' => null,  // simpleEntity
    ));
    $resolver->setAllowedValues('taxonomy_format', [null, 'object', 'name', 'slug', 'id'] );
    $resolver->setAllowedValues('type', [
      null,
      'meta',
      'entity',
      'entities',
      'featuredimage',
      'attachmenturl',
      'imagesizes',
      'imagesize',
      'parent',

      // depricated
      'parentpost', 'term' , 'post', 'terms', 'posts'
    ]);
    $resolver->setAllowedValues('mapping', [
      'uni', // save as one meta fields (serialized array)
      'bi',  // save as multiple meta fields
      'inverse:one', // save one meta field with the foreign id on the inverse side
      'inverse:many'  // save multiple meta fields with the foreign id on the inverse side
    ]);
  }

  public function hasType($which = null )
  {
    $options = $this->options;
    if(null === $which)
    {
      return $this->options['type'];
    }
    return ($which == $this->options['type']);
  }

  public function getType()
  {
    return $this->hasType();
  }

  public function isMeta()
  {
    return ($this->getType() == 'meta');
  }

  public function isEntity()
  {
    return ($this->getType() == 'entity');
  }

  public function isEntities()
  {
    return ($this->getType() == 'entities');
  }

  public function hasFetcher()
  {
    return (null !== $this->get('fetcher'));
  }

  public function hasSaver()
  {
    return (null !== $this->get('saver'));
  }

  // DEPRICATED

  public function isTerm()
  {
    return ($this->getType() == 'term');
  }

  public function isTerms()
  {
    return ($this->getType() == 'terms');
  }

}
