<?php
namespace Sef\WpEntities\Entities;       
use Sef\WpEntities\Base\TermEntity;
use Sef\WpEntities\Annotation\TermOptions as Options;

 /**
   * @Options(
   *  taxonomy="category"
   * )
   */
class Category extends TermEntity {

  protected $id;

  protected $slug;

  protected $name;
 
  protected $taxonomy;
 
  protected $description;
  
  protected $termGroup;

  protected $termTaxonomyId;

  protected $parent;

  protected $count;

  protected $link;
  
}