<?php
namespace Sef\WpEntities\Entities\Util;       
use Sef\WpEntities\Interfaces\ImagesizesInterface;
use Sef\WpEntities\Interfaces\ImagesizeInterface;

class Imagesizes implements ImagesizesInterface {
  
  public $full;
  
  public $large;
  
  public $medium;
  
  public $thumbnail;  
  
  public function setUpImagesize( ImagesizeInterface $imagesize )
  {
    $sizename = $imagesize->size;
    if( property_exists( $this, $sizename))
    {
      $imagesize->setUp();
      $this->$sizename = $imagesize;
    }
  }
}