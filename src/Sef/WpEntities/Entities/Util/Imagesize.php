<?php
namespace Sef\WpEntities\Entities\Util;       
use Sef\WpEntities\Interfaces\ImagesizeInterface;

class Imagesize implements ImagesizeInterface {
  
  public $size;
  
  public $url;
  
  public $width;
  
  public $height;

  public $maxWidth;
  
  public $maxHeight;
    
  public $cropped = null;

  public $crop;

  public $exactSize;

  public $isIntermediate;  
  
  protected $attachmentId;
 
  public function __construct( $attachmentId = null, $size = 'full' )
  {
    if( is_numeric( $attachmentId ))
    {
      $this->setAttachmentId( $attachmentId );
      $this->setSize( $size );
      $this->setUp();      
    }
  }

  public function setAttachmentId( $attachmentId )
  {
    $this->attachmentId = $attachmentId;
  }
  public function setSize( $size ) 
  {
    $this->size = $size;
  }
    
  public function setUp()
  {
    $meta = $this->getMeta( $this->attachmentId, $this->size );
    if( $meta )
    {
      foreach ( $meta as $k =>$v )
      {
        $this->$k = $v;
      }
    }
  }
  
  protected function getMeta( $attachmentId, $size )
  {
    $data = [];
	global $_wp_additional_image_sizes;

    $availableSizes = get_intermediate_image_sizes();
    $availableSizes[] = 'full';
    
    if( ! in_array($size, $availableSizes)) 
    {
      return false;
    }
    
    if( in_array($size, array( 'thumbnail', 'medium', 'large' ) ))
    {
      $data['maxWidth'] = (int) get_option( $size . '_size_w' );
      $data['maxHeight'] = (int) get_option( $size . '_size_h' );
      $data['crop'] = (bool) get_option( $size . '_crop' );      
    } elseif ($size == 'full') {
      $data['maxWidth'] = false;
      $data['maxHeight'] = false;
      $data['crop'] = false; 
    } else {
      $data['maxWidth'] = $_wp_additional_image_sizes[ $size ]['width'];
      $data['maxHeight'] = $_wp_additional_image_sizes[ $size ]['height'];
      $data['crop'] = $_wp_additional_image_sizes[ $size ]['crop'];
    }

    $meta = wp_get_attachment_image_src( $attachmentId, $size );
    $url = $meta[0];
    $width =$meta[1];
    $height = $meta[2];
    $isIntermediate = $meta[3];

    // was the image properly resized for cropped
/*
    $cropped = false;
    if( $data['crop'] && 
    ( $width < $data['width'] || $height < $data['height'])) {
      $cropped = true;
    }
*/
    $data['exactSize'] = ( $width == $data['maxWidth'] && $height == $data['maxHeight']);

    $data['url'] = $url;
    $data['isIntermediate'] = $isIntermediate;
    $data['width'] = $width;
    $data['height'] = $height;
     
    return $data;
  }
}