<?php
namespace Sef\WpEntities\Entities;

use Sef\WpEntities\Base\UserEntity;
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Annotation\PostOptions as Options;
use Doctrine\Common\Collections\ArrayCollection;


/**
   * @Options(
   * )
   */
class User extends UserEntity {

  protected $id;

  protected $firstName;

  protected $lastName;

  protected $roles;

  protected $login;

  protected $pass;

  protected $nicename;

  protected $email;

  protected $url;

  protected $displayName;

  protected $registered;

  protected $description;

  protected $status;

  protected $activationKey;

  protected $spam;

  protected $deleted;


  protected function __construct( $user = null )
  {
    parent::__construct($user);
  }

  public static function configOptions()
  {
    return [];
  }
}
