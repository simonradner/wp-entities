<?php
namespace Sef\WpEntities\Entities;       
use Sef\WpEntities\Base\PostEntity;
use Sef\WpEntities\Annotation\PostOptions as Options;


/**
   * @Options(
   *  post_type="any"
   * )
   */
class ParentPost extends PostEntity {

  protected $id;

  protected $author;

  protected $date;

  protected $dateGmt;

  protected $content;

  protected $rawContent;

  protected $title;

  protected $rawTitle;

  protected $excerpt;

  protected $rawExcerpt;

  protected $status;

  protected $name;

  protected $parent;

  protected $menuorder;

  protected $type;

  protected $commentcount;

  protected $link;

  protected $editLink;


  protected function __construct( $post = null ) 
  {
    parent::__construct($post);
  }  

  public static function configOptions()
  {
    return [];
  }
}