<?php
namespace Sef\WpEntities\Entities;
use Sef\WpEntities\Base\CommentEntity;
use Sef\WpEntities\Annotation\CommentOptions as Options;


class Comment extends CommentEntity {

  protected $id;

  protected $postId;

  protected $authorName;

  protected $authorEmail;

  protected $authorUrl;

  protected $authorIP;

  protected $date;

  protected $dateGmt;

  protected $content;

  protected $karma;

  protected $approved;

  protected $agent;

  protected $type;

  protected $parent;

  protected $userId;

}
