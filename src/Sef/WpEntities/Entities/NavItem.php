<?php
namespace Sef\WpEntities\Entities;       
use Sef\WpEntities\Base\PostEntity;
use Sef\WpEntities\Base\EntityBag;  
use Sef\WpEntities\Annotation\PostOptions as Options;
use Doctrine\Common\Collections\ArrayCollection;


/**
   * @Options(
   *  post_type="nav_menu_item"
   * )
   */
class NavItem extends PostEntity {

  protected $id;

  protected $author;

  protected $date;

  protected $dateGmt;

  protected $content;

  protected $rawContent;

  protected $title;

  protected $rawTitle;

  protected $excerpt;

  protected $rawExcerpt;

  protected $status;

  protected $name;

  protected $parent;

  protected $menuorder;

  protected $type;

  protected $commentcount;

  protected $link;

  protected $editLink;


// new

  protected $dbId;

  protected $menuItemParent;

  protected $objectId;

  protected $object;

  protected $typeLabel;

  protected $url;

  protected $navItemTitle;

  protected $target;

  protected $attrTitle;

  protected $description;

  protected $classes;

  protected $xfn;

  protected $current;

  protected $currentItemAncestor;

  protected $currentItemParent;



  protected function __construct( $post = null ) 
  {
    parent::__construct($post);
  }
  
  public function getDbId( EntityBag $entityBag, $dbId )
  {
    return $dbId;
  }

  public function getMenuItemParent( EntityBag $entityBag, $menuItemParent )
  {
    return $menuItemParent;
  }

  public function getObjectId( EntityBag $entityBag, $objectId )
  {
    return $objectId;
  }

  public function getObject( EntityBag $entityBag, $object )
  {
    return $object;
  }

  public function getTypeLabel( EntityBag $entityBag, $typeLabel )
  {
    return $typeLabel;
  }

  public function getUrl( EntityBag $entityBag, $url )
  {
    return $url;
  }

  public function getNavItemTitle( EntityBag $entityBag, $navItemTitle )
  {
    return $navItemTitle;
  }
  
  public function getTarget( EntityBag $entityBag, $target )
  {
    return $target;
  }

  public function getAttrTitle( EntityBag $entityBag, $attrTitle )
  {
    return $attrTitle;
  }

  public function getDescription( EntityBag $entityBag, $description )
  {
    return $description;
  }
  
  public function getClasses( EntityBag $entityBag, $classes )
  {
    return $classes;
  }

  public function getXfn( EntityBag $entityBag, $xfn )
  {
    return $xfn;
  }

  public function getCurrent( EntityBag $entityBag, $current )
  {
    return $current;
  }

  public function getCurrentItemAncestor( EntityBag $entityBag, $currentItemAncestor )
  {
    return $currentItemAncestor;
  }

  public function getCurrentItemParent( EntityBag $entityBag, $currentItemParent )
  {
    return $currentItemParent;
  }

  public static function configOptions()
  {
    return [
      'propertyOptions'   => [
        'dbId'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'menuItemParent'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'objectId'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'object'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'typeLabel'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'url'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'navItemTitle'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'target'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'attrTitle'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'description'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'classes'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'xfn'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'current'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'currentItemAncestor'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ],
        'currentItemParent'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Nav\\InheritFetcher'
        ]
      ]
    ];
  }
}