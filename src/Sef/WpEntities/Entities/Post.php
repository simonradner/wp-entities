<?php
namespace Sef\WpEntities\Entities;
use Sef\WpEntities\Base\PostEntity;
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Annotation\PostOptions as Options;
use Sef\WpEntities\Repositories\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;


/**
   * @Options(
   *  post_type="post"
   * )
   */
class Post extends PostEntity {

  protected $id;

  protected $author;

  protected $date;

  protected $dateGmt;

  protected $content;

  protected $rawContent;

  protected $title;

  protected $rawTitle;

  protected $excerpt;

  protected $rawExcerpt;

  protected $status;

  protected $name;

  protected $parent;

  protected $menuorder;

  protected $type;

  protected $commentcount;

  protected $link;

  protected $editLink;

 /**
   * @Options(
   *  type="entities",
   *  entity="Sef\WpEntities\Entities\Category",
   *  repository="Sef\WpEntities\Repositories\CategoryRepository"
   * )
   */
  protected $categories;

 /**
   * @Options(
   *  type="entities",
   *  entity="Sef\WpEntities\Entities\Tag",
   *  repository="Sef\WpEntities\Repositories\TagRepository"
   * )
   */
  protected $tags;

  /**
    * @Options(
    *  type="entities",
    *  entity="Sef\WpEntities\Entities\Comment",
    *  repository="Sef\WpEntities\Repositories\CommentRepository"
    * )
    */
   protected $comments;

  protected function __construct( $post = null )
  {
    $this->categories = new ArrayCollection;
    $this->tags = new ArrayCollection;
    $this->comments = new ArrayCollection;

    parent::__construct($post);
  }

  public function getCategories( EntityBag $entityBag, $categories )
  {
    return $this->categories;
  }

  public function setCategories( $categories, EntityBag $entityBag )
  {
    $this->categories = $categories;
    return $this;
  }

  public function getTags( EntityBag $entityBag, $tags )
  {
    return $this->tags;
  }

  public function setTags($tags, EntityBag $entityBag )
  {
    $this->tags = $tags;
    return $this;
  }

  public function getComments( EntityBag $entityBag, $comments )
  {
    return $comments;
  }

  public function setComments( $comments, EntityBag $entityBag )
  {
    $this->comments = $comments;
    return $this;
  }

  public static function configOptions()
  {
    return [];
  }
}
