<?php
namespace Sef\WpEntities\Entities;       
use Sef\WpEntities\Base\PostEntity;
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Annotation\PostOptions as Options;
use Doctrine\Common\Collections\ArrayCollection;


/**
   * @Options(
   *  post_type="attachment"
   * )
   */
class Attachment extends PostEntity {

  protected $id;

  protected $author;

  protected $date;

  protected $dateGmt;

  protected $content;

  protected $rawContent;

  protected $title;

  protected $rawTitle;

  protected $excerpt;

  protected $rawExcerpt;

  protected $status;

  protected $name;

  protected $parent;

  protected $menuorder;

  protected $type;

  protected $commentcount;

  protected $link;

  protected $editLink;
  
  protected $mimeType;

  protected $attachmentUrl;

  protected $sizes;
  
  public function getMimeType( EntityBag $entityBag, $mimeType )
  {
    return $this->mimeType;
  }

  public function getAttachmentUrl( EntityBag $entityBag, $attachmentUrl  )
  {
    return $this->attachmentUrl;
  }

  public function getSizes( EntityBag $entityBag, $sizes )
  {
    return $this->sizes;
  }
  
  public static function configOptions()
  {
    return [
      'propertyOptions'   => [
        'mimeType'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'attachmentUrl'  => [
          'type'  => 'attachmenturl'
        ],
        'sizes'  => [
          'entity'  => 'Sef\\WpEntities\\Entities\\Util\\Imagesize',
          'type'  => 'imagesizes',
          'size'  => [],          
        ]
      ]
    ];
  }
}