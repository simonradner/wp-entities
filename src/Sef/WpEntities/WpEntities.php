<?php
namespace Sef\WpEntities;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\FileCacheReader;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\Options;



class WpEntities {

  private static $instance;

  public $validator;

  public $annotationReader;

  protected $options;

  private static $built = false;

  protected function __construct($options=[]) {

    $resolver = new OptionsResolver();
    $resolver->setDefaults(array(
      'cacheDir'       => false,
      'srcDir'       => dirname(dirname(__DIR__)),
      'symfonyConstraintsDir' => dirname(dirname(dirname(dirname(dirname(__DIR__))))) . '/symfony/validator/Constraints/',
      'debug' => (defined( 'WP_DEBUG') && WP_DEBUG)
    ));

    $resolver->setDefault('annotationReader', function (Options $options, $previousValue) {
        return $this->createAnnotationReader($options);
    });
/*
    $resolver->setDefault('validator', function (Options $options, $previousValue) {
        return $this->createValidator($options);
    });
*/
    $resolver->setDefined('validator');

    $resolver->setAllowedTypes('cacheDir', ['bool', 'string'] );
    $resolver->setAllowedTypes('srcDir', ['string'] );
    $resolver->setAllowedTypes('symfonyConstraintsDir', ['string'] );
    $resolver->setAllowedTypes('annotationReader', 'Doctrine\Common\Annotations\Reader' ); // Interface: Doctrine\Common
    $resolver->setAllowedTypes('validator', ['Symfony\Component\Validator\Validator\ValidatorInterface'] );
    $resolver->setAllowedTypes('debug', 'bool' );

    $this->options = $resolver->resolve($options);
    if( isset($this->options['validator']))
    {
      $this->validator = $this->options['validator'];
    }
    $this->annotationReader = $this->options['annotationReader'];
    $this->entityManager = new EntityManager();


    //add_action('wp_footer', [$this, 'test']);
  }

  /**
   * build function.
   *
   * @access public
   * @static
   * @param mixed $options (default: [])
   * @return void
   */
  public static function build($options=[])
  {
    if(true === static::$built )
      return;

    static::$instance = new static($options);

    static::$built = true;
  }

  /**
   * getInstance function.
   *
   * @access public
   * @static
   * @return void
   */
  public static function getInstance()
  {
      if (null === static::$instance) {
          static::build();
      }
      return static::$instance;
  }

  private function __clone(){}

  private function __wakeup(){}

  /**
   * configureFormFactory function.
   *
   * creates the default Validator
   *
   * @access public
   * @return void
   */
  public function createValidator($options=[])  {

    // validator is optional
    if ( ! class_exists("Symfony\Component\Validator\Validation"))
      return false;


    // create the validator - details will vary
    $validator = \Symfony\Component\Validator\Validation::createValidatorBuilder()
      ->enableAnnotationMapping($options['annotationReader'])
      ->addMethodMapping('loadValidatorMetadata')
      ->getValidator();

    // Validator Annotation Autoload
    AnnotationRegistry::registerLoader(function($class) use($options) {
      $file = str_replace("\\", DIRECTORY_SEPARATOR, $class) . ".php";
      if (strpos($file, "Symfony/Component/Validator/Constraints/") === 0) {

        $file = str_replace("Symfony/Component/Validator/Constraints/", '', $file);
        if (file_exists(trailingslashit($options['symfonyConstraintsDir']) . $file)) {
          // file exists makes sure that the loader fails silently
          require trailingslashit($options['symfonyConstraintsDir']) . $file;
          return true;
        }
      }
    });

    return $validator;
  }

  /**
   * configureAnnotationReader function.
   *
   * creates the default annotation reader
   *
   * @access public
   * @return void
   */
  public function createAnnotationReader($options = [])
  {
    AnnotationRegistry::registerAutoloadNamespace("Sef\WpEntities\Annotation", $options['srcDir']);
    // Cache Reader
    if($options['cacheDir']) {
      $annotationReader = new FileCacheReader(
          new AnnotationReader(),
          trailingslashit($options['cacheDir']) . 'entities/annotationreader',
          $debug = $options['debug']
      );
    } else {
      $annotationReader = new AnnotationReader();
    }

    return  $annotationReader;
  }


  public function test()
  {
    $entity = Entities\Post::make(1);
    echo'<pre>';  print_r($entity->export()); echo'</pre>';
    return;
      $entity = Entities\Post::make();
//     echo '<pre>'; print_r($entity); echo '</pre>';
    $entity->setCategory('Book');
//     $entity->setCategory('Unknown');

    echo '<pre>'; print_r($entity->getCategory()); echo '</pre>';



    return;

      $category = Entities\Category::make(11);
      $category2 = Entities\Category::make(5);

      $entity = Entities\Post::make();
      $entity->setTitle('full new');
      $entity->setContent('Saved Content');
      $entity->setColor('green');
      $cats = $entity->getCategories();
      $cats->add($category);
      $cats->add($category2);
//       echo '<pre>'; print_r($cats); echo '</pre>';
      $entity->setCategories($cats);
      $entity->setStatus('publish');

//       echo '<pre>'; print_r($entity->getCategories()); echo '</pre>';
 $entity->save();

//        echo '<pre>'; print_r($entity->export()); echo '</pre>';
//        echo '<pre>'; print_r($entity->getDate()); echo '</pre>';
//        echo '<pre>'; print_r($entity->getColor()); echo '</pre>';



    return;
   // return;


//    $entity = new \Entity\Person(71);

    //$entity = Entities\Post::create(5)->fetch();
    $entity1 = Entities\Post::make(5)->fetch();
  //  $config = $entity->config->options;
//     $config->set('validator', null);
//     $config->set('annotationReader', null);

//    $entity->getTitle();
  //  echo '<pre>'; print_r($entity->getDate()); echo '</pre>';
//     echo '<pre>'; print_r($entity->getLink()); echo '</pre>';

    //$entity->fetch();

  //  $realEntity = $entity->getInstance();
//     echo '<pre>'; print_r($config); echo '</pre>';
//     echo '<pre>'; print_r($entity->getConfig()->options['post_type']); echo '</pre>';

//      echo '<pre>'; print_r($realEntity); echo '</pre>';


//   $converter = new \Sef\WpEntities\Components\Converter\DateTimeConverter('now');
//   echo '<pre>'; print_r($converter->convert()); echo '</pre>';


   // return;

 $repo =  \Sef\WpEntities\Repositories\PostRepository::make();
 foreach($repo->findAll() as $entity ) {
       //  echo '<pre>'; print_r($entity->getInstance()); echo '</pre>';
//          echo $entity->getColor();
 }


 $repo = \Sef\WpEntities\Repositories\CategoryRepository::make();
//           echo '<pre>'; print_r($repo->findAll()); echo '</pre>';

 foreach($repo->findAll() as $entity ) {
//           echo '<pre>'; print_r($entity->getInstance()); echo '</pre>';
//           echo $entity->getSlug();
 }




//    $entity = new \Entity\Person(71);

    //$entity = Entities\Post::create(5)->fetch();
    $entity = Entities\Category::make(4);
   // $config = $entity->config->options;
//     $config->set('validator', null);
//     $config->set('annotationReader', null);

//    $entity->getTitle();
//     echo '<pre>'; print_r($entity); echo '</pre>';
//     echo '<pre>'; print_r($entity->getLink()); echo '</pre>';

    //$entity->fetch();
  $entity->getLink();
  $entity->getColor();

   $realEntity = $entity->getInstance();
//    echo '<pre>'; print_r($realEntity); echo '</pre>';
//     echo '<pre>'; print_r($config); echo '</pre>';
//     echo '<pre>'; print_r($entity->getConfig()->options['post_type']); echo '</pre>';

//     echo '<pre>'; print_r($entity); echo '</pre>';


//   $converter = new \Sef\WpEntities\Components\Converter\DateTimeConverter('now');
//   echo '<pre>'; print_r($converter->convert()); echo '</pre>';




  }
}
