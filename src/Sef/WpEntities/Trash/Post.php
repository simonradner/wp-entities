<?php
namespace Sef\WpEntities\Entities;       
use Sef\WpEntities\Base\PostEntity;
use Sef\WpEntities\Annotation\PostOptions as Options;
use Sef\WpEntities\Repositories\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
   * @Options(
   *  post_type="post"
   * )
   */
class Post extends PostEntity {

  /**
   */
  protected $id;


  /**
   */
  protected $type;

  /**
   */
  protected $date;

  /**
   * @Options(format="display")
   */
  protected $author;

  /**
   * @Assert\NotBlank()
   */
  protected $title;

  /**
   */
  protected $status;

 /**
   */
  protected $excerpt;

 /**
   */
  protected $editExcerpt;


 /**
   */
  protected $content;


 /**
   */
  protected $editContent;


 /**
   * @Options(
   *  type="meta"
   * )
   * @Assert\Email(
   *     message = "The email '{{ value }}' is not a valid email.",
   *     checkMX = false
   * )
   */
  protected $email;


 /**
   */
  protected $link;


 /**
   * @Options(
   *  filter="display",
   *  context="bar",
   *  type="meta"
   * )
   */
  protected $color;


 /**
   * @Options(
   *  type="entities",
   *  entity="Sef\WpEntities\Entities\Category",
   *  repository="Sef\WpEntities\Repositories\CategoryRepository"
   * )
   */
  protected $categories;

 /**
   * @Options(
   *  type="entity",
   *  entity="Sef\WpEntities\Entities\Category",
   *  repository="Sef\WpEntities\Repositories\CategoryRepository",
   *  setterConverter="Sef\WpEntities\Components\Converter\PropertyConverter\Name2TermPropertyConverter"
   * )
   */
  protected $category;


 /**
   * @Options(
   *  type="entities",
   *  entity="Sef\WpEntities\Entities\Tag",
   *  repository="Sef\WpEntities\Repositories\TagRepository"
   * )
   */
  protected $tags;

 /**
   * @Options(
   * fetcher="Sef\WpEntities\Components\Fetcher\Leaf\Post\FilteredContentFetcher"
   * )
   */
  protected $filteredContent;


  protected function __construct( $post = null ) 
  {
    $this->categories = new ArrayCollection;
    $this->tags = new ArrayCollection;
    parent::__construct($post);
  }
  
  public function getColor()
  {
    return $this->color;
  }
  
  public function setColor($color)
  {
    $this->color = $color;
  }

  public function getEmail()
  {
    return $this->email;
  }
  
  public function setEmail($email)
  {
    $this->email = $email;
  }
  
  public function getCategory()
  {
    return $this->category;
  }
  
  /// must be repository
  
  // @TODO setterConverter
  
  public function setCategory($category)
  {
//     echo '<pre>'; print_r($category); echo '</pre>';
    
    if($category->isNew())
    {
      $category->save();
    }
    
    $this->category = $category;
    return $this;
    
    
    
/*
    ///
    if ($category instanceof Category ) {
      $this->category = $category;
      return $this;    
    } 
    if ( is_string( $category ) ) {
      $categoryObject = new Category($category);
      $this->category = $categoryObject;      
      return $this;    
    } 
*/
  }    

  public function getCategories()
  {
    return $this->categories;
  }
  
  public function setCategories( $categories )
  {    
    if ($categories instanceof ArrayCollection ) {
      $this->categories = $categories;
      return $this;    
    } 
    if ( is_string( $categories ) ) {
      
      $catNames = array_map('trim', explode(',', $categories));
      $this->categories = new ArrayCollection();
      foreach($catNames as $catName ) {
        $this->categories->add(new Category($catName));
      } 
      return $this;    
    }     
  }   

  public function getTags()
  {
    return $this->tags;
  }
  
  /// must be repository
    // @TODO setterConverter

  public function setTags($tags)
  {

    if ($tags instanceof ArrayCollection ) {
      $this->tags = $tags;
      return $this;    
    } 
    if ( is_string( $tags ) ) {
      
      $names = array_map('trim', explode(',', $tags));
      $this->tags = new ArrayCollection();
      foreach($names as $name ) {
        $this->tags->add(new Category($name));
      } 
      return $this;    
    }    
    
  }    



  public static function configOptions()
  {
    return [
      'foo' => 4,
      'classOptions' => [
        'foo' => 3
      ],
      'propertyOptions'   => [
        'date'  => [
          'getterConverter' => 'Sef\\WpEntities\\Components\\Converter\\DateTimeConverter'  // testing
        ]
      ]
    ];
  }
}