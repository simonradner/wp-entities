<?php
namespace Sef\WpEntities\Entities;       
use Sef\WpEntities\Base\TermEntity;
use Sef\WpEntities\Annotation\TermOptions as Options;

 /**
   * @Options(
   *  taxonomy="category"
   * )
   */
class Category extends TermEntity {

 /**
   * @Options(
   *  type="meta"
   * )
   */
  protected $color;

 /**
   */
  protected $id;

 /**
   */ 
  protected $name;

  protected $slug;

  protected $description;
  
  public function getColor()
  {
    return $this->color;
  }

  public function setColor($color)
  {
    $this->color = $color;
    return $this;
  }
}