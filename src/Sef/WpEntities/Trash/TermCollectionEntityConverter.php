<?php
namespace Sef\WpEntities\Components\Converter;       
use Sef\WpEntities\Interfaces\EntityConverter;       
use Sef\WpEntities\Interfaces\CollectionEntityConverter;       
use Doctrine\Common\Collections\ArrayCollection;

class TermCollectionEntityConverter implements CollectionEntityConverter {
  
  protected $entityConverter;

  public function __construct( $entityConverter = null )
  {
    if($entityConverter)
      $this->setEntityConverter($entityConverter);
  }
  
  public function convert( $data ){
    
    if( $data instanceof ArrayCollection)
      return $data;

    if( ! is_array($data)) {
      $array[] = $data;
      $data = $array;
    }
    $collection = new ArrayCollection;
    foreach($data as $item )
    {
      $collection->add( $this->entityConverter->convert($item));
    }

    return $collection;  
  }
  
  public function setEntityConverter(EntityConverter $converter ) {
    $this->entityConverter = $converter;
  } 
}


