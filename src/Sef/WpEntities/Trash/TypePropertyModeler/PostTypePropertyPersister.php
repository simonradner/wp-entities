<?php
namespace Sef\WpEntities\Components\TypePropertyModeler;       
use Sef\WpEntities\Base\Entity;
use Sef\WpEntities\Base\TermEntity;
use Sef\WpEntities\Base\PostEntity;
use Doctrine\Common\Collections\ArrayCollection;

class PostTypePropertyPersister {
  
  protected $entity;
  protected $entityReflectionObject;
  protected $saverMethods = [];
  
  public function __construct( Entity $entity, $types = [] )
  {
      $this->entity = $entity;
      $this->types = $types;
      $this->entityReflectionObject = new \ReflectionObject($this);

    foreach($this->types as $type ) {
      if($this->entity->propertiesByType($type)) {
        foreach ($this->entity->propertiesByType($type) as $property ) {          
          if($this->entityReflectionObject->hasMethod($this->entity->saverName($property))) {
            $this->saverMethods[] = $this->entity->saverName($property);
          }
        }       
      }
    }
  }
  
  public function persist()
  {
    foreach($this->types as $type ) {
      if($this->entity->propertiesByType($type)) {
        foreach ($this->entity->propertiesByType($type) as $property ) {          
          if( ! $this->entityReflectionObject->hasMethod($this->entity->saverName($property))) {
            $propertyPersister = 'persist' . ucfirst($type);
            $propertyValue = $this->entity->rawPropertyValue($property);
            $this->$propertyPersister($this->entity, $property, $propertyValue);            
          }
        }       
      }
    }
  }

  public function getSaverMethods()
  {
    return $this->saverMethods;
  }

  protected function persistEntity($entity, $property, $propertyValue ) 
  {

  }
  protected function persistEntities($entity, $property, $propertyValue ) 
  {
    // its a collection of terms
    
    if( $propertyValue instanceof ArrayCollection && ! $propertyValue->isEmpty() && $propertyValue->forAll(function($key, $element){
      return ( $element instanceof TermEntity ) ? true : false; // check if all items are termEntities
    })) {
      $this->persistTerms($entity, $property, $propertyValue);  
    }

    // its a collection of posts
    
    if( $propertyValue instanceof ArrayCollection && ! $propertyValue->isEmpty() && $propertyValue->forAll(function($key, $element){
      return ( $element instanceof PostEntity ) ? true : false; // check if all items are termEntities
    })) {
      $this->persistPosts($entity, $property, $propertyValue);  
    } 
  }

// DEPRICATED

  
  protected function persistTerm($entity, $property, $propertyValue ) 
  {
    if( $propertyValue instanceof TermEntity ) {
      wp_set_object_terms( $entity->getId(), $propertyValue->getId(), $propertyValue->getTaxonomy(), $append = false );            
    }
  }

  protected function persistTerms($entity, $property, $propertyValue  ) 
  {
    if( $propertyValue instanceof ArrayCollection && ! $propertyValue->isEmpty() && $propertyValue->forAll(function($key, $element){
      return ( $element instanceof TermEntity ) ? true : false; // check if all items are termEntities
    })) {
      $terms = $propertyValue;
      $taxonomies = [];
      foreach ($terms as $term ) {
        if( ! isset($taxonomies[$term->getTaxonomy()])) {
          $taxonomies[$term->getTaxonomy()] = [];
        }
        $taxonomies[$term->getTaxonomy()][] = $term->getId();
      }
      foreach( $taxonomies as $taxonomy => $ids ) {
        wp_set_object_terms( $entity->getId(), $ids, $taxonomy, $append = false );            
      }
    }
  }

  protected function persistPosts($entity, $property, $propertyValue  ) 
  {
    if( $propertyValue instanceof ArrayCollection && ! $propertyValue->isEmpty() && $propertyValue->forAll(function($key, $element){
      return ( $element instanceof PostEntity ) ? true : false; // check if all items are postEntities
    })) {
      $posts = $propertyValue;
      $postsIds = [];
      foreach ($posts as $post ) {
        $postsIds[] = $post->getId();
      }
      update_post_meta($entity->getId(), $property, $postsIds);
    }
  }

}