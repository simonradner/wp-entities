<?php
namespace Sef\WpEntities\Components\TypePropertyModeler;       
use Sef\WpEntities\Base\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class PostTypePropertySetter {
  
  protected $entity;
  protected $entityConfig;
  
  public function __construct( Entity $entity, $types = [] )
  {
    $entity::getConfig();
      $this->entity = $entity;
      $this->entityConfig = $entity::getConfig();
      $this->types = $types;

  }
  
  public function set()
  {
    

    foreach($this->types as $type ) {
      if($this->entityConfig->getPropertiesByType($type)) {
        foreach ($this->entityConfig->getPropertiesByType($type) as $property ) {
          $setterName = $this->entity->setterName($property->name); // refactor
          $propertyFetcher = 'fetch' . ucfirst($type);
          $value = $this->$propertyFetcher($this->entity, $property);
          $this->entity->$setterName($value);
        }       
      }
    }
  }
  
  protected function fetchMeta($entity, $property ) 
  {
    return get_post_meta($this->entity->getId(), $property->name, true); 
  }


  protected function fetchEntity($entity, $property ) 
  {
    $targetEntityClass = $property->getOptions()->get('entity');    
    $repositoryClass = $property->getOptions()->get('repository');    
    
    $repository = new $repositoryClass();
    $targetEntityReflector = new \ReflectionClass($targetEntityClass);
    
    // a post
    if( $targetEntityReflector->isSubclassOf('Sef\\WpEntities\\Base\\PostEntity')) {
      $targetEntity =  $repository->findById(get_post_meta($entity->getId(), $property->name, true ));
      return $targetEntity;    
    }
    
    // a term
    if( $targetEntityReflector->isSubclassOf('Sef\\WpEntities\\Base\\TermEntity')) {
      $term = $repository->findOneByPost($entity);
      return $term; 
    }
  }

  protected function fetchEntities($entity, $property, $options = [] ) 
  {
    $targetEntityClass = $property->getOptions()->get('entity');    
    $repositoryClass = $property->getOptions()->get('repository');     
    
    $repository = new $repositoryClass();
    $targetRepositoryReflector = new \ReflectionClass($repositoryClass);

    // a collection of post
    if( $targetRepositoryReflector->isSubclassOf('Sef\\WpEntities\\Base\\PostRepository')) {
      $arrayCollection = $repository->findByIds(get_post_meta($entity->getId(), $property->name, true ));
      return $arrayCollection; 
    }
    
    // a collections of terms
    if( $targetRepositoryReflector->isSubclassOf('Sef\\WpEntities\\Base\\TermRepository')) {
      $arrayCollection = $repository->findByPost($entity);
      return $arrayCollection;      
    }
  }






// DEPRICATED

  protected function fetchTerm($entity, $property ) 
  {
    $repositoryClass = $property->getOptions()->get('repository');     
    $repository = new $repositoryClass();
    $term = $repository->findOneByPost($entity);
    return $term; 
  }

  protected function fetchTerms($entity, $property, $options = [] ) 
  {
    $repositoryClass = $property->getOptions()->get('repository');     
    $repository = new $repositoryClass();
    $arrayCollection = $repository->findByPost($entity);
    return $arrayCollection; 
  }

  protected function fetchPosts($entity, $property, $options = [] ) 
  {
    $repositoryClass = $property->getOptions()->get('repository');     
    $repository = new $repositoryClass();
    $arrayCollection = $repository->findByIds(get_post_meta($entity->getId(), $property->name, true ));
    return $arrayCollection; 
  }
  
  protected function fetchPost($entity, $property, $options = [] ) 
  {
    $repositoryClass = $property->getOptions()->get('repository');     
    $repository = new $repositoryClass();
    $arrayCollection = $repository->findByIds(get_post_meta($entity->getId(), $property->name, true ));
    return $arrayCollection; 
  }



}