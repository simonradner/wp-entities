<?php
namespace Sef\WpEntities\Components\Exporter\Raw;    
use Sef\WpEntities\Components\Exporter\AbstractExporterStrategy;   
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Base\Entity;
use Sef\WpEntities\Interfaces\ExporterStrategyInterface;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Sef\WpEntities\Components\Exporter\ExportObject;
use Doctrine\Common\Collections\ArrayCollection;

class AllExporterStrategy extends AbstractExporterStrategy implements ExporterStrategyInterface {

  /**
   * generateTarget function.
   * 
   * @access protected
   * @param EntityBag $from
   * @return object
   */
  protected function generateTarget( EntityBag $from )
  {
    return new ExportObject;
  }

  /**
   * setTargetValue function.
   * 
   * @access protected
   * @param PropertyInterface $property
   * @param mixed $target
   * @param mixed $value
   * @return void
   */
  protected function setTargetValue( PropertyInterface $property, $target, $value )
  {
    $propertyName = $property->getName();
    $target->{$propertyName} = $value;
  }
}




