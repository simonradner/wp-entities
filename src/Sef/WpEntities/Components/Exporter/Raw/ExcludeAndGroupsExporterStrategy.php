<?php
namespace Sef\WpEntities\Components\Exporter\Raw;       
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Base\Entity;
use Sef\WpEntities\Interfaces\ExporterStrategyInterface;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Sef\WpEntities\Components\Exporter\ExportObject;
use Doctrine\Common\Collections\ArrayCollection;

class ExcludeAndGroupsExporterStrategy extends ExcludeExporterStrategy implements ExporterStrategyInterface {
    
 
  /**
   * setGroups function.
   * 
   * @access public
   * @param mixed $groups
   * @return void
   */
  public function setGroups( $groups ){
    if ( is_array( $groups ))
      $this->groups = $groups;
    
    if ( is_string( $groups ))
      $this->groups = [$groups];
  } 

  /**
   * setGroup function.
   * 
   * alias
   *
   * @access public
   * @param mixed $groups
   * @return void
   */
  public function setGroup( $groups ){
    $this->setGroups( $groups );
  } 

  /**
   * isPropertyIncluded function.
   * 
   * @access protected
   * @param PropertyInterface $property
   * @return void
   */
  protected function isPropertyIncluded( PropertyInterface $property )
  {


    // GROUP EXCLUSION


    $propertyGroups = $property->getGroups()->get();
    
    // no group in annotation
    if( empty( $propertyGroups )) 
    {
      return false;      
    }
    
    // test groups from $groups (set via setGroups, or args, set via setArgs
    // setGroups has priority
    $testGroups = [];
    if( ! empty( $this->groups )) 
    {
      $testGroups = $this->groups;
    } else 
    {
      if( isset($this->args['@groups'])) 
      {
        $testGroups = $this->args['@groups'];
      }
    }
    
    if( ! empty( $testGroups )) {
      // no matching group in annotation
      $commonItems = array_intersect( $testGroups, $propertyGroups );
      if( ! $commonItems ) 
      {
        return false;
      }
    }


    return parent::isPropertyIncluded( $property );
    
  } 


  /**
   * generateRecusrsiveStrategy function.
   * 
   * @access public
   * @param PropertyInterface $property
   * @param mixed $target
   * @param mixed $value
   * @return void
   */
  protected function generateRecusrsiveStrategy( PropertyInterface $property, $target, $value )
  {
    $strategy = new static();
    $strategy->setArgs($this->getNestedArgs( $property ))->setGroups($this->groups);
    return $strategy;
  }

}