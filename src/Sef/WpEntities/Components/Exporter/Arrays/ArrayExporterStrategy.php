<?php
namespace Sef\WpEntities\Components\Exporter\Arrays;    
use Sef\WpEntities\Components\Exporter\AbstractExporterStrategy;   
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Base\Entity;
use Sef\WpEntities\Interfaces\ExporterStrategyInterface;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Doctrine\Common\Collections\ArrayCollection;

class ArrayExporterStrategy extends AbstractExporterStrategy implements ExporterStrategyInterface {

  /**
   * generateTarget function.
   * 
   * @access protected
   * @param EntityBag $from
   * @return object
   */
  protected function generateTarget( EntityBag $from )
  {
    return new ArrayCollection;
  }

  protected function setTargetValue( PropertyInterface $property, $target, $value )
  {
    $propertyName = $property->getName();
    $target->set($propertyName, $value);
  }
  
  protected function returnTarget( $target )
  {
    return $target->toArray();
  }  
}