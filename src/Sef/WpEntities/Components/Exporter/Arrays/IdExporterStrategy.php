<?php
namespace Sef\WpEntities\Components\Exporter\Arrays;    
use Sef\WpEntities\Components\Exporter\AbstractExporterStrategy;   
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Base\Entity;
use Sef\WpEntities\Interfaces\ExporterStrategyInterface;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Doctrine\Common\Collections\ArrayCollection;

class IdExporterStrategy extends ArrayExporterStrategy implements ExporterStrategyInterface {


  protected function returnTarget( $target )
  {
    return $target->get('id');
  }  


  /**
   * isPropertyIncluded function.
   * 
   * @access protected
   * @param PropertyInterface $property
   * @return void
   */
  protected function isPropertyIncluded( PropertyInterface $property )
  {

    // explicitly inluded
    if( $property->getName() === 'id')
    {
      return true;
    }     

    return false;
  } 
}