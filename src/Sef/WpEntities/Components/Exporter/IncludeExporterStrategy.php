<?php
namespace Sef\WpEntities\Components\Exporter;       
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Base\Entity;
use Sef\WpEntities\Interfaces\ExporterStrategyInterface;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Sef\WpEntities\Components\Exporter\ExportObject;
use Doctrine\Common\Collections\ArrayCollection;


class IncludeExporterStrategy extends AllExporterStrategy implements ExporterStrategyInterface {
    
  protected $args = [];

  /**
   * isPropertyIncluded function.
   * 
   * @access protected
   * @param PropertyInterface $property
   * @return void
   */
  protected function isPropertyIncluded( PropertyInterface $property )
  {
    
    // no include strategy
    if( empty($this->args))
    {
      return true;
    } 
    // explicitly inluded
    if( in_array($property->getName(), $this->args))
    {
      return true;
    }     
    
    // explicitly inluded with an array of values for the next level
    if( array_key_exists( $property->getName(), $this->args))
    {
      return true;
    }     
    return false;
  } 

  
}
