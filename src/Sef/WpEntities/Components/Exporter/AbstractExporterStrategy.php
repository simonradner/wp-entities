<?php
namespace Sef\WpEntities\Components\Exporter;       
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Base\Entity;
use Sef\WpEntities\Interfaces\ExporterStrategyInterface;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Doctrine\Common\Collections\ArrayCollection;

abstract class AbstractExporterStrategy implements ExporterStrategyInterface {
    
  protected $args = [];
  
  protected $target;
  
  public function __construct( array $args = [] )
  {
    $this->args = $args;
  }

  public function setArgs( array $args = [])
  {
    $this->args = $args;
    return $this;
  }
  
  public function export( EntityBag $from )
  { 
    
    $to = $this->generateTarget( $from );
    $this->target = $to;
    
    foreach( $from->config->getProperties() as $property )
    {
      
      // dont know if not already excluded
      // remove properties we dont want
      if( $property->getName() === 'wpEntity' ) {
        continue;           
      }    

      // remove property of excluded
      if( ! $this->isPropertyIncluded( $property )) {
        continue;           
      }      
      
      // property is included...
      $propertyName = $property->getName();
      $getter = $property->getterName();

      if( is_callable( [$from->getInstance(), $getter]) )
      {
        // property fetchers are activated here
        $value = $from->$getter();
        
        // find an entity
        if( $value instanceof EntityBag ) {
  
          $strategy = $this->generateRecusrsiveStrategy( $property, $to, $value );
          $newValue = $value->export( $strategy );
          
          $this->setTargetValue( $property, $to, $newValue );
          continue;        
        }
        
        // find an entities
        if( $value instanceof ArrayCollection ) {
          $exportedArrayCollection = $value->map( function( $item ) use ( $property, $to, $value ) {
            if( $item instanceof EntityBag ) {
              $strategy = $this->generateRecusrsiveStrategy( $property, $to, $value );
              return $item->export( $strategy );
            } else {
              return $item;
            }   
          });        
          $normalized = $exportedArrayCollection->toArray();
          $this->setTargetValue( $property, $to, $normalized );
          continue;        
        }
        
        // default
      $this->setTargetValue( $property, $to, $value );
      }
    }
    return $this->returnTarget($to);  
  }



  /**
   * generateTarget function.
   * 
   * @access protected
   * @abstract
   * @param EntityBag $from
   * @return object
   */
  abstract protected function generateTarget( EntityBag $from );
  
  /**
   * getTarget function.
   * 
   * @access protected
   * @return object
   */
  protected function getTarget()
  {
    return $this->target;
  }


  /**
   * setTargetValue function.
   * 
   * @access protected
   * @abstract
   * @param PropertyInterface $property
   * @param mixed $target
   * @param mixed $value
   * @return void
   */
  abstract protected function setTargetValue( PropertyInterface $property, $target, $value );
  
  /**
   * returnTarget function.
   * 
   * @access protected
   * @param mixed $target
   * @return void
   */
  protected function returnTarget( $target )
  {
    return $target;
  }

  /**
   * generateRecusrsiveStrategy function.
   * 
   * @access public
   * @param PropertyInterface $property
   * @param mixed $target
   * @param mixed $value
   * @return void
   */
  protected function generateRecusrsiveStrategy( PropertyInterface $property, $target, $value )
  {
    $strategy = new static();
    $strategy->setArgs($this->getNestedArgs( $property ));
    return $strategy;
  }
  
  /**
   * isPropertyIncluded function.
   * 
   * @access protected
   * @param PropertyInterface $property
   * @return bool
   */
  protected function isPropertyIncluded( PropertyInterface $property )
  { 
    return true;
  }
  
  /**
   * getNestedArgs function.
   * 
   * entities or entity properties my have it's own exclusion array
   *
   * @access protected
   * @param PropertyInterface $property
   * @return void
   */
  protected function getNestedArgs( PropertyInterface $property )
  {
    // no nested args
    if(empty( $this->args ))
    {
      return [];      
    }
    
    // nested args array found for this property
    if( array_key_exists( $property->getName(), $this->args) && is_array($this->args[$property->getName()]))
    {
      return $this->args[$property->getName()];      
    }
    
    return [];
  }   
}




