<?php
namespace Sef\WpEntities\Components\Exporter;    
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Base\Entity;
use Sef\WpEntities\Interfaces\ExporterStrategyInterface;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Sef\WpEntities\Components\Exporter\ExportObject;
use Doctrine\Common\Collections\ArrayCollection;

class AllExporterStrategy extends AbstractExporterStrategy implements ExporterStrategyInterface {

  /**
   * generateTarget function.
   * 
   * @access protected
   * @param EntityBag $from
   * @return object
   */
  protected function generateTarget( EntityBag $from )
  {
    return clone $from->getInstance();
  }

  protected function setTargetValue( PropertyInterface $property, $target, $value )
  {
    $target->_setProperty($property, $value);
  }
}

