<?php
namespace Sef\WpEntities\Components\Exporter;       
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Base\Entity;
use Sef\WpEntities\Interfaces\ExporterStrategyInterface;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Sef\WpEntities\Components\Exporter\ExportObject;
use Doctrine\Common\Collections\ArrayCollection;
    
    
class ExcludeExporterStrategy extends IncludeExporterStrategy implements ExporterStrategyInterface {
    
  protected $args = [];


  /**
   * isPropertyIncluded function.
   * 
   * @access protected
   * @param PropertyInterface $property
   * @return void
   */
  protected function isPropertyIncluded( PropertyInterface $property )
  {
    
    // no exclude strategy
    if( empty($this->args))
    {
      return true;
    } 
    // explicitly excluded
    if( in_array($property->getName(), $this->args))
    {
      return false;
    }     
    
    // explicitly excluded with an array of values for the next level
    if( array_key_exists( $property->getName(), $this->args))
    {
      return true;
    }    
    
    return true;
  } 
}




