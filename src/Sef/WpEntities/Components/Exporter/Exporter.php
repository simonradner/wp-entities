<?php
namespace Sef\WpEntities\Components\Exporter;       
  
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Interfaces\ExporterStrategyInterface;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Exporter class.
 *
 * exports the entity Instance, all included data fetched, recursive, bags are removed
 */
class Exporter {
  
  public $entityBag;
  
  protected $strategy = null;

  
  public function __construct( EntityBag $entityBag )
  {
    $this->entityBag = $entityBag;
    $this->strategy = new AllExporterStrategy;
  }
  
  public function setStrategy( ExporterStrategyInterface $strategy )
  {
    $this->strategy = $strategy;
  }
  
  public function export()
  {
    $entityBag = $this->entityBag;
    $strategy = $this->strategy;
    
    return $strategy->export( $entityBag );    
  }
}