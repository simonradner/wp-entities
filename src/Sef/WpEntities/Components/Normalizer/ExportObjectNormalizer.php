<?php
namespace Sef\WpEntities\Components\Normalizer;       
use Sef\WpEntities\Components\Exporter\ExportObject;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * User normalizer
 */
class ExportObjectNormalizer implements NormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = array())
    {
      $to = [];
            $properties = get_object_vars($object);

      foreach($properties as $k => $v)
      {
        $to[$k] = $v;
      }
      
      return $properties;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ExportObject;
    }
}