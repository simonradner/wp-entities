<?php
namespace Sef\WpEntities\Components\WpEntity;       
use Sef\WpEntities\Interfaces\WpEntityInterface;       


class Term extends WpEntity implements WpEntityInterface {
 
  public function __construct( \WP_Term $initialObject ) 
  {
    $this->initialObject = $initialObject;
  }
  
  public function getId() {
    return $this->lazyLoadProperty('term_id');
  }  
}