<?php
namespace Sef\WpEntities\Components\WpEntity;
use Sef\WpEntities\Interfaces\WpEntityInterface;


class User extends WpEntity implements WpEntityInterface {

  public function __construct( \WP_User $initialObject )
  {
    $this->initialObject = $initialObject;
  }

  public function getId() {
    return $this->lazyLoadProperty('ID');
  }
}
