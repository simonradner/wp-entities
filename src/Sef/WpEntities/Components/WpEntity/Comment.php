<?php
namespace Sef\WpEntities\Components\WpEntity;
use Sef\WpEntities\Interfaces\WpEntityInterface;


class Comment extends WpEntity implements WpEntityInterface {

  public function __construct( \WP_Comment $initialObject )
  {
    $this->initialObject = $initialObject;
  }

  public function getId() {
    return $this->lazyLoadProperty('comment_ID');
  }
}
