<?php
namespace Sef\WpEntities\Components\WpEntity;       
use Sef\WpEntities\Interfaces\WpEntityInterface;       


class Post extends WpEntity implements WpEntityInterface {

  public function __construct( \WP_Post $initialObject ) 
  {
    $this->initialObject = $initialObject;
  }

  public function getId() {
    return $this->lazyLoadProperty('ID');
  }  
}