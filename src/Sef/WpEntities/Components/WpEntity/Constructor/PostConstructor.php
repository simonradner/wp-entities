<?php
namespace Sef\WpEntities\Components\WpEntity\Constructor;
use Sef\WpEntities\Components\WpEntity\Constructor\Constructor;
use Sef\WpEntities\Components\WpEntity\Post;
use Sef\WpEntities\Components\WpEntity\WpEntity;
use Doctrine\Common\Collections\ArrayCollection;

class PostConstructor extends Constructor {

  protected function wrapData( $data )
  {
    if ( $data instanceof \WP_Post)
    {
      $wp_entity = new Post($data);
      $this->wpEntity = $wp_entity;
      $wp_entity->setConstructor($this);
      return $wp_entity;
    }
    return null;
  }

  protected function buildCollection( array $data )
  {
    $collection = new ArrayCollection;

    foreach( $data as $single ) {
      $constructor = new static($single);
      $wp_entity = $constructor->get();
      if( $wp_entity instanceof WpEntity ) {
        $collection->add($wp_entity);
      }
    }

    if($collection->isEmpty())
      return null;

    return $collection;
  }

  protected function build($data)
  {
    $built = $this->getEmptyWP_Post();

    switch(true) {
      case is_numeric($data):
        $built->ID = $data;
      break;
      default:
        // fail
        $built = null;
      break;
    }
    return $built;
  }

  protected function retrieve( $data )
  {
    $retrieved = null;

    switch(true) {
      case is_numeric($data):
        $retrieved = get_post($data);
      break;
      default:
      break;
    }
    return $retrieved;
  }

  protected function getEmptyWP_Post()
  {
    $post = new \WP_Post((object)['ID' => 0]);
    foreach ( get_object_vars( $post ) as $key => $value )
    {
      $post->$key = new NotLoadedProperty;
    }
    return $post;
  }
}
