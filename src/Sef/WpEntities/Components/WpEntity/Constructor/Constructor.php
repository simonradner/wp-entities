<?php
namespace Sef\WpEntities\Components\WpEntity\Constructor;       
use Sef\WpEntities\Interfaces\WpEntityConstructorInterface;       


abstract class Constructor implements WpEntityConstructorInterface {
  
  protected $data;

  protected $wpEntity = null;  
  
  final public function __construct( $data = null ) 
  {
    $this->data = $data;
  }
  
  final public function getConstructorData()
  {
    return $this->data;
  }

  public function setData($data)
  {
    // clear
    $this->wpEntity = null;
    
    // set
    $this->data = $data;
    return $this;
  }
      
  /**
   * get function.
   * 
   * @access public
   * @final
   * @return null | Arraycollection (WpEntity) | WpEntity
   */
  final public function get()
  {
    // check for loaded data first
    if($this->wpEntity)
    {
      return $this->wpEntity;
    }
    
    $wpEntity = null;
    
    $wpEntity = $this->wrapData( $this->data );
    if( is_array( $this->data ) ) {
      $wpEntity = $this->buildCollection( $this->data );
    }
    
    if( null === $wpEntity ) {
      $buildData = $this->build( $this->data ); 
      $wpEntity = $this->wrapData( $buildData );
    }

       
    $this->wpEntity = $wpEntity;
    return $this->wpEntity;
  }
  
  final public function load()
  {
    $loadData = $this->retrieve( $this->data );
    $wpEntity = $this->wrapData( $loadData );
    $this->wpEntity = $wpEntity;
    return $loadData;
  }

  abstract protected function wrapData( $data );

  abstract protected function build( $data );

  abstract protected function buildCollection( array $data );

  abstract protected function retrieve( $data );

}