<?php
namespace Sef\WpEntities\Components\WpEntity\Constructor;       
use Sef\WpEntities\Components\WpEntity\Term;

class TermConstructorByName extends AbstractTermConstructor {

  protected function build( $data ) 
  {
    $built = $this->getEmptyWP_Term();
    switch(true) {
      case is_integer($data):
        $built->term_id = $data;
      break;
      case is_numeric($data): //@TODO take care, difference between hirachical and non-hirachical
        $built->term_id = $data;
      break;
      case is_string($data):
        $built->name = $data;
      break;
      default:
        // fail
        $built = null;
      break; 
    }
    
    return $built;
  }

  protected function retrieve( $data ) 
  {  
    $retrieved = null;  

    switch(true) {
      case is_integer($data):
        $retrieved = $this->getTermById();
      break;
      case is_numeric($data): //@TODO take care, difference between hirachical and non-hirachical
        $retrieved = $this->getTermById();
      break;
      case is_string($data):
        $retrieved = $this->getTermByName();
      break;
      default:
      break; 
    }
    return $retrieved;
  } 
}
