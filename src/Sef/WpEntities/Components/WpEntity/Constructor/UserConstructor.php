<?php
namespace Sef\WpEntities\Components\WpEntity\Constructor;
use Sef\WpEntities\Components\WpEntity\Constructor\Constructor;
use Sef\WpEntities\Components\WpEntity\User;
use Sef\WpEntities\Components\WpEntity\WpEntity;
use Doctrine\Common\Collections\ArrayCollection;

class UserConstructor extends Constructor {

  protected function wrapData( $data )
  {
    if ( $data instanceof \WP_User)
    {
      $wp_entity = new User($data);
      $this->wpEntity = $wp_entity;
      $wp_entity->setConstructor($this);
      return $wp_entity;
    }
    return null;
  }

  protected function buildCollection( array $data )
  {
    $collection = new ArrayCollection;

    foreach( $data as $single ) {
      $constructor = new static($single);
      $wp_entity = $constructor->get();
      if( $wp_entity instanceof WpEntity ) {
        $collection->add($wp_entity);
      }
    }

    if($collection->isEmpty())
      return null;

    return $collection;
  }

  protected function build($data)
  {
    $built = $this->getEmptyWP_User();

    switch(true) {
      case is_integer($data):
        $built->ID = $data;
      break;
      case is_string($data):
        $built->data->login = $data;
      break;
      default:
        // fail
        $built = null;
      break;
    }
    return $built;
  }

  protected function retrieve( $data )
  {
    $retrieved = null;

    switch(true) {
      case is_integer($data):
        $retrieved = get_user_by('id', $data);
      break;
      case is_string($data):
        $retrieved = get_user_by('login', $data);
      break;
      default:
      break;
    }
    return $retrieved;
  }

  protected function getEmptyWP_User()
  {
    $user = new \WP_User();
    foreach ( get_object_vars( $user ) as $key => $value )
    {
      $user->$key = new NotLoadedProperty;
    }
    return $user;
  }
}
