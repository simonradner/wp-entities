<?php
namespace Sef\WpEntities\Components\WpEntity\Constructor;
use Sef\WpEntities\Components\WpEntity\Constructor\Constructor;
use Sef\WpEntities\Components\WpEntity\Comment;
use Sef\WpEntities\Components\WpEntity\WpEntity;
use Doctrine\Common\Collections\ArrayCollection;



class CommentConstructor extends Constructor {

  protected function wrapData( $data )
  {
    if ( $data instanceof \WP_Comment)
    {
      $wp_entity = new Comment($data);
      $this->wpEntity = $wp_entity;
      $wp_entity->setConstructor($this);
      return $wp_entity;
    }
    return null;
  }

  protected function buildCollection( array $data )
  {
    $collection = new ArrayCollection;

    foreach( $data as $single ) {
      $constructor = new static($single);
      $wp_entity = $constructor->get();
      if( $wp_entity instanceof WpEntity ) {
        $collection->add($wp_entity);
      }
    }

    if($collection->isEmpty())
      return null;

    return $collection;
  }

  protected function build($data)
  {
    $built = $this->getEmptyWP_Comment();

    switch(true) {
      case is_numeric($data):
        $built->comment_ID = $data;
      break;
      default:
        // fail
        $built = null;
      break;
    }
    return $built;
  }

  protected function retrieve( $data )
  {
    $retrieved = null;

    switch(true) {
      case is_numeric($data):
		$data = absint($data);
        $retrieved = get_comment($data, OBJECT);
      break;
      default:
      break;
    }
    return $retrieved;
  }

  protected function getEmptyWP_Comment()
  {
    $comment = new \WP_Comment((object)['comment_ID' => null]);
    foreach ( get_object_vars( $comment ) as $key => $value )
    {
      $comment->$key = new NotLoadedProperty;
    }
    return $comment;
  }
}
