<?php
namespace Sef\WpEntities\Components\WpEntity\Constructor;
use Sef\WpEntities\Interfaces\WpEntityConstructorInterface;
use Sef\WpEntities\Interfaces\WpEntityTermConstructorInterface;
use Sef\WpEntities\Components\WpEntity\WpEntity;
use Sef\WpEntities\Components\WpEntity\Term;
use Doctrine\Common\Collections\ArrayCollection;


abstract class AbstractTermConstructor extends Constructor implements WpEntityConstructorInterface, WpEntityTermConstructorInterface {

  protected $taxonomy;

  public function setData($data)
  {
    // clear
    $this->taxonomy = null;

    return parent::setData($data);
  }

  public function getTaxonomy() {
    return $this->taxonomy;
  }

  public function setTaxonomy( $taxonomy = 'category' )
  {
    $this->taxonomy = $taxonomy;
    return $this;
  }

  protected function wrapData( $data )
  {
    if ( $data instanceof \WP_Term)
    {
      $wp_entity = new Term($data);
      $this->wpEntity = $wp_entity;
      $wp_entity->setConstructor($this);
      return $wp_entity;
    }
    return null;
  }



  protected function buildCollection( array $data )
  {
    $collection = new ArrayCollection;

    foreach( $data as $wp_term ) {
      // only supports collection creation when a WP_Term object is given
      if( $wp_term instanceof \WP_Term ) {
        $constructor = new static($wp_term);
        $wp_entity = $constructor->setTaxonomy($wp_term->taxonomy)->get();
        if( $wp_entity instanceof WpEntity ) {
          $collection->add($wp_entity);
        }
      }
    }

    if($collection->isEmpty())
      return null;

    return $collection;
  }


  protected function getTermbyId()
  {
    return get_term_by('id', $this->data, $this->taxonomy);
  }

  protected function getTermbyName()
  {
    return get_term_by('name', $this->data, $this->taxonomy);
  }

  protected function getTermbySlug()
  {
    return get_term_by('slug', $this->data, $this->taxonomy);
  }

  // not implemented
  protected function retrieve($data)
  {
    return $data;
  }

  protected function getEmptyWP_Term()
  {
    $term = new \WP_Term((object)['term_id' => 0]);
    foreach ( get_object_vars( $term ) as $key => $value )
    {
      $term->$key = new NotLoadedProperty;
    }
    return $term;
  }
}
