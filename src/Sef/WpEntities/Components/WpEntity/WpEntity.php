<?php
namespace Sef\WpEntities\Components\WpEntity;

use Sef\WpEntities\Interfaces\WpEntityInterface;
use Sef\WpEntities\Interfaces\WpEntityConstructorInterface;
use Sef\WpEntities\Components\WpEntity\Constructor\NotLoadedProperty;


abstract class WpEntity implements WpEntityInterface {

  protected $constructor = null;

  protected $initialObject = null;

  protected $loadedObject = null;

  public function setConstructor(WpEntityConstructorInterface $constructor)
  {
    $this->constructor = $constructor;
  }

  public function getId() {
    return NULL;
  }

  public function getProperty($property) {
    return $this->lazyLoadProperty($property);
  }

  protected function lazyLoadProperty($property)
  {
    if ($this->loadedObject)
    {
      return $this->loadedObject->$property;
    }

    if( $this->initialObject->$property instanceof NotLoadedProperty )
    {
      if( $this->constructor )
      {
        $loadedObject = $this->constructor->load();
        if( ! $loadedObject )
          return null;

        if( !isset($loadedObject->$property))
          return null;

        $this->loadedObject = $loadedObject;

        return $loadedObject->$property;
      }
    }
    return $this->initialObject->$property;
  }

  public function getRawObject()
  {
    if($this->constructor)
    {

      if($this->loadedObject)
        return $this->loadedObject;

      // no loaded object
      // check if initial object is complete
      $objectVars = get_object_vars($this->initialObject);
      foreach( $objectVars as $key => $objectVar )
      {
        if( $objectVar instanceof NotLoadedProperty )
        {
          $this->loadedObject = $this->constructor->load();
          return $this->loadedObject;
          break;
        }
      }
    }
    // the initial object is
    return $this->initialObject;
  }
}
