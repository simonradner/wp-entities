<?php
namespace Sef\WpEntities\Components\NamingStrategy;
use Sef\WpEntities\Interfaces\NamingStrategyInterface;


class CamelCase2SnakeCaseNamingStrategy extends AbstractNamingStrategy
{
  public function translateName()
  {
    $name = $this->name;

    // http://stackoverflow.com/questions/1993721/how-to-convert-camelcase-to-camel-case
    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $name, $matches);
    $ret = $matches[0];
    foreach ($ret as &$match) {
      $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
    }
    return implode('_', $ret);
  }
}
