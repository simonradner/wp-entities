<?php
namespace Sef\WpEntities\Components\NamingStrategy;
use Sef\WpEntities\Interfaces\NamingStrategyInterface;


class CamelCase2SnakeCasePrefixUnderscoreNamingStrategy extends AbstractNamingStrategy
{
  public function translateName()
  {
    $name = $this->name;

    $strategy1 = new CamelCase2SnakeCaseNamingStrategy();
    $strategy1->setName($name);
    $name = $strategy1->translateName();
    $strategy2 = new PrefixUnderscoreNamingStrategy();
    $strategy2->setName($name);
    $name = $strategy2->translateName();
    return $name;
  }
}
