<?php
namespace Sef\WpEntities\Components\NamingStrategy;
use Sef\WpEntities\Interfaces\NamingStrategyInterface;


abstract class AbstractNamingStrategy implements NamingStrategyInterface
{
  protected $name;

  public function setName( $name )
  {
    $this->name = $name;
  }

  public function canTranslate()
  {
    return true;
  }
}
