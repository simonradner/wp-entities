<?php
namespace Sef\WpEntities\Components\NamingStrategy;
use Sef\WpEntities\Interfaces\NamingStrategyInterface;


class PrefixUnderscoreNamingStrategy extends AbstractNamingStrategy
{
  public function translateName()
  {
    $name = $this->name;
    return '_' . $name;
  }
}
