<?php
namespace Sef\WpEntities\Components;
use Sef\WpEntities\Annotation;
use Sef\WpEntities\Interfaces\PropertyInterface;


class EntityProperty implements PropertyInterface {

  // @TODO refactor: no public properties, use the interface for public access

  public $name;

  public $type = null;

  public $entityClass = null;

  public $repositoryClass = null;

  protected $wpName;

  protected $options;

  protected $classOptions;

  public function __construct(  \ReflectionProperty $property, Annotation\Options $propertyOptions, Annotation\Options $classOptions, Annotation\Groups $groups  )
  {
    $this->name = $property->getName();

    $this->options = $propertyOptions;
    $this->classOptions = $classOptions;

    $this->groups = $groups;

    $this->type = $propertyOptions->get('type');

    // if( $options instanceof  Annotation\PostOptions ) {
      $this->entityClass = $propertyOptions->get('entity');
      $this->repositoryClass = $propertyOptions->get('repository');
    // }
  }

  public function getOptions()
  {
    return $this->options;
  }

  public function getOption($key)
  {
    return $this->options->get($key);
  }

  public function getClassOptions()
  {
    return $this->classOptions;
  }

  public function getName()
  {
    return $this->name;
  }

  public function getWpName()
  {
    // explicitly set
    if( $this->getOption('wpname'))
    {
      return $this->getOption('wpname');
    }

    // the class has a naming strategy
    if($strategyClass = $this->classOptions->get('wpNamingStrategy'))
    {
      $reflector = new \ReflectionClass($strategyClass);
      if($reflector->implementsInterface('Sef\\WpEntities\\Interfaces\\NamingStrategyInterface'))
      {
          $strategy = new $strategyClass();
          $strategy->setName($this->getName());
          if ( $strategy->canTranslate())
          {
            return $strategy->translateName();
          }
      }
    }

    // default
    return $this->getName();
  }

  public function getType()
  {
    return $this->type;
  }

  public function getEntityClass()
  {
    return $this->options;
  }

  public function getRepositoryClass()
  {
    return $this->repositoryClass;
  }

  public function getGroups()
  {
    return $this->groups;
  }

  public function getterName()
  {
    return 'get' . ucfirst($this->getName());
  }

  public function setterName()
  {
    return 'set' . ucfirst($this->getName());
  }
}
