<?php
namespace Sef\WpEntities\Components\OverloadAccessors;       
use Sef\WpEntities\Interfaces\OverloadAccessorInterface;       
use Sef\WpEntities\Interfaces\OverloadAccessorable;       
use Sef\WpEntities\Base\EntityBag;       
use Sef\WpEntities\Components\Util\OverloadArrayCollection;       


class OverloadResult {
  
  public $success = false;

  public $value = null;

}