<?php
namespace Sef\WpEntities\Components\OverloadAccessors;       
use Sef\WpEntities\Interfaces\OverloadAccessorInterface;       
use Sef\WpEntities\Interfaces\OverloadAccessorable;       
use Sef\WpEntities\Base\EntityBag;       
use Sef\WpEntities\Components\Util\OverloadArrayCollection;       


class GetOverloadAccessor implements OverloadAccessorInterface {
    
  public function overloadAccessors( OverloadAccessorable $instance, $name, $args )
  {
    $result = new OverloadResult();
    $getter = 'get' . ucfirst($name);
    
    if( is_callable([$instance, $getter ]) )
    {
      $result->success = true;
      $result->value = $instance->$getter();

      return $result;
    }

    return $result;
  }
}