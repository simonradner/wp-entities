<?php
namespace Sef\WpEntities\Components\Importer;
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Interfaces\ImporterStrategyInterface;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Doctrine\Common\Collections\ArrayCollection;

class SetterImporterStrategy implements ImporterStrategyInterface {

  protected $args = [];

  public function __construct( array $args = [] )
  {
    $this->args = $args;
  }

  public function import( $data, EntityBag $to )
  {
    if( ! is_array( $data ))
    {
      // cant import anything
      return;
    }

    foreach( $to->config->getProperties() as $property )
    {
      $setter = $property->setterName();
      if( array_key_exists( $property->getWpName(), $data ) ) {
        // do import
        $to->$setter($data[$property->getWpName()]);
      }
    }
  }
}
