<?php
namespace Sef\WpEntities\Components\Importer;

use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Interfaces\ImporterStrategyInterface;
use Sef\WpEntities\Interfaces\ImporterInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Exporter class.
 *
 * imports data into the entity Instance
 */
class Importer implements ImporterInterface {

  public $entityBag;

  protected $strategy = null;


  public function __construct( $data, EntityBag $entityBag )
  {
    $this->data = $data;
    $this->entityBag = $entityBag;
    $this->strategy = new SetterImporterStrategy; // default
  }

  public function setStrategy( ImporterStrategyInterface $strategy )
  {
    $this->strategy = $strategy;
  }

  public function import()
  {
    $entityBag = $this->entityBag;
    $strategy = $this->strategy;
    $data = $this->data;

    return $strategy->import( $data, $entityBag );
  }
}
