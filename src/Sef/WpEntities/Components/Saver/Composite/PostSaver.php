<?php
namespace Sef\WpEntities\Components\Saver\Composite;       
use Sef\WpEntities\Components\Saver\Saver;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Interfaces\SaverInterface;
use Sef\WpEntities\Base\EntityBag;

use Doctrine\Common\Collections\ArrayCollection;


// composite


class PostSaver extends Saver {
  

  public function save() 
  {
    
    // exit early when no values where set    
    if ( false === $this->state->wasSet())
    {
      return $this->getRawPropertyValue('id');
    }
    
//     $late_save_functions = []; 
 
 
    $post = array(
      'ID'            => $this->getRawPropertyValue('id'),
      'post_title'    => $this->getRawPropertyValue('rawTitle'),
      'post_content'  => $this->getRawPropertyValue('rawContent'),
      'post_status'   => $this->getRawPropertyValue('status'),
      'post_type'     => $this->getRawPropertyValue('type'),
      'post_author'   => $this->getRawPropertyValue('author'),
      'post_date'     => $this->getRawPropertyValue('date'),
      'post_date_gmt' => $this->getRawPropertyValue('dateGmt'),
      'post_excerpt'  => $this->getRawPropertyValue('rawExcerpt'),
      'post_name'     => $this->getRawPropertyValue('name'),
      'post_parent'   => $this->getRawPropertyValue('parent'),
      'menu_order'    => $this->getRawPropertyValue('menuorder')
    );
    $post = array_filter($post, function($value){
      return (null !== $value);
    });    



    if($this->entityConfig->getPropertiesByType('meta'))
    {
      $post['meta_input'] = []; 
      foreach ($this->entityConfig->getPropertiesByType('meta') as $property ) {
        if( ! $property->getOptions()->hasSaver() && $this->state->wasSet($property) ) 
        {
          $post['meta_input'][$property->getWpName()] = $this->entity->_getProperty($property);         
        }
      }
    }
    
    // Insert the post into the database
    if(isset($post['ID']) && $post['ID'] && is_numeric($post['ID']) )
    {
      $result = wp_update_post( $post, true ); // return WP_Error     
    } else {
      $result = wp_insert_post( $post, true ); // return WP_Error     
    }

    if( $result instanceof \WP_Error )
    {
      return false;
    }
    
    $id = $result;
    
    // other saver
    foreach($this->properties as $property ) 
    {
      if($property->getOptions()->hasType('meta') && ! $property->getOptions()->hasSaver())
      {
        continue; // we saved that before
      }
      
      $propertyValue = $this->entity->_getProperty($property);         
      if( null !== $propertyValue && $this->state->wasSet($property))
      {
        $saver = Saver::factory( $this->entity, $this->state, $id, $property );
        if($saver instanceof SaverInterface) 
        {  
          $saver->save();
        }
      }
    }

    return $id; 

    
    // sort saver
    
    
    // execute saver
    
  }  

  
}
