<?php
namespace Sef\WpEntities\Components\Saver\Composite;       
use Sef\WpEntities\Components\Saver\Saver;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Interfaces\SaverInterface;
use Sef\WpEntities\Base\EntityBag;

use Doctrine\Common\Collections\ArrayCollection;


// composite


class TermSaver extends Saver {
  

  public function save() 
  {
    
    // exit early when no values where set    
    if ( false === $this->state->wasSet())
    {
      return $this->getRawPropertyValue('id');
    }

    $result = false;

    if( !$result && $this->getRawPropertyValue('id') && term_exists( $this->getRawPropertyValue('id'), $this->getRawPropertyValue('taxonomy'), $this->getRawPropertyValue('parent') ) ) {
      $result = $this->update();
    }    
    if( !$result && $this->getRawPropertyValue('name') && term_exists( $this->getRawPropertyValue('name'), $this->getRawPropertyValue('taxonomy'), $this->getRawPropertyValue('parent') ) ) {
      $result = $this->update();
    }  

    if( !$result && ! $this->getRawPropertyValue('id')) {
      $result = $this->insert();
    }
    
    if(!$result)
      return false;
    
    $id = $result;  
    
    // other saver
    foreach($this->properties as $property ) 
    {
      $propertyValue = $this->entity->_getProperty($property);         
      if( null !== $propertyValue && $this->state->wasSet($property)) 
      {
        $saver = Saver::factory( $this->entity, $this->state, $id, $property );
        if( $saver instanceof SaverInterface ) 
        {
          $saver->save();
        }  
      }
    }
    return $id; 

    
    // sort saver
    
    
    // execute saver
    
  }  




  protected function insert() 
  {
    if ( $this->getRawPropertyValue('id'))
      return false;
    
    // cannot happen in fact
    if ( ! $this->getRawPropertyValue('taxonomy'))
      return false;

    $name = $this->getRawPropertyValue('name');
    $slug = $this->getRawPropertyValue('slug');
    
    // use the slug as name also if no name is given
    if( ! $name && $slug)
      $name = $slug;
    
    $result = wp_insert_term(
      $name, 
      $this->getRawPropertyValue('taxonomy'),
      array(
        'description'=> $this->getRawPropertyValue('description') ?: '',
        'slug' => $slug,
        'parent'=> $this->getRawPropertyValue('parent')
      )
    );     

    if( $result instanceof \WP_Error)
      return false;
       
    return $result['term_id'];       
  }

  protected function update() 
  {
    if (! $this->getRawPropertyValue('id'))
      return false;
    
    // cannot happen in fact
    if ( ! $this->getRawPropertyValue('taxonomy'))
      return false;
    
    $updateValues = [];
    $coreProperties = [
      'name',
      'slug',
      'description',
      'parent'
    ];
  
    foreach( $coreProperties as $corePropertyName ) {
      $property = $this->entityConfig->getProperty($corePropertyName);
      if( $this->state->wasSet( $property ) ) {
        $updateValues[$corePropertyName] = $this->getRawPropertyValue($corePropertyName);
      } 
    }

    $result = wp_update_term(
      $this->getRawPropertyValue('id'),
      $this->getRawPropertyValue('taxonomy'),
      $updateValues
    );     

    if( $result instanceof \WP_Error)
      return false;
    
    return $result['term_id'];       

  }  
}