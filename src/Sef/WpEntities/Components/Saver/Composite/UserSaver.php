<?php
namespace Sef\WpEntities\Components\Saver\Composite;    

use Sef\WpEntities\Components\Saver\Saver;
use Sef\WpEntities\Interfaces\SaverInterface;
use Sef\WpEntities\Base\EntityBag;

use Doctrine\Common\Collections\ArrayCollection;


// composite


class UserSaver extends Saver {
  
  private $userMetaAlreadyHandeled = [
    'first_name',
    'last_name',
    'nickname',
    'description',
    'rich_editing',
    'locale'
  ];

  public function save() 
  {
    
    // exit early when no values where set    
    if ( false === $this->state->wasSet())
    {
      return $this->getRawPropertyValue('id');
    }
 
    $userdata = array(
      'ID'            => $this->getRawPropertyValue('id'),
      'user_pass'     => $this->getRawPropertyValue('pass'),
      'first_name'    => $this->getRawPropertyValue('firstName'),
      'last_name'     => $this->getRawPropertyValue('lastName'),
      'user_login'    => $this->getRawPropertyValue('login'),
      'user_nicename' => $this->getRawPropertyValue('nicename'),
      'user_email'    => $this->getRawPropertyValue('email'),
      'display_name'  => $this->getRawPropertyValue('displayName'),
      'nickname'      => $this->getRawPropertyValue('nickname'),
      'user_url'      => $this->getRawPropertyValue('url'),
      'description'   => $this->getRawPropertyValue('description'),
      'user_registered'    => $this->getRawPropertyValue('registered'), //  Y-m-d H:i:s
      'rich_editing'    => $this->getRawPropertyValue('richEditing'), // any string for true
      'role'    => $this->getRawPropertyValue('role'),
      'locale'  => $this->getRawPropertyValue('locale'), 
    );

    $userdata = array_filter($userdata, function($value){
      return (null !== $value);
    });    

    // Insert the user into the database
    if(isset($userdata['ID']) && $userdata['ID'] && is_numeric($userdata['ID']) )
    {
      $result = wp_update_user( $userdata ); // return WP_Error     
    } else {
      $result = wp_insert_user( $userdata ); // return WP_Error     
    }
    
    if( $result instanceof \WP_Error )
    {
      return $result;
    }
    
    $id = $result;

    // other saver
    foreach($this->properties as $property ) 
    {
      if ( $this->state->wasSet($property) ) {

        $propertyValue = $this->entity->_getProperty($property);         
        // meta and no saver
        if( $property->getOptions()->hasType('meta') && 
            ! $property->getOptions()->hasSaver() &&
            ! in_array( $property->getWpName(), $this->userMetaAlreadyHandeled )
        ){          
          if( $propertyValue === null ) {
            delete_user_meta( $id, $property->getWpName() );
            
          } else {
            update_user_meta( $id, $property->getWpName(), $propertyValue );
          }
        }

        // an explicit saver
        if( null !== $propertyValue )
        {
          $saver = Saver::factory( $this->entity, $this->state, $id, $property );
          if($saver instanceof SaverInterface) 
          {  
            $saver->save();
          }
        }
      }
    }

    return $id; 
    
  }  
}
