<?php
namespace Sef\WpEntities\Components\Saver\Composite\Post;
use Sef\WpEntities\Components\Saver\Saver;
use Sef\WpEntities\Interfaces\TermEntityInterface;
use Sef\WpEntities\Interfaces\PostEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;


class AttachedEntitiesViaNonHierarchicalTaxonomySaver extends Saver {

  public function save()
  {

    if(false === $this->state->wasSet($this->property))
      return;

    $repositoryClass = $this->property->getOptions()->get('repository');

    $repository = $repositoryClass::make();
    $this->repository = $repository;
    $targetRepositoryReflector = new \ReflectionClass($repositoryClass);

    $property = $this->property;
    $entity = $this->entity;
    $propertyValue = $this->entity->_getProperty($property);

    // support collections of PostEntities
    if( $targetRepositoryReflector->isSubclassOf('Sef\\WpEntities\\Base\\PostRepository'))
    {
      if( $propertyValue->isEmpty() )
      {
        wp_delete_object_term_relationships( $this->id, $property->getWpName() );
      }

      else if( $propertyValue instanceof ArrayCollection && $propertyValue->forAll(function($key, $element){
        return ( $element->getInstance() instanceof PostEntityInterface ) ? true : false; // check if all items are postEntities
      })) {

        // saving
        $entitiesToSave = $propertyValue->filter(function($element){
          if ( ! $element->getInstance() instanceof PostEntityInterface )
          {
            return false;
          }
          if( ! $element->exists() )
          {
            return false;
          }

          return true;
        });
        $idsToSave = [];

        foreach( $entitiesToSave as $entity )
        {
          $idsToSave[] = (string)$entity->getId();
        }
        wp_set_object_terms( $this->id, $idsToSave, $property->getWpName(), false );

      }
    }
    // no support for collections of TermEntities

  }
}
