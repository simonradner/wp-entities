<?php
namespace Sef\WpEntities\Components\Saver\Composite\Post;       
use Sef\WpEntities\Components\Saver\Saver;
use Sef\WpEntities\Interfaces\TermEntityInterface;
use Sef\WpEntities\Interfaces\PostEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;


// composite


class AttachedEntitiesSaver extends Saver {
  
  protected $repository = null;
  
  public function save() 
  {
    
    if(false === $this->state->wasSet($this->property))
      return;
    
    $repositoryClass = $this->property->getOptions()->get('repository');     
    
    $repository = $repositoryClass::make();
    $this->repository = $repository;
    $targetRepositoryReflector = new \ReflectionClass($repositoryClass);

    $property = $this->property;
    $entity = $this->entity;
    $propertyValue = $this->entity->_getProperty($property);         



    // a collection of post
    if( $targetRepositoryReflector->isSubclassOf('Sef\\WpEntities\\Base\\PostRepository')) 
    {      
      if( $propertyValue instanceof ArrayCollection && $propertyValue->forAll(function($key, $element){
        return ( $element->getInstance() instanceof PostEntityInterface ) ? true : false; // check if all items are termEntities
      })) {
        $this->persistPosts($this->id, $property, $propertyValue);  
      } 
    }
    
    // a collections of terms
    if( $targetRepositoryReflector->isSubclassOf('Sef\\WpEntities\\Base\\TermRepository')) 
    {

      if( $propertyValue instanceof ArrayCollection && $propertyValue->forAll(function($key, $element){
        return ( $element->getInstance() instanceof TermEntityInterface ) ? true : false; // check if all items are termEntities
      })) {
        $this->persistTerms($this->id, $property, $propertyValue);  
      }
    }
  }


  protected function persistTerms($entityId, $property, ArrayCollection $propertyValue  ) 
  {
    
    // clear all
    if($propertyValue->count() === 0) {
      wp_delete_object_term_relationships( $entityId, $this->repository->getTaxonomies());        
      return;
    }
    
    // or
    
    $terms = $propertyValue;
    $taxonomies = [];
    foreach ($terms as $term ) {
      if( ! isset($taxonomies[$term->getTaxonomy()])) {
        $taxonomies[$term->getTaxonomy()] = [];
      }
      $taxonomies[$term->getTaxonomy()][] = $term->getId();
    }
    foreach( $taxonomies as $taxonomy => $ids ) {
      wp_set_object_terms( $entityId, $ids, $taxonomy, $append = false );            
    }
  }

  protected function persistPosts($entityId, $property, ArrayCollection $propertyValue  ) 
  {
    $mapping = $this->property->getOptions()->get('mapping');
    if( in_array($mapping, ['inverse:one', 'inverse:many']))
    {
      // we dont save inversed sides
      return;
    }
    
    $posts = $propertyValue;
    $postsIds = [];
    foreach ($posts as $postEntity ) {
      $postsIds[] = $postEntity->getId();
    }
    
    $hasMultiplemeta = ($this->property->getOptions()->get('mapping') === 'bi') ? true : false;
    if( $mapping === 'bi' )
    {
      // delete all keys of the meta
      delete_post_meta($entityId, $property->getWpName());
      foreach ($postsIds as $postsId ) {
        add_post_meta($entityId, $property->getWpName(), $postsId, false );    
      }      
    } 
    else if ( $mapping === 'uni' ) 
    {
      update_post_meta($entityId, $property->getWpName(), $postsIds);    
    }
  }  
}

