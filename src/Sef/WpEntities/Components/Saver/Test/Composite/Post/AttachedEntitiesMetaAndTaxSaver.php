<?php
namespace Sef\WpEntities\Components\Saver\Test\Composite\Post;
use Sef\WpEntities\Components\Saver\Saver;
use Sef\WpEntities\Interfaces\TermEntityInterface;
use Sef\WpEntities\Interfaces\PostEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;

use Sef\WpEntities\Components\Saver\Composite\Post\AttachedEntitiesSaver;


class AttachedEntitiesMetaAndTaxSaver extends Saver {


  public function save()
  {
    $entity = $this->entity;
    $state = $this->state;
    $id = $this->id;
    $property = $this->property;

    if(false === $this->state->wasSet($this->property))
      return;

    // the normal way via post meta
    $normalSaver = new AttachedEntitiesSaver( $entity, $state, $id, $property );
    $normalSaver->save();


    $propertyValue = $this->entity->_getProperty($property);
    if( $propertyValue instanceof ArrayCollection  )
    {
      if( $propertyValue->isEmpty() )
      {
        wp_delete_object_term_relationships( $this->id, $property->getWpName() );
      } else {
        $entitiesToSave = $propertyValue->filter(function($element){
          if ( ! $element->getInstance() instanceof PostEntityInterface )
          {
            return false;
          }
          if( ! $element->exists() )
          {
            return false;
          }

          return true;
        });
        $idsToSave = [];

        foreach( $entitiesToSave as $entity )
        {
          $idsToSave[] = (string)$entity->getId();
        }
        wp_set_post_terms( $this->id, $idsToSave, $property->getWpName(), false );
      }
    }


  }
}
