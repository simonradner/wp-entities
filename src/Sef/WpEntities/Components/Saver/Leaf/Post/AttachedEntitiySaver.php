<?php
namespace Sef\WpEntities\Components\Saver\Leaf\Post;
use Sef\WpEntities\Components\Saver\Saver;
use Sef\WpEntities\Interfaces\TermEntityInterface;
use Sef\WpEntities\Interfaces\PostEntityInterface;
use Sef\WpEntities\Base\EntityBag;

// leaf


class AttachedEntitiySaver extends Saver {

  public function save()
  {

    $property = $this->property;

    if( false === $this->state->wasSet($property))
    {
      return;
    }

    $entity = $this->entity;
    $propertyValue = $this->entity->_getProperty($property);

    // deleting
    if( $propertyValue === false)
  	{
      $attachedEntityClass = $property->getOptions()->get('entity');
      $attachedEntityMock = $attachedEntityClass::make();

      // a post term
      if( $attachedEntityMock->getInstance() instanceof PostEntityInterface ){
        delete_post_meta($this->id, $property->getWpName());
      }

      // a term
      if( $attachedEntityMock->getInstance()  instanceof TermEntityInterface ){
        wp_delete_object_term_relationships( $this->id, $propertyValue->getTaxonomy() );
      }

      return;

    }

    if( ! $propertyValue instanceof EntityBag )
      return;


    // a post term
    if( $propertyValue->getInstance() instanceof PostEntityInterface ){
      update_post_meta($this->id, $property->getWpName(), $propertyValue->getId());
    }

    // a term
    if( $propertyValue->getInstance() instanceof TermEntityInterface ){
      wp_set_object_terms( $this->id, $propertyValue->getId(), $propertyValue->getTaxonomy(), $append = false );
    }
  }
}
