<?php
namespace Sef\WpEntities\Components\Saver\Leaf\Post;       
use Sef\WpEntities\Components\Saver\Saver;
use Sef\WpEntities\Interfaces\PostEntityInterface;
use Sef\WpEntities\Base\EntityBag;

// leaf


class FeaturedImageSaver extends Saver {

  public function save() 
  {
    $property = $this->property;
    
    if( false === $this->state->wasSet($property))
    {
      return;
    }
    
    $entity = $this->entity;
    $propertyValue = $this->entity->_getProperty($property);         
    
    $attachmentId = null;
    
    if( $propertyValue instanceof EntityBag && $propertyValue->getInstance() instanceof PostEntityInterface )
    {
      $attachmentId = $propertyValue->getId();
    }

    // depricated
    if( $propertyValue && is_numeric( $propertyValue ))
    {
      $attachmentId = $propertyValue->getId();
    }
    
    if( $attachmentId )
    {
      set_post_thumbnail( $this->id, $attachmentId );    
    }
  }
}