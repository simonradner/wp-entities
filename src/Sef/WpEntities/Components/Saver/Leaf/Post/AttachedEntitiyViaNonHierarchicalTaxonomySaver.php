<?php
namespace Sef\WpEntities\Components\Saver\Leaf\Post;
use Sef\WpEntities\Components\Saver\Saver;
use Sef\WpEntities\Interfaces\TermEntityInterface;
use Sef\WpEntities\Interfaces\PostEntityInterface;
use Sef\WpEntities\Base\EntityBag;

// leaf


class AttachedEntitiyViaNonHierarchicalTaxonomySaver extends Saver {

  public function save()
  {
    $property = $this->property;

    if( false === $this->state->wasSet($property))
    {
      return;
    }

    $entity = $this->entity;
    $propertyValue = $this->entity->_getProperty($property);

    // deleting
    if( $propertyValue === false) // in fact, false is a missleading convention to trigger a delete, better change to NULL ??
  	{
      $attachedEntityClass = $property->getOptions()->get('entity');
      $attachedEntityMock = $attachedEntityClass::make();

      // a post term
      //
      // wpName is the name of the intermediate taxonomy
      if( $attachedEntityMock->getInstance() instanceof PostEntityInterface ){
        wp_delete_object_term_relationships( $this->id, $property->getWpName() );

      }

      // a term
      if( $attachedEntityMock->getInstance()  instanceof TermEntityInterface ){
        // not supported
      }

      return;

    }

    if( ! $propertyValue instanceof EntityBag )
      return;


    // a post term
    if( $propertyValue->getInstance() instanceof PostEntityInterface ){

      // wpName is the name of the intermediate taxonomy

      // we have to pass the id as string, then it will be saved as name in the intermediate taxonomy
      $attachedPostIdAsString = (string)$propertyValue->getId();
      wp_set_object_terms( $this->id, [$attachedPostIdAsString],  $property->getWpName(), $append = false );
    }

    // a term
    if( $propertyValue->getInstance() instanceof TermEntityInterface ){
      // not supported
    }
  }
}
