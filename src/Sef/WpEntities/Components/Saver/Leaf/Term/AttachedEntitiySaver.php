<?php
namespace Sef\WpEntities\Components\Saver\Leaf\Term;       
use Sef\WpEntities\Components\Saver\Saver;
use Sef\WpEntities\Interfaces\TermEntityInterface;
use Sef\WpEntities\Interfaces\PostEntityInterface;


// leaf


class AttachedEntitiySaver extends Saver {

  public function save() 
  {
    
    $property = $this->property;
    
    if( false === $this->state->wasSet($property))
    {
      return;
    }
    
    $entity = $this->entity;
    $propertyValue = $this->entity->_getProperty($property);         


//     echo '<pre>'; print_r($propertyValue); echo '</pre>';    

    // a post term
    if( $propertyValue->getInstance() instanceof PostEntityInterface ){
      update_term_meta($this->id, $property->getWpName(), $propertyValue->getId());
    } 
    
    // a term
    
    // a term cannot have assigned terms
  }
}

