<?php
namespace Sef\WpEntities\Components\Saver\Leaf\Term;       
use Sef\WpEntities\Components\Saver\Saver;
use Sef\WpEntities\Interfaces\TermEntityInterface;
use Sef\WpEntities\Interfaces\PostEntityInterface;


// leaf


class MetaSaver extends Saver {

  public function save() 
  {

    $property = $this->property;
    
    if( false === $this->state->wasSet($property))
    {
      return;
    }
 
    $propertyValue = $this->entity->_getProperty($property);         
    update_term_meta($this->id, $property->getWpName(), $propertyValue);
  }
}

