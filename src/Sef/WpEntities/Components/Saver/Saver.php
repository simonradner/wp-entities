<?php
namespace Sef\WpEntities\Components\Saver;
use Sef\WpEntities\Interfaces\SaverInterface;       

use Sef\WpEntities\Base\Entity;
use Sef\WpEntities\Interfaces\PostEntityInterface;
use Sef\WpEntities\Interfaces\TermEntityInterface;
use Sef\WpEntities\Interfaces\UserEntityInterface;
use Sef\WpEntities\Components\State\EntityState;

use Sef\WpEntities\Interfaces\PropertyInterface;

use Doctrine\Common\Collections\ArrayCollection;


abstract class Saver implements SaverInterface {
  protected $properties;
  protected $property;
  protected $entity;
  protected $state;
  protected $id;

  public function __construct(Entity $entity, EntityState $state, $id = null, $property = null )
  {
    $this->entity = $entity;
    $this->state = $state;

    $entityConfig = $entity::getConfig();
    $this->entityConfig = $entityConfig;
    $this->id = $id;
    $this->properties = $entityConfig->getProperties();

    if ( $property instanceof PropertyInterface ) {
      $this->property = $property;
    }
  }

  public function save() {
    return NULL;
  }

  public static function factory(Entity $entity, EntityState $state, $id = null, $property = null )
  {

    if (null ===  $property)
    {

      switch(true)
      {
        case ($entity instanceof PostEntityInterface  ):
          return new Composite\PostSaver($entity, $state, $id);
        break;
        case ( $entity instanceof TermEntityInterface ):
          return new Composite\TermSaver($entity, $state, $id);
        case ( $entity instanceof UserEntityInterface ):
          return new Composite\UserSaver($entity, $state, $id);
        default:
          return false;
        break;
      }
    }
    if ( $property instanceof PropertyInterface && $explicitSaverClassname = $property->getOption('saver') )
    {
      return new $explicitSaverClassname( $entity, $state, $id, $property  );
    }

    if ( $property instanceof PropertyInterface && $property->getType() ) {
      if($entity instanceof PostEntityInterface)
      {
        switch($property->getType())
        {
          case ('meta'):
            // return new Leaf\Post\MetaSaver($entity, $id, $property );
          break;
          case ('entity'):
            return new Leaf\Post\AttachedEntitiySaver($entity, $state, $id, $property );
          break;
          case ('entities'):
            return new Composite\Post\AttachedEntitiesSaver($entity, $state, $id, $property );
          break;
          case ('featuredimage'):
            return new Leaf\Post\FeaturedImageSaver($entity, $state, $id, $property );
          break;
          case ('parent'):
             return new Leaf\Post\ParentSaver($entity, $state, $id, $property );
          break;
          case ('parentpost'): // depricated
            return new Leaf\NoSaver($entity, $state, $id, $property);
          break;
          default:
           // return null;
          break;
        }
      }
      if($entity instanceof TermEntityInterface)
      {
        switch($property->getType())
        {
          case ('meta'):
            return new Leaf\Term\MetaSaver($entity, $state, $id, $property );
          break;
          case ('entity'):
            return new Leaf\Term\AttachedEntitiySaver($entity, $state, $id, $property );
          break;
          case ('parent'):
             return new Leaf\Term\ParentSaver($entity, $state, $id, $property );
          break;
          default:
           // return null;
          break;
        }
      }
    }
    return null;
  }

  protected function getRawPropertyValue($propertyName)
  {
    $property = $this->entityConfig->getProperty($propertyName);
    if($property instanceof PropertyInterface) {
      return  $this->entity->_getProperty($property);
    }
  }
}
