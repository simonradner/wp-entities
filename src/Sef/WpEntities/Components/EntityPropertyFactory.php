<?php
namespace Sef\WpEntities\Components;
use Sef\WpEntities\Annotation;
use Sef\WpEntities\Components\Config\EntityConfig;
use Sef\WpEntities\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;


class EntityPropertyFactory {

  public static function get( EntityConfig $entityConfig )
  {
    $collection = new ArrayCollection;
    $classOptions = $entityConfig->options->get('classOptions');

    foreach($entityConfig->getReflectionProperties() as $property ) {

      if($property->isStatic())
        continue;

      // bad, as we dont know if $property really is a ReflectionProperty with method getName
      $propertyOptions = $entityConfig->options->get('propertyOptions')[$property->getName()];
      // bad, as annotatedOptions can return null

      $groupsInstance = $entityConfig->options->get('annotationReader')->getPropertyAnnotation($property, 'Sef\\WpEntities\\Annotation\\Groups');
      $groups = ($groupsInstance instanceof Groups ) ? $groupsInstance : new Groups();


      // each property is expected to have an option
      if($propertyOptions) {
        $property = new EntityProperty($property, $propertyOptions, $classOptions, $groups);
        $collection->set( $property->name, $property );
      }
    }
    return $collection;
  }
}
