<?php
namespace Sef\WpEntities\Components\Fetcher\Composite\Term;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;
use Doctrine\Common\Collections\ArrayCollection;

// composite

class AttachedEntitiesFetcher extends Fetcher {

  public function fetch()
  {
    $targetEntityClass = $this->property->getOptions()->get('entity');
    $repositoryClass = $this->property->getOptions()->get('repository');
    $repository = $repositoryClass::make();
    $targetRepositoryReflector = new \ReflectionClass($repositoryClass);

    // a collection of post
    if( $targetRepositoryReflector->isSubclassOf('Sef\\WpEntities\\Base\\PostRepository')) {
      $arrayCollection = $repository->findByIds(get_term_meta($this->wpEntity->getId(), $this->property->getWpName(), true ));
      return new FetchResult($this->property, $arrayCollection);
    }

    // a collections of terms
    if( $targetRepositoryReflector->isSubclassOf('Sef\\WpEntities\\Base\\TermRepository')) {
      $arrayCollection = $repository->findByIds(get_term_meta($this->wpEntity->getId(), $this->property->getWpName(), true ));
      return new FetchResult($this->property, null);
    }
  }
}
