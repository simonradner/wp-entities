<?php
namespace Sef\WpEntities\Components\Fetcher\Composite\User;

use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;
use Doctrine\Common\Collections\ArrayCollection;

// composite

class AttachedEntitiesFetcher extends Fetcher {

  public function fetch()
  {
    $targetEntityClass = $this->property->getOptions()->get('entity');
    $repositoryClass = $this->property->getOptions()->get('repository');

    $repository = $repositoryClass::make();
    $targetRepositoryReflector = new \ReflectionClass($repositoryClass);

    // a collection of post
    if( $targetRepositoryReflector->isSubclassOf('Sef\\WpEntities\\Base\\PostRepository')) {

      switch( $this->property->getOptions()->get('mapping') ) {
        case 'uni':
          $arrayCollection = $repository->findByIds(get_user_meta($this->wpEntity->getId(), $this->property->getWpName(), true ));
        break;
        case 'bi':
          $arrayCollection = $repository->findByIds(get_user_meta($this->wpEntity->getId(), $this->property->getWpName(), false ));
        break;
        case 'inverse:one':
        case 'inverse:many':


          // @TODO
          // implement (in repository)

          // throw error
          $arrayCollection = new ArrayCollection;

          // $arrayCollection = $repository->findByForeignKey($this->entity, $this->property->getWpName());
        break;
        default:
          // throw error
          $arrayCollection = new ArrayCollection;
        break;
      }

      return new FetchResult($this->property, $arrayCollection);
    }

    // collection of terms not supported yet
    return new FetchResult($this->property, new ArrayCollection);

  }
}
