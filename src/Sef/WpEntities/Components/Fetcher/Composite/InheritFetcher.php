<?php
namespace Sef\WpEntities\Components\Fetcher\Composite;
use Sef\WpEntities\Interfaces\FetcherInterface;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;
use Doctrine\Common\Collections\ArrayCollection;
use Sef\WpEntities\Components\Fetcher\Leaf\Post\InheritFetcher as PostInheritFetcher;
use Sef\WpEntities\Components\Fetcher\Leaf\Term\InheritFetcher as TermInheritFetcher;

// composite

final class InheritFetcher extends Fetcher {

  public function fetch()
  {
    $collection = new ArrayCollection;
    foreach($this->properties as $property )
    {
      $fetcher = Fetcher::factory( $this->entity, $this->wpEntity, $property );
      if($fetcher instanceof FetcherInterface && ( $fetcher instanceof PostInheritFetcher || $fetcher instanceof TermInheritFetcher) ) {
        $collection->add($fetcher->fetch());
      }
    }
    return $collection;
  }
}
