<?php
namespace Sef\WpEntities\Components\Fetcher\Composite\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;
use Doctrine\Common\Collections\ArrayCollection;
use Sef\WpEntities\Exceptions\FetchException;

// composite

class AttachedEntitiesViaNonHierarchicalTaxonomyFetcher extends Fetcher {

  public function fetch()
  {
    $targetEntityClass = $this->property->getOptions()->get('entity');
    $repositoryClass = $this->property->getOptions()->get('repository');

    $repository = $repositoryClass::make();
    $targetRepositoryReflector = new \ReflectionClass($repositoryClass);

    // a collection of post
    if( $targetRepositoryReflector->isSubclassOf('Sef\\WpEntities\\Base\\PostRepository')) {
      $taxonomy = $this->property->getWpName();
      if ( ! taxonomy_exists( $taxonomy ))
      {
        throw new FetchException( 'Taxonomy "' . $taxonomy . '" not exist.' );
      }

      // the term names represent the ids of the posts
      // term names are numeric strings, not integers
      $termNamesAsPostIds = wp_get_post_terms( $this->wpEntity->getId(), $taxonomy, [
        'fields' => 'names'
      ]);

      if( is_wp_error( $termNamesAsPostIds ) )
      {
        throw new FetchException( $termNamesAsPostIds->get_error_message());
      }

      // its an array!
      $arrayCollection = $repository->findByIds( $termNamesAsPostIds );

      return new FetchResult($this->property, $arrayCollection);
    } else {

      // only collections of Post Entities are supported

      // throw error
      return new FetchResult($this->property, new ArrayCollection);
    }
  }
}
