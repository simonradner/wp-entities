<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;
use Doctrine\Common\Collections\ArrayCollection;

// leaf

class AttachedTermNoChildrenFetcher extends Fetcher {

  public function fetch()
  {
    $targetEntityClass = $this->property->getOptions()->get('entity');
    $repositoryClass = $this->property->getOptions()->get('repository');
    $repository = $repositoryClass::make();
    $targetRepositoryReflector = new \ReflectionClass($repositoryClass);

    // term
    if( $targetRepositoryReflector->isSubclassOf('Sef\\WpEntities\\Base\\TermRepository')) {
      $entity = $repository->findOneByPostNoChildren($this->entity);
      return new FetchResult($this->property, $entity);
    }
  }
}
