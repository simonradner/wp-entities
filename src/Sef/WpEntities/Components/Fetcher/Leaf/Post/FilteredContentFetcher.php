<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;


// dont use this
// do not fetch a filtered value into a property

// instead filter the getter



class FilteredContentFetcher extends Fetcher {

  public function fetch()
  {
    global $post;
    $temp = $post;
    $post = $this->wpEntity->getRawObject();
    setup_postdata($post);
    $content = apply_filters('the_content', get_the_content());
    $content = str_replace(']]>', ']]&gt;', $content);
    //wp_reset_postdata();
    $post = $temp;
    return new FetchResult($this->property, $content );
  }
}
