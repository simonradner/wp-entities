<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;


class EditLinkFetcher extends Fetcher {

  public function fetch()
  {
    return new FetchResult($this->property, get_edit_post_link($this->wpEntity->getId(), 'display'));
  }
}
