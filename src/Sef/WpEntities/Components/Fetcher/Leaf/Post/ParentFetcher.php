<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;


class ParentFetcher extends Fetcher {

  public function fetch()
  {
    $value = null;
    if($this->wpEntity->getProperty('post_parent'))
    {
      // could be we have custom Entity
      $targetEntityClass = $this->property->getOptions()->get('entity');
      $entityClass = ( $targetEntityClass ) ? $targetEntityClass : get_class( $this->entity->getInstance());
      $value = $entityClass::make($this->wpEntity->getProperty('post_parent'));
    }
    return new FetchResult($this->property, $value );
  }
}
