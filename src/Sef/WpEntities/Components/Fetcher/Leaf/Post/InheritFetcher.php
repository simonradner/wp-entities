<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;


class InheritFetcher extends Fetcher {

  protected $map = [
    'author'    => 'post_author',
    'date'      => 'post_date',
    'dateGmt'   => 'post_date_gmt',
    'rawContent'   => 'post_content',
    'rawTitle'     => 'post_title',
    'rawExcerpt'    => 'post_excerpt',
    'status'    => 'post_status',
    'name'      => 'post_name',
    'menuorder'    => 'menu_order',
    'type'          => 'post_type',
    'commentcount'    => 'comment_count',
    'parent'		=> 'post_parent',
    'mimeType'  => 'post_mime_type'
  ];

  public function fetch()
  {
    $value = null;

    switch($this->property->name)
    {
      case 'id':
        $value = $this->wpEntity->getId();
      break;
      default:
        if( isset($this->map[$this->property->name]) )
        {
          $wp_object_property = $this->map[$this->property->name];
          $value = $this->wpEntity->getProperty($wp_object_property);
        }
      break;
    }
    return new FetchResult($this->property, $value);
  }
}
