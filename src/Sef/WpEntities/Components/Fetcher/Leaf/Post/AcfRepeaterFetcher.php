<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;



class AcfRepeaterFetcher extends Fetcher {
  public function fetch()
  {
    if( class_exists('acf') ) {
      $value = get_field($this->property->getWpName(), $this->wpEntity->getId());
    } else {
      $value = null;
    }
    return new FetchResult($this->property, $value);
  }
}
