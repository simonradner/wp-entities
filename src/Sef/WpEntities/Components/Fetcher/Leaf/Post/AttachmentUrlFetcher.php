<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;



class AttachmentUrlFetcher extends Fetcher {
  public function fetch()
  {
    $url = wp_get_attachment_url($this->wpEntity->getId());
    return new FetchResult($this->property, $url);
  }
}
