<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;


class FeaturedImageFetcher extends Fetcher {
  public function fetch()
  {
    $attachmentId = get_post_thumbnail_id($this->wpEntity->getId());
    if( ! $attachmentId)
      return new FetchResult($this->property, null);

    $targetEntityClass = $this->property->getOptions()->get('entity');
    $repositoryClass = $this->property->getOptions()->get('repository');
    $repository = $repositoryClass::make();
    $targetRepositoryReflector = new \ReflectionClass($repositoryClass);

    // a  post
    if( $targetRepositoryReflector->isSubclassOf('Sef\\WpEntities\\Base\\PostRepository')) {
      $entity = $repository->findById($attachmentId);
      return new FetchResult($this->property, $entity);
    }
    return new FetchResult($this->property, null);
  }
}
