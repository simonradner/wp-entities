<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;
use Sef\WpEntities\Entities\Util\Imagesize;
use Sef\WpEntities\Components\Util\OverloadArrayCollection;
use Doctrine\Common\Collections\ArrayCollection;
use Sef\WpEntities\Interfaces\ImagesizeInterface;

class AttachmentSizesFetcher extends Fetcher {

  public function fetch()
  {

    $imagesizesClass = $this->property->getOptions()->get('entity');
    $imagesizeClass = $this->property->getOptions()->get('childentity');

    $sizes = $this->property->getOptions()->get('size');

    $reflector = new \ReflectionClass( $imagesizesClass );
    if( ! $reflector->isSubclassOf('Sef\\WpEntities\\Interfaces\\ImagesizesInterface' ))
      return new FetchResult( $this->property, $collection );

    $reflector = new \ReflectionClass( $imagesizeClass ); // @TODO var not defined !!
    if( ! $reflector->isSubclassOf('Sef\\WpEntities\\Interfaces\\ImagesizeInterface' ))
      return new FetchResult( $this->property, $collection ); // @TODO var not defined !!

    $imagesizes = new $imagesizesClass;

    $availableSizes = get_intermediate_image_sizes();
    $availableSizes[] = 'full';
    $validSizes = array_intersect($availableSizes, $sizes );
    $sizes = ($sizes) ? array_intersect($availableSizes, $sizes ) : $availableSizes;

    foreach( $sizes as $size )
    {
      $imagesize = new $imagesizeClass;
      $imagesize->setAttachmentId( $this->wpEntity->getId() );
      $imagesize->setSize( $size );
      $imagesizes->setUpImagesize( $imagesize );
    }
    return new FetchResult( $this->property, $imagesizes );
  }
}
