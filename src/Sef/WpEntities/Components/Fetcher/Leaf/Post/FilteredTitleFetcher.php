<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;


// dont use this
// do not fetch a filtered value into a property

// instead filter the getter


class FilteredTitleFetcher extends Fetcher {

  public function fetch()
  {
    $value = get_the_title($this->wpEntity->getId());
    return new FetchResult($this->property, $value);
  }
}
