<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Components\Fetcher\FetchResult;
use Sef\WpEntities\Components\Fetcher\Composite\Post\AttachedEntitiesViaNonHierarchicalTaxonomyFetcher;

class AttachedEntityViaNonHierarchicalTaxonomyFetcher extends AttachedEntitiesViaNonHierarchicalTaxonomyFetcher {

  // uses the equivalent entities fetcher
  // returns the first result
  //
  public function fetch()
  {
    $fetcherResult = parent::fetch();
    // composites always return an ArrayCollection
    $collection = $fetcherResult->value;
    if( $collection->isEmpty() )
    {
      return new FetchResult($this->property, null );
    } else {
      return new FetchResult($this->property, $collection->first() );
    }
  }
}
