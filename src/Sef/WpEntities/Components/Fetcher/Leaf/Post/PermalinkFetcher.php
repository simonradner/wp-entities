<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;

class PermalinkFetcher extends Fetcher {

  public function fetch()
  {
    return new FetchResult($this->property, get_permalink($this->wpEntity->getId()));
  }
}
