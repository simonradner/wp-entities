<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;


// dont use this
// do not fetch a filtered value into a property

// instead filter the getter



class FilteredExcerptFetcher extends Fetcher {

  public function fetch()
  {
    global $post;
    $temp = $post;
    $post = $this->wpEntity->getRawObject();
    setup_postdata($post);
    $excerpt = get_the_excerpt();
    $post = $temp; // more reliable for recursive WP Loops
    //wp_reset_postdata();

    return new FetchResult($this->property, $excerpt );
  }
}
