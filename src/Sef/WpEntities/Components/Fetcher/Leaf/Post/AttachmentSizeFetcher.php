<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Post;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;
use Sef\WpEntities\Entities\Util\Imagesize;
use Doctrine\Common\Collections\ArrayCollection;
use Sef\WpEntities\Interfaces\ImagesizeInterface;

class AttachmentSizeFetcher extends Fetcher {

  public function fetch()
  {
    $imagesizeClass = $this->property->getOptions()->get('entity');
    $size = $this->property->getOptions()->get('size');

    $reflector = new \ReflectionClass( $imagesizeClass );
    if( ! $reflector->isSubclassOf('Sef\\WpEntities\\Interfaces\\ImagesizeInterface' ))
      return new FetchResult( $this->property, $collection );

    $availableSizes = get_intermediate_image_sizes();
    $availableSizes[] = 'full';

    $size = ( in_array($size, $availableSizes )) ? $size : 'full';

    return new FetchResult( $this->property, new $imagesizeClass($this->wpEntity->getId(), $size ) );
  }
}
