<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Term;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;

class ParentFetcher extends Fetcher {

  public function fetch()
  {
    $value = null;
    if($this->wpEntity->getProperty('parent'))
    {
      $entityClass = get_class($this->entity->getInstance());
      $value = $entityClass::make($this->wpEntity->getProperty('parent'));
    }
    return new FetchResult($this->property, $value );
  }
}
