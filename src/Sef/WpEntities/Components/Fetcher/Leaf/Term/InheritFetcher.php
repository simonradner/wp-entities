<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Term;
use Sef\WpEntities\Components\Fetcher\Leaf\Post\InheritFetcher as PostInheritFetcher;
use Sef\WpEntities\Interfaces\Fetchable;


class InheritFetcher extends PostInheritFetcher {

  protected $map = [
    'name'    => 'name',
    'slug'      => 'slug',
    'termGroup'  => 'term_group',
    'termTaxonomyId'  => 'term_taxonomy_id',
    'taxonomy'   => 'taxonomy',
    'description'   => 'description',
    'parent'     => 'parent',
    'count'    => 'count'
  ];

}
