<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Term;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;


class PermalinkFetcher extends Fetcher {

  public function fetch()
  {
    return new FetchResult($this->property, get_term_link($this->wpEntity->getId(), $this->wpEntity->getProperty('taxonomy') ));
  }
}
