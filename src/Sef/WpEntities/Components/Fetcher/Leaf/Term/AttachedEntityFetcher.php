<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Term;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;
use Doctrine\Common\Collections\ArrayCollection;

// leaf

class AttachedEntityFetcher extends Fetcher {

  public function fetch()
  {
    $targetEntityClass = $this->property->getOptions()->get('entity');
    $repositoryClass = $this->property->getOptions()->get('repository');
    $repository = $repositoryClass::make();
    $targetRepositoryReflector = new \ReflectionClass($repositoryClass);

    // a  post
    if( $targetRepositoryReflector->isSubclassOf('Sef\\WpEntities\\Base\\PostRepository')) {
      $entity = $repository->findById( get_term_meta($this->wpEntity->getId(), $this->property->getWpName(), true ));
      return new FetchResult($this->property, $entity);
    }

    // a term

    // cant be supported, a term cannot have assigned terms
  }
}
