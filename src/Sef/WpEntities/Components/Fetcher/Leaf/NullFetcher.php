<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;
use Doctrine\Common\Collections\ArrayCollection;


class NullFetcher extends Fetcher {

  public function fetch()
  {
      return new FetchResult($this->property, null);
  }
}
