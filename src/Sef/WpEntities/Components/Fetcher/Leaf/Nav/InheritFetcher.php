<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Nav;
use Sef\WpEntities\Components\Fetcher\Leaf\Post\InheritFetcher as PostInheritFetcher;
use Sef\WpEntities\Interfaces\Fetchable;


class InheritFetcher extends PostInheritFetcher {

  protected $map = [
    'dbId' => 'db_id',
    'menuItemParent' => 'menu_item_parent',
    'objectId' => 'object_id',
    'object' => 'object',
    'typeLabel' => 'type_label',
    'url' => 'url',
    'navItemTitle' => 'title',
    'target' => 'target',
    'attrTitle' => 'attr_title',
    'description' => 'description',
    'classes' => 'classes',
    'xfn' => 'xfn',
    'current' => 'current',
    'currentItemAncestor' => 'current_item_ancestor',
    'currentItemParent' => 'current_item_parent'
  ];

}
