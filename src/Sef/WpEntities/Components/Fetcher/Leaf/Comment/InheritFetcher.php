<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\Comment;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Components\Fetcher\Leaf\Post\InheritFetcher as PostInheritFetcher;

class InheritFetcher extends PostInheritFetcher {

  protected $map = [
    'postId'        => 'comment_post_ID',
    'authorName'    => 'comment_author',
    'authorEmail'   => 'comment_author_email',
    'authorUrl'     => 'comment_author_url',
    'authorIP'      => 'comment_author_IP',
    'date'          => 'comment_date',
    'dateGmt'       => 'comment_date_gmt',
    'content'       => 'comment_content',
    'karma'         => 'comment_karma',
    'approved'      => 'comment_approved',
    'agent'         => 'comment_agent',
    'type'          => 'comment_type',
    'parent'		    => 'comment_parent',
    'userId'		    => 'user_id'
  ];
}
