<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\User;

use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;



class MetaFetcher extends Fetcher {
  public function fetch()
  {
    $value = get_user_meta($this->wpEntity->getId(), $this->property->getWpName(), true);
    return new FetchResult($this->property, $value);
  }
}
