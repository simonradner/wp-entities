<?php
namespace Sef\WpEntities\Components\Fetcher\Leaf\User;

use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\FetchResult;


class InheritFetcher extends Fetcher {

  protected $map = [

    // in root
    'roles'    => 'roles',
    'caps'     => 'caps',
    'allCaps'     => 'allcaps',
    
    // in data
    'login'  => 'user_login',
    'pass'  => 'user_pass',
    'nicename'   => 'user_nicename',
    'email'   => 'user_email',
    'registered'     => 'user_registered',
    'activationKey'    => 'user_activation_key',
    'status'    => 'user_status',
    'displayName'    => 'display_name',
    'spam'    => 'spam',
    'deleted'    => 'deleted',
  ];

  public function fetch()
  {
    $value = null;
    switch($this->property->name)
    {
      case 'id':
        $value = $this->wpEntity->getId();
      break;
      case 'roles':
      case 'caps':
      case 'allCaps':  
        if( isset($this->map[$this->property->name]) )
        {
          $wp_object_property = $this->map[$this->property->name];
          $value = $this->wpEntity->getProperty($wp_object_property);
        }
      break;
      // in data
      default:
        if( isset($this->map[$this->property->name]) )
        {
          $wp_object_property = $this->map[$this->property->name];
          $data = $this->wpEntity->getProperty('data');
          if( isset($data->$wp_object_property) ) {
            $value = $data->$wp_object_property;
          }
        }
      break;
    }
    return new FetchResult($this->property, $value);
  }
}
