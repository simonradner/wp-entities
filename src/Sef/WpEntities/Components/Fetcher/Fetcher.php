<?php
namespace Sef\WpEntities\Components\Fetcher;
use Sef\WpEntities\Interfaces\FetcherInterface;
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Interfaces\PostEntityInterface;
use Sef\WpEntities\Interfaces\TermEntityInterface;
use Sef\WpEntities\Interfaces\CommentEntityInterface;
use Sef\WpEntities\Interfaces\UserEntityInterface;
use Sef\WpEntities\Interfaces\WpEntityInterface;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Sef\WpEntities\Components\Fetcher\FetchResult;

use Doctrine\Common\Collections\ArrayCollection;

abstract class Fetcher implements FetcherInterface
{
  protected $properties;

  protected $property;

  protected $wpEntity;

  protected $entity;

  public function __construct( EntityBag $entity, WpEntityInterface $wpEntity, $property = null )
  {
    $this->entity = $entity;
    $this->wpEntity = $wpEntity;
    $entityConfig = $entity->config;
    $this->properties = $entityConfig->getProperties();

    if ( $property instanceof PropertyInterface ) {
      $this->property = $property;
    }
  }

  public abstract function fetch();

  public final function fetchAsArrayCollection()
  {
    $result = $this->fetch();
    if( $result instanceof FetchResult ) {
      $arrayCollection = new ArrayCollection;
      $arrayCollection->add($result);
    } else {
      $arrayCollection = $result;;
    }
    return $arrayCollection;
  }

  public static function factory(EntityBag $entity, WpEntityInterface $wpEntity, $property = null )
  {


    if (null ===  $property)
    {
      switch(true)
      {
        case ($entity->getInstance() instanceof PostEntityInterface || $entity->getInstance() instanceof TermEntityInterface  ):
          return new Composite\EntityFetcher($entity, $wpEntity );
        break;
        default:
          return null;
        break;
      }
    }

    // we have a property
    if( false === $property->getOption('fetcher'))
    {
      return new Leaf\NullFetcher($entity, $wpEntity, $property);
    }

    if ( $property instanceof PropertyInterface && $explicitFetcherClassname = $property->getOption('fetcher') )
    {
      return new $explicitFetcherClassname( $entity, $wpEntity, $property  );
    }

    if ( $property instanceof PropertyInterface && $property->getType() ) {
      if( $entity->getInstance() instanceof PostEntityInterface )
      {
        switch($property->getType())
        {
          case ('meta'):
            return new Leaf\Post\MetaFetcher($entity, $wpEntity, $property );
          break;
          case ('entity'):
            return new Leaf\Post\AttachedEntityFetcher($entity, $wpEntity, $property );
          break;
          case ('entities'):
            return new Composite\Post\AttachedEntitiesFetcher($entity, $wpEntity, $property );
          break;
          case ('parent'):
            return new Leaf\Post\ParentFetcher($entity, $wpEntity, $property );
          break;
          case ('featuredimage'):
            return new Leaf\Post\FeaturedImageFetcher($entity, $wpEntity, $property );
          break;
          case ('attachmenturl'):
            return new Leaf\Post\AttachmentUrlFetcher($entity, $wpEntity, $property );
          break;
          case ('imagesize'):
            return new Leaf\Post\AttachmentSizeFetcher($entity, $wpEntity, $property );
          break;
          case ('imagesizes'):
            return new Leaf\Post\AttachmentSizesFetcher($entity, $wpEntity, $property );
          break;
          default:
           // return null;
          break;
        }
      }
      if($entity->getInstance() instanceof TermEntityInterface)
      {
        switch($property->getType())
        {
          case ('meta'):
            return new Leaf\Term\MetaFetcher($entity, $wpEntity, $property );
          break;
          case ('entity'):
            return new Leaf\Term\AttachedEntityFetcher($entity, $wpEntity, $property );
          break;
          case ('entities'):
            return new Composite\Term\AttachedEntitiesFetcher($entity, $wpEntity, $property );
          break;
          case ('parent'):
            return new Leaf\Term\ParentFetcher($entity, $wpEntity, $property );
          break;
          default:
           // return null;
          break;
        }
      }
      if($entity->getInstance() instanceof UserEntityInterface)
      {
        switch($property->getType())
        {
          case ('meta'):
            return new Leaf\User\MetaFetcher($entity, $wpEntity, $property );
          break;
          case ('entity'):
            return new Leaf\User\AttachedEntityFetcher($entity, $wpEntity, $property );
          case ('entities'):
            return new Composite\User\AttachedEntitiesFetcher($entity, $wpEntity, $property );
          break;
          default:
           // return null;
          break;
        }
      }
      if( 'entities' === $property->getType())
      {
        // depricated
        return null;
      }
      if( 'entity' === $property->getType())
      {
        // depricated
        return null;
      }
    }
    return null;
  }
}
