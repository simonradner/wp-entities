<?php
namespace Sef\WpEntities\Components\Fetcher;       
use Sef\WpEntities\Interfaces\PropertyInterface;       

class FetchResult {

  public $property;

  public $value;  

  public function __construct( PropertyInterface $property, $value = NULL ) 
  {
    $this->property = $property;
    $this->value = $value;
  }
}