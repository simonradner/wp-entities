<?php
namespace Sef\WpEntities\Components\Converter;       
use Doctrine\Common\Collections\ArrayCollection;

class ViennaDateTimeConverter extends Converter {
   
  /**
   * converting function.
   * 
   * @access protected
   * @param mixed $data
   * @return DateTime
   */
  protected function converting($data)
  {
	if( ! $data )
		return null;

    $dateTime = new \DateTime($data, new \DateTimeZone('Europe/Vienna'));
    return ($dateTime instanceof \DateTime) ? $dateTime : null;
  }

  /**
   * isConverted function.
   * 
   * @access protected
   * @param mixed $data
   * @return bool
   */
  protected function isConverted( $data )
  {
    return ($data instanceof \DateTime);
  }
}