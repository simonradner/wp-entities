<?php
namespace Sef\WpEntities\Components\Converter;       
use Doctrine\Common\Collections\ArrayCollection;

class Id2WpUserConverter extends Converter {
   
  /**
   * converting function.
   * 
   * @access protected
   * @param mixed $data
   * @return WP_User
   */
  protected function converting($data)
  {
    return get_user_by( 'id', $data); 
  }

  /**
   * isConverted function.
   * 
   * @access protected
   * @param mixed $data
   * @return bool
   */
  protected function isConverted( $data )
  {
    return ($data instanceof \WP_User );
  }
}