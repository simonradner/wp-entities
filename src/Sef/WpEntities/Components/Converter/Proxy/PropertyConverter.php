<?php
namespace Sef\WpEntities\Components\Converter\Proxy;

use Sef\WpEntities\Components\Converter\Converter;
use Sef\WpEntities\Interfaces\ConverterInterface;

use Sef\WpEntities\Base\EntityBone;
use Sef\WpEntities\Interfaces\PropertyInterface;



// NOT DEPRICATED



class PropertyConverter implements ConverterInterface {

  protected $entity;
  protected $property;
  
  public function __construct( EntityBone $entity, PropertyInterface $property, ConverterInterface $converter )
  {
    $this->entity = $entity;
    $this->property = $property;
    $this->converter = $converter;
  }

  public function setFlags($flags)
  {
    $this->converter->setFlags($flags);
    return $this;
  }

  public function setData( $data ) {
    $this->converter->setData($data);
    return $this;
  }

  /**
   * convert function.
   *
   * @access public
   * @param mixed $data
   * @return void
   */
  public function convert()
  {
    $convertedData = $this->converter->convert();

    // covert the property value

    if ( null === $convertedData )
    {
      $data = $this->entity->_getProperty($this->property);
      $this->setData( $data );
      $convertedData = $this->converter->convert();
    }

    return $convertedData;
  }
}
