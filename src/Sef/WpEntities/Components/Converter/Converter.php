<?php
namespace Sef\WpEntities\Components\Converter;
use Sef\WpEntities\Interfaces\ConverterInterface;
use Doctrine\Common\Collections\ArrayCollection;

abstract class Converter implements ConverterInterface {

  const NO_FLAGS = 0;

  const ARRAY_AS_COLLECTION = 1;

  const AS_COLLECTION = 2;

  protected $data = null;

  protected $flags = 0;

  public function setFlags($flags)
  {
    $this->flags = $flags;
    return $this;
  }

  public function setData( $data ) {
    $this->data = $data;
    return $this;
  }

  /**
   * convert function.
   *
   * @access public
   * @param mixed $data
   * @return void
   */
  public function convert()
  {
    $this->setup();

    if ( null === $this->data )
    {
      return;
    }

    $data = $this->data;

    if( $this->isConverted($data))
    {
      return $data;
    }

    if( $this->isCollection($data) )
    {
      return $this->convertingCollection($data);
    }

    return $this->converting($data);
  }

  /**
   * converting function.
   *
   * @access protected
   * @abstract
   * @param mixed $data
   * @return void
   */
  abstract protected function converting($data);

  /**
   * convertingCollection function.
   *
   * @access protected
   * @param array $data
   * @return void
   */
  protected function convertingCollection($data)
  {
    $collection = new ArrayCollection;
	
	//	@TODO check for array or Traversable	

    foreach( $data as $itemdata )
    {
      // var_dump($this->converting($item)->export());
      $item = $this->converting($itemdata);
      $collection->add( $item );
    }
    return $collection;
  }

  /**
   * isConverted function.
   *
   * @access protected
   * @param mixed $data
   * @return bool
   */
  protected function isConverted( $data )
  {
    return false;
  }

  /**
   * isConvertingAsCollection function.
   *
   * @access protected
   * @param mixed $data
   * @return bool
   */
  protected function isCollection( $data )
  {

    if( ( $this->flags & self::ARRAY_AS_COLLECTION ) && is_array($data))
    {
      return true;
    }

    if( $this->flags & self::AS_COLLECTION  )
    {
      if( ! is_array( $data ))
      {
        $array[] = $data;
        $data = $array;
      }
      return true;
    }

    return false;

  }

  protected function setup()
  {
    //
  }

}
