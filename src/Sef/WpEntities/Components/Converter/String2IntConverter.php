<?php
namespace Sef\WpEntities\Components\Converter;   
use Sef\WpEntities\Base\EntityBag;         
use Doctrine\Common\Collections\ArrayCollection;

class String2IntConverter extends Converter {
   
  protected function converting($data)
  {
    return absint($data); 
  }

  protected function isConverted( $data )
  {
    return (is_integer( $data ) && $data >= 0);
  }
}