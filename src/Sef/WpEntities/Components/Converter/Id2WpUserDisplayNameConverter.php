<?php
namespace Sef\WpEntities\Components\Converter;       
use Doctrine\Common\Collections\ArrayCollection;

class Id2WpUserDisplayNameConverter extends Converter {
   

  /**
   * converting function.
   * 
   * @access protected
   * @param mixed $data
   * @return string
   */
  protected function converting($data)
  {
    return get_the_author_meta( 'display_name', $data );
  }

  /**
   * isConverted function.
   * 
   * @access protected
   * @param mixed $data
   * @return bool
   */
  protected function isConverted( $data )
  {
    return false; 
  }
}