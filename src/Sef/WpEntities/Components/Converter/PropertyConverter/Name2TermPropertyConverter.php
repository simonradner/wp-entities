<?php
namespace Sef\WpEntities\Components\Converter\PropertyConverter;   
use Sef\WpEntities\Interfaces\TermEntityInterface;
use Sef\WpEntities\Base\EntityBag;        
use Sef\WpEntities\Components\WpEntity\Constructor\TermConstructorByName; 
use Doctrine\Common\Collections\ArrayCollection;

class Name2TermPropertyConverter extends PropertyConverter {
   
   protected $flags = self::ARRAY_AS_COLLECTION;
   
   protected $repository;
   
   protected $targetEntityClass;
  /**
   * converting function.
   * 
   * @access protected
   * @param mixed $data
   * @return WP_User
   */
  protected function converting($data)
  {
    $constructor = new TermConstructorByName();
    $constructor->setData($data);
    
    // get config of target Entity
    $targetClass = $this->targetEntityClass;
    $taxonomy = $targetClass::getConfig()->options->get('classOptions')->get('taxonomy');
    
    $constructor->setTaxonomy($taxonomy);
    $wpEntity = $constructor->get();    
    
    if($wpEntity) 
    {
     $t =  $targetClass::make($wpEntity);
//       $t->setName('ss');

//       echo '<pre>'; print_r($targetClass::make($wpEntity)); echo '</pre>';
      return $targetClass::make($wpEntity);
    } else {
      $newEntity = $targetClass::make();
      $newEntity->setName($data);
      return $newEntity;
    }
  }
  
  protected function setup()
  {
    if (null === $this->property)
      return;
     
    $this->targetEntityClass = $this->property->getOptions()->get('entity');    
  }

  /**
   * isConverted function.
   * 
   * @access protected
   * @param mixed $data
   * @return bool
   */
  protected function isConverted( $data )
  {
    return ($data instanceof ArrayCollection || ($data instanceof EntityBag && $data->getInstance() instanceof TermEntityInterface ) );
  }
}