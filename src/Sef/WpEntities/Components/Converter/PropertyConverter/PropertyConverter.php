<?php
namespace Sef\WpEntities\Components\Converter\PropertyConverter;
use Sef\WpEntities\Interfaces\PropertyConverterInterface;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Sef\WpEntities\Components\Converter\Converter;
use Sef\WpEntities\Exceptions\PropertyNotSetException;

use Doctrine\Common\Collections\ArrayCollection;

abstract class PropertyConverter extends Converter implements PropertyConverterInterface {

  protected $property;

  public function setProperty( PropertyInterface $property )
  {
    $this->property = $property;

    return $this;
  }

  public function convert()
  {
    if ( null === $this->property )
    {
      throw new PropertyNotSetException('no Property set');
    }

    return parent::convert();
  }
}
