<?php
namespace Sef\WpEntities\Components\Converter\PropertyConverter;
use Sef\WpEntities\Base\EntityBag;
use Doctrine\Common\Collections\ArrayCollection;
use Sef\WpEntities\Exceptions\MissingOptionException;
use Sef\WpEntities\Exceptions\PropertyNotSetException;
use Sef\WpEntities\Exceptions\MissingImplementationException;
use Sef\WpEntities\Interfaces\ImporterInterface;
use Sef\WpEntities\Interfaces\ImporterStrategyInterface;

class Importdata2EntityPropertyConverter extends PropertyConverter {

   protected $flags = self::NO_FLAGS;

   protected $targetEntityClass;

   protected $importerStrategy;

  /**
   * converting function.
   *
   * @access protected
   * @param mixed $data
   * @return WP_User
   */
  protected function converting($data)
  {
    $entityClass = $this->targetEntityClass;
    $entity = $entityClass::make();
    $importerStrategy = $this->importerStrategy;
    $entity->import($data, $importerStrategy );
    return $entity;
  }


  protected function setup()
  {

    if ( null === $this->property )
    {
      throw new PropertyNotSetException('no Property set');
    }

    $targetEntityClass = $this->property->getOptions()->get('entity');
    if ( ! $targetEntityClass )
    {
      throw new MissingOptionException('No entity');
    }
    // make an empty object
    $entity = $targetEntityClass::make();

    $importerStrategyClass = null;
    $importerStrategyClassFromProperty = $this->property->getOptions()->get('importerStrategy');
    if ( $importerStrategyClassFromProperty)
    {
      $importerStrategyClass = $importerStrategyClassFromProperty;
    } else {
      $importerStrategyClass  =  $entity->config->options->get('classOptions')->get('importerStrategy');
    }

    if ( ! $importerStrategyClass )
    {
      throw new MissingOptionException('No ImporterInterface');
    }
    $importerStrategyClassReflector = new \ReflectionClass($importerStrategyClass);

    if( ! $importerStrategyClassReflector->implementsInterface('Sef\\WpEntities\\Interfaces\\ImporterStrategyInterface')  )
    {
      throw new MissingImplementationException('No ImporterInterface');
    }
    $this->targetEntityClass = $targetEntityClass;
    $this->importerStrategy = new $importerStrategyClass;
  }

  /**
   * isConverted function.
   *
   * @access protected
   * @param mixed $data
   * @return bool
   */
  protected function isConverted( $data )
  {
    // @TODO also check for SimpleEntity

    return ($data instanceof ArrayCollection || ($data instanceof EntityBag ));
  }

  protected function isCollection( $data )
  {
    if( ! is_array($data))
      return false;
	
	// handle empty arrays as collection
	if( empty($data)) 
	{
		return true;
	}	

    return ( ! $this->isAssocArray($data));
  }

  protected function isAssocArray($arr)
  {
      return array_keys($arr) !== range(0, count($arr) - 1);
  }
}
