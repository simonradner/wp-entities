<?php
namespace Sef\WpEntities\Components\Converter\PropertyConverter;
use Sef\WpEntities\Base\EntityBag;
use Doctrine\Common\Collections\ArrayCollection;

class Id2EntityPropertyConverter extends PropertyConverter {

   protected $flags = self::ARRAY_AS_COLLECTION;

   protected $repository;

  /**
   * converting function.
   *
   * @access protected
   * @param mixed $data
   * @return WP_User
   */
  protected function converting($data)
  {
    $entity = $this->repository->findById($data);
    return ($entity) ? $entity : null;
  }

  protected function convertingCollection($data)
  {
    $collection = $this->repository->findByIds($data);
    return $collection;
  }

  protected function setup()
  {
    if (null === $this->property)
      return;

    $targetEntityClass = $this->property->getOptions()->get('entity');
    $repositoryClass = $this->property->getOptions()->get('repository');
    $this->repository = $repositoryClass::make();
  }

  /**
   * isConverted function.
   *
   * @access protected
   * @param mixed $data
   * @return bool
   */
  protected function isConverted( $data )
  {
    return ($data instanceof ArrayCollection || $data instanceof EntityBag);
  }
}
