<?php
namespace Sef\WpEntities\Components\Converter\PropertyConverter;
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Base\SimpleEntity;
use Doctrine\Common\Collections\ArrayCollection;

class Data2SimpleEntityPropertyConverter extends PropertyConverter {

   protected $flags = self::NO_FLAGS;

   protected $targetEntityClass;

   protected $importerStrategy;

  /**
   * converting function.
   *
   * @access protected
   * @param mixed $data
   * @return WP_User
   */
  protected function converting($data)
  {
    $simpleEntityClass = $this->targetEntityClass;
    $simpleEntity = $simpleEntityClass::make();
    $importerStrategy = $this->importerStrategy;
    $simpleEntity->import($data, $importerStrategy );
    return $simpleEntity;
  }


  protected function setup()
  {

    if (null === $this->property)
      return null;

    $targetEntityClass = $this->property->getOptions()->get('entity');
    // make an empty object
    $simpleEntity = $targetEntityClass::make();

    // @TODO throw exception if no simpleEntity


    $importerStrategyClass = null;
    $importerStrategyClassFromProperty = $this->property->getOptions()->get('importerStrategy');
    if ( $importerStrategyClassFromProperty)
    {
      $importerStrategyClass = $importerStrategyClassFromProperty;
    } else {
      $importerStrategyClass  =  $simpleEntity->config->options->get('classOptions')->get('importerStrategy');
    }

    //wp_die($importerStrategyClass);
    // @TODO check importerinterface and throw exception
    $importerStrategyClassReflector = new \ReflectionClass($importerStrategyClass);

    $this->targetEntityClass = $targetEntityClass;
    $this->importerStrategy = new $importerStrategyClass;
  }

  /**
   * isConverted function.
   *
   * @access protected
   * @param mixed $data
   * @return bool
   */
  protected function isConverted( $data )
  {
    // @TODO also check for SimpleEntity

    return ($data instanceof ArrayCollection || ($data instanceof EntityBag && $data->getInstance() instanceof SimpleEntity ) );
  }

  protected function isCollection( $data )
  {
    if( ! is_array($data))
      return false;

    return ( ! $this->isAssocArray($data));
  }

  protected function isAssocArray($arr)
  {
      return array_keys($arr) !== range(0, count($arr) - 1);
  }
}
