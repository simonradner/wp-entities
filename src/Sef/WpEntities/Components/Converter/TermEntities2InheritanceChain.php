<?php
namespace Sef\WpEntities\Components\Converter;  
use Sef\WpEntities\Interfaces\TermEntityInterface;       
use Sef\WpEntities\Base\EntityBag;       


     
use Doctrine\Common\Collections\ArrayCollection;

class TermEntities2InheritanceChain extends Converter {
   
  /**
   * converting function.
   * 
   * @access protected
   * @param mixed $data
   * @return DateTime
   */
  protected function converting($data)
  {
    if( ! $data instanceof ArrayCollection )
      return new ArrayCollection;
      
    $roots = $data->filter(function( $entity ){
      if( ! $entity instanceof EntityBag || ! $entity->getInstance() instanceof TermEntityInterface )
      {
        return false;
      }
      return ( ! $entity->getParent());
    });
    if( ! $roots->isEmpty())
    {
      $root = $roots->first();
      $chain = $this->recursiveChainbuilder( $root, $data, new ArrayCollection );
      return $chain;
    }   
    return new ArrayCollection;
  }

  /**
   * isConverted function.
   * 
   * @access protected
   * @param mixed $data
   * @return bool
   */
  protected function isConverted( $data )
  {
    return false;
  }


  private function recursiveChainbuilder( EntityBag $parent, ArrayCollection $entities, ArrayCollection $chain )
  {
    if( $chain->isEmpty())
    {
      $chain->add( $parent );
    }
    $children = $entities->filter(function( $entity ) use ( $parent ){
      if( ! $entity->getParent())
      {
        return false;          
      }
      return ( $entity->getParent() === $parent->getId());
    });

    if( ! $children->isEmpty())
    {
      $child = $children->first();
      $chain->add( $child );
      return $this->recursiveChainbuilder($child, $entities, $chain);
    }
    return $chain;    
  }
}