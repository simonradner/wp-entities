<?php
namespace Sef\WpEntities\Components\State;       
use Sef\WpEntities\Interfaces\EntityStateInterface;       
use Sef\WpEntities\Components\Config\EntityConfig;
use Sef\WpEntities\Components\EntityProperty;     
use Doctrine\Common\Collections\ArrayCollection;
  

class EntityState implements EntityStateInterface {
  
  protected $entityConfig;
  
  protected $propertyStates;
  
  public function __construct(  EntityConfig $entityConfig )
  {
    $this->entityConfig = $entityConfig;
    
    $propertyStates = [];
    foreach($entityConfig->getProperties() as $property)
    {
      $propertyStates[$property->name] = new PropertyState($property);
    };  
    $this->propertyStates = new ArrayCollection($propertyStates);  
  }
  
  
  public function fetched( $property = null )
  {
    $this->setOneOrAll('fetched', $property);
  } 

  public function wasFetched($property = null)
  {
    return $this->returnOneOrAll('wasFetched', $property);
  } 

  public function set( $property = null )
  {
    $this->setOneOrAll('set', $property);
  } 

  public function wasSet($property = null)
  {
    return $this->returnOneOrAll('wasSet', $property);
  } 

  public function saved( $property = null )
  {
    $this->setOneOrAll('saved', $property);
  } 

  public function wasSaved($property = null)
  {
    return $this->returnOneOrAll('wasSaved', $property);
  } 
    
  // internal

  protected function findPropertyState(EntityProperty $property)
  {
    return $this->propertyStates->get($property->name);
  }

  protected function setOneOrAll($methodName = 'fetched', $property = null)
  {
    if( $property instanceof EntityProperty ) 
    {
      $propertyState = $this->findPropertyState($property);
      $propertyState->$methodName();
    } else {
      $this->propertyStates->map(function($propertyState) use($methodName){
        $propertyState->$methodName();
      });      
    }    
  }

  protected function returnOneOrAll($methodName = 'wasFetched', $property = null)
  {
    if( $property instanceof EntityProperty ) 
    {
      $propertyState = $this->findPropertyState($property);
      return $propertyState->$methodName();      
    }
    
    // when one was set we re fine
    $trueProperties =  $this->propertyStates->filter(function( $propertyState) use($methodName) {
      return $propertyState->$methodName();
    });
    
    return ($trueProperties->isEmpty()) ? false : true;
  }
}