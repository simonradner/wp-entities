<?php
namespace Sef\WpEntities\Components\State;       
use Sef\WpEntities\Components\EntityProperty;       

class PropertyState  {
  
  protected $fetched = false;
  
  protected $set = false;
  
  protected $saved = false; 
  
  protected $property;
  
  public function __construct(  EntityProperty $property )
  {
    $this->property = $property;
  }
  
  public function set()
  {
    $this->set = true;
  }

  public function wasSet()
  {
    return $this->set;
  }

  public function fetched()
  {
    $this->fetched = true;
  }

  public function wasFetched()
  {
    return $this->fetched;
  }

  public function saved()
  {
    $this->saved = true;
  }

  public function wasSaved()
  {
    return $this->saved;
  }
}