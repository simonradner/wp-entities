<?php
namespace Sef\WpEntities\Components\Config;       
use Sef\WpEntities\Annotation\Options;


class RepositoryConfig extends Config {
  
  public function __construct( $configuredClass = '', Options $configOptions ) 
  {
    parent::__construct($configuredClass,  $configOptions );
    $this->parseSubOptions('annotationEntityClass', 'entities' );
  }
}