<?php
namespace Sef\WpEntities\Components\Config;       
use Sef\WpEntities\Annotation\Options;

abstract class Config {
  
  public $options = [];
  
  public $entityReflectionClass;
    
  public function __construct( $configuredClass = '', Options $configOptions ) 
  {
    $this->entityReflectionClass = new \ReflectionClass($configuredClass);      
    $this->options = $configOptions::make($this->getSubclassesMergedConfigOptions());
    $this->parseSubOptions('optionClass', 'classOptions' );
  }
 
  /**
   * getSubclassesMergedConfigOptions function.
   * 
   * calls all ::configOptions form all subclasses and merges the options recursively
   *
   * @access protected
   * @return void
   */
  protected  function getSubclassesMergedConfigOptions()
  {        
    $tree = [];
    $class = $this->entityReflectionClass;
    $tree[] = $class->getName();

    while ($class = $class->getParentClass()) 
    {
      $tree[] = $class->getName();
    }
    $tree = array_reverse($tree);
    $options = [];
    foreach ($tree as $subclass) 
    {
      $options[] = $subclass::configOptions();
    }
    $options = call_user_func_array('array_replace_recursive', $options );

    return $options;    

  } 
  
  /**
   * parseSubOptions function.
   * 
   * single configoptions values may be arrays, 
   * here those arrays get parsed into an option Object itself
   * php config and annotation values are merged
   *
   * @access protected
   * @param string $optionsClassnameKey (default: 'optionClass')
   * @param string $optionsKey (default: 'classOptions')
   * @return void
   */
  protected function parseSubOptions($optionsClassnameKey = 'optionClass', $optionsKey = 'classOptions')
  {
    // parse Class Options
    $optionClass = $this->options->get($optionsClassnameKey);
    $subOptionsRaw = $this->options->get($optionsKey);
    $subOptions = new $optionClass($subOptionsRaw);
    
    // check for annotations
    $annotatedClassOptions = $this->options->get('annotationReader')->getClassAnnotation($this->entityReflectionClass, $this->options->get($optionsClassnameKey));
    if($annotatedClassOptions) {
      // override php values values with annotated option 
      $subOptions->mergeRecusrsive([$annotatedClassOptions->get()]);
    } 
    
    // replace the array with an Option Object
    $this->options->set($optionsKey, $subOptions );
  }
}