<?php
namespace Sef\WpEntities\Components\Config;
use Sef\WpEntities\Annotation\Options;
use Sef\WpEntities\Components\EntityPropertyFactory;

class EntityConfig extends Config {

  // not implemented
  const TYPE_POST = 0b0001; //1

  // not implemented
  const TYPE_TERM = 0b0010; //2

  // not implemented
  const TYPE_USER = 0b0100; //4

  // not implemented
  const TYPE_SIMPLE = 0b1000; //8

  // not implemented
  const TYPE_COMMENT = 0b10000; //16

  protected $properties = null;

  public function __construct( $configuredClass = '', Options $configOptions )
  {
    parent::__construct($configuredClass,  $configOptions );

    // prepare all
    $this->parsePropertyOptions();
    $this->properties = EntityPropertyFactory::get($this);
  }

  /**
   * getPropertiesByType function.
   *
   * @access public
   * @param mixed $type
   * @return ArrayCollection
   */
  public function getPropertiesByType($type)
  {
    return $this->properties->filter(function($property) use ($type) {
      return ($property->type === $type);
    });
  }

  /**
   * getProperties function.
   *
   * @access public
   * @return ArrayCollection
   */
  public function getProperties()
  {
    return $this->properties;
  }

  /**
   * getProperties function.
   *
   * @access public
   * @return Sef\WpEntities\Components\EntityProperty
   */
  public function getProperty($name)
  {
    return $this->properties->get($name);
  }

  public function getPropertyOptions($methodOrProperty)
  {
    if(substr($methodOrProperty, 0,3) == 'get') {
      $propertyName = lcfirst(substr($methodOrProperty, 3));
    } else {
      $propertyName = $methodOrProperty;
    }

    $property = $this->getProperty($propertyName);
    if($property)
      return $property->getOptions();
  }

  public function getReflectionProperties()
  {
    return $this->entityReflectionClass->getProperties( \ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED | \ReflectionProperty::IS_PRIVATE);

//     ~\ReflectionProperty::IS_STATIC
  }

  private function parsePropertyOptions()
  {

    // parse Property Options

    // find all non-static properties

    $propertyOptionsArray = [];
    $rawPropertyOptionsArray = $this->options->get('propertyOptions');

    foreach($this->getReflectionProperties() as $property ) {
      $rawPropertyOptions = (isset($rawPropertyOptionsArray[$property->getName()])) ? $rawPropertyOptionsArray[$property->getName()] : [];
      $propertyOptionClass = $this->options->get('optionClass');
      $propertyOptions = new $propertyOptionClass($rawPropertyOptions);

      // check for annotations
      $annotatedPropertyOptions = $this->options->get('annotationReader')->getPropertyAnnotation($property, $this->options->get('optionClass'));
      if($annotatedPropertyOptions) {

        // override php values values with annotated option
        $propertyOptions->mergeRecusrsive([$annotatedPropertyOptions->get()]);
      }
      // replace the array with an Option Object
      $propertyOptionsArray[$property->getName()] = $propertyOptions;
    }
    $this->options->set('propertyOptions', $propertyOptionsArray);
  }
}
