<?php
namespace Sef\WpEntities;       
use Doctrine\Common\Collections\ArrayCollection;

final class EntityManager {

  protected $taxonomies = null;

  public function __construct( ) {
    $this->taxonomies = new ArrayCollection();
  }
  
  public function addTaxonomy( $taxonomy ) {
    $this->taxonomies->add( $taxonomy);
  }

  public function getTaxonomy( $taxonomy ) {
    return $this->taxonomies->get($taxonomy);
  }
}
