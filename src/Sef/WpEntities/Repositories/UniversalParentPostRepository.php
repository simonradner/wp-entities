<?php
namespace Sef\WpEntities\Repositories;
use Sef\WpEntities\Base\PostRepository as PostRepositoryBase;
use Sef\WpEntities\Annotation as Anote;

 /**
   * @Anote\Entities( 
   *   any = "Sef\WpEntities\Entities\ParentPost"
   * )
   */
class UniversalParentPostRepository extends PostRepositoryBase {


  public static function configOptions()
  { 
    return [
      'queryDefaults'       => ['post_type' => 'any', 'post_status' => 'publish'],
    ];
  }



}

