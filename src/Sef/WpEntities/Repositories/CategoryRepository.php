<?php
namespace Sef\WpEntities\Repositories;               
use Sef\WpEntities\Base\TermRepository;
use Sef\WpEntities\Annotation as Anote;

 /**
   * @Anote\Entities( 
   *    category = "\Sef\WpEntities\Entities\Category"
   * )
   */
class CategoryRepository extends TermRepository {
   
}