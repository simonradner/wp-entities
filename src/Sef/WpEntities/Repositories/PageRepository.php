<?php
namespace Sef\WpEntities\Repositories;
use Sef\WpEntities\Base\PostRepository as PostRepositoryBase;
use Sef\WpEntities\Annotation as Anote;

 /**
   * @Anote\Entities( 
   *   page = "Sef\WpEntities\Entities\Page"
   * )
   */
class PageRepository extends PostRepositoryBase {

  public static function configOptions()
  { 
    return [
      'queryDefaults'       => ['post_type' => 'page', 'post_status' => 'publish'],
    ];
  }
}