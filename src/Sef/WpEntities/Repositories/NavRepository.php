<?php
namespace Sef\WpEntities\Repositories;

use Sef\WpEntities\Base\Repository;
use Sef\WpEntities\Annotation as Anote;
use Doctrine\Common\Collections\ArrayCollection;


 /**
   * @Anote\Entities(
   *   nav_menu_item = "Sef\WpEntities\Entities\NavItem"
   * )
   */
class NavRepository extends Repository {

  protected $wpMenuObjects = [];

	protected function __construct()
	{
    parent::__construct();
  }

  public function find( $args = [] )
  {
    $args['echo'] = false;

    $this->entityCollection->clear();
    $this->wpMenuObjects = [];

    $args = wp_parse_args( $args, $this->getConfig()->options->get('queryDefaults'));

    // extract and set this->$menuObjects
    add_filter( 'wp_nav_menu_objects', [ $this, 'extractWpMenuObjectsFilter' ], 10, 2 );
    wp_nav_menu($args);
    remove_filter( 'wp_nav_menu_objects', [ $this, 'extractWpMenuObjectsFilter' ], 10 );

    if( $this->wpMenuObjects ) {
      $this->entityCollection = $this->wpObjectsOrIds2EntityCollection( $this->wpMenuObjects, 'post_type');
      return $this->entityCollection;
    }
    return new ArrayCollection;
  }

  public function extractWpMenuObjectsFilter( $sorted_menu_items, $args )
  {
    $this->wpMenuObjects = array_values($sorted_menu_items); // re assign the keys, starting with 0
    return $this->wpMenuObjects;
  }

  public static function configOptions()
  {
    return [];
  }
}
