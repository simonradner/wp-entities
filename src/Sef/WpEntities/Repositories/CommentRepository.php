<?php
namespace Sef\WpEntities\Repositories;
use Sef\WpEntities\Base\CommentRepository as BaseCommentRepository;
use Sef\WpEntities\Annotation as Anote;

 /**
   * @Anote\Entities(
   *   any = "Sef\WpEntities\Entities\Comment"
   * )
   */
class CommentRepository extends BaseCommentRepository {

  public static function configOptions()
  {
    return [
      'queryDefaults'       => ['status' => 'approve'],
    ];
  }
}
