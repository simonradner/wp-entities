<?php
namespace Sef\WpEntities\Repositories;               
use Sef\WpEntities\Base\TermRepository;
use Sef\WpEntities\Annotation as Anote;

 /**
   * @Anote\Entities( 
   *   post_tag = "Sef\WpEntities\Entities\Tag"
   * )
   */
class TagRepository extends TermRepository {

}
