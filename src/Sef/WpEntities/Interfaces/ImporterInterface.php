<?php
namespace Sef\WpEntities\Interfaces;
use Sef\WpEntities\Base\EntityBag;

interface ImporterInterface {
  function __construct( $data, EntityBag $entityBag );
  function setStrategy( ImporterStrategyInterface $strategy );
  function import();
}
