<?php
namespace Sef\WpEntities\Interfaces;       

interface PropertyConverterInterface extends ConverterInterface {
  function setProperty( PropertyInterface $property );
}

