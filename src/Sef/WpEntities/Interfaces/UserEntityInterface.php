<?php
namespace Sef\WpEntities\Interfaces;
use Sef\WpEntities\Base\EntityBag;

interface UserEntityInterface {
  function getId( EntityBag $entityBag, $id );
  function getLogin( EntityBag $entityBag, $login );
  function getPass( EntityBag $entityBag, $pass );
  function getEmail( EntityBag $entityBag, $email );
}
