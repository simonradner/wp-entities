<?php
namespace Sef\WpEntities\Interfaces;       

interface WpEntityConstructorInterface { 
  function getConstructorData(); 
  function get(); 
  function load(); 
  function setData($data); 
}  
