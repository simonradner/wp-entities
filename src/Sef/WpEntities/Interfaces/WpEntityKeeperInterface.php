<?php
namespace Sef\WpEntities\Interfaces;
use Sef\WpEntities\Components\WpEntity\WpEntity;

interface WpEntityKeeperInterface {
  function setWpEntity( WpEntity $wpEntity );
  function getWpEntity();
}
