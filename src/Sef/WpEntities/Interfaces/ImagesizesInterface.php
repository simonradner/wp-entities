<?php
namespace Sef\WpEntities\Interfaces;       


interface ImagesizesInterface {
  function setUpImagesize( ImagesizeInterface $imagesize );
}
