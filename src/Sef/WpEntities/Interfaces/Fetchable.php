<?php
namespace Sef\WpEntities\Interfaces;
use Sef\WpEntities\Base\EntityBag;


interface Fetchable {
  function getId( EntityBag $entityBag, $id );
}
