<?php
namespace Sef\WpEntities\Interfaces;
use Sef\WpEntities\Base\EntityBag;

interface ImporterStrategyInterface {
  function __construct( array $args = []);
  function import( $data, EntityBag $to );
}
