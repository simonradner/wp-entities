<?php
namespace Sef\WpEntities\Interfaces;       

interface OverloadAccessorable {  
  function setOverloadAccessor( OverloadAccessorInterface $overloadAccessor );
}
