<?php
namespace Sef\WpEntities\Interfaces;       

interface Deleteable {  
  function delete();
}
