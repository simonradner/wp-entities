<?php
namespace Sef\WpEntities\Interfaces;       

interface OverloadAccessorInterface {  
  function overloadAccessors( OverloadAccessorable $instance, $method, $args );
}
