<?php
namespace Sef\WpEntities\Interfaces;       
use Sef\WpEntities\Base\EntityBag;

interface TermEntityInterface {
  function getId( EntityBag $entityBag, $id );
  function getName( EntityBag $entityBag, $name );
  function getSlug( EntityBag $entityBag, $slug );
}
