<?php
namespace Sef\WpEntities\Interfaces;
use Sef\WpEntities\Base\EntityBag;

interface CommentEntityInterface {
  function getId( EntityBag $entityBag, $id );
}
