<?php
namespace Sef\WpEntities\Interfaces;       
use Symfony\Component\OptionsResolver\OptionsResolver;

interface AnnotationOptionInterface 
{
  function configureOptions(OptionsResolver $resolver);
  function set($key, $value );
  function get($key = null);
  function merge(array $arrayOfOptions = []);
  function mergeRecusrsive(array $arrayOfOptions = []);
  static function make( $options = [] );
}