<?php
namespace Sef\WpEntities\Interfaces;       


interface FetcherInterface {
  function fetch(); 
  function fetchAsArrayCollection(); 
}
