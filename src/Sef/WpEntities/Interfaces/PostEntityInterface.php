<?php
namespace Sef\WpEntities\Interfaces;       
use Sef\WpEntities\Base\EntityBag;

interface PostEntityInterface {
  function getId( EntityBag $entityBag, $id );
  function getTitle( EntityBag $entityBag, $title );
  function getContent( EntityBag $entityBag, $content );
  function getName( EntityBag $entityBag, $name );
}

