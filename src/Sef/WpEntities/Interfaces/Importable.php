<?php
namespace Sef\WpEntities\Interfaces;


interface Importable {
  function import($data, $strategy = null);
}
