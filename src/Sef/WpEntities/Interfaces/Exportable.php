<?php
namespace Sef\WpEntities\Interfaces;       


interface Exportable {  
  function export($include=[]);
}
