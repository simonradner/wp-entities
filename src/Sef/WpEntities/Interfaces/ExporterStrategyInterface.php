<?php
namespace Sef\WpEntities\Interfaces;       
use Sef\WpEntities\Base\EntityBag;
use Sef\WpEntities\Base\Entity;

interface ExporterStrategyInterface {
  function __construct( array $args = []);
  function export( EntityBag $from );
  function setArgs( array $args = []);
}

