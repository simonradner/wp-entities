<?php
namespace Sef\WpEntities\Interfaces;


interface NamingStrategyInterface {
  function setName( $name );
  function translateName();
  function canTranslate();
}
