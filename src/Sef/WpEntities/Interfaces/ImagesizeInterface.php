<?php
namespace Sef\WpEntities\Interfaces;       


interface ImagesizeInterface {
  function setAttachmentId( $attachmentID );
  function setSize( $size );
  function setUp();
}
