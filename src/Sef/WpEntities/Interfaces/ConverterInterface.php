<?php
namespace Sef\WpEntities\Interfaces;       

interface ConverterInterface {
  function convert();
  function setFlags( $flags );
  function setData( $data );

}

