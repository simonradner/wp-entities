<?php
namespace Sef\WpEntities\Interfaces;       

interface EntityStateInterface {
  function fetched( $property = null );
  function wasFetched($property = null);
  function set( $property = null );
  function wasSet($property = null);
  function saved( $property = null );
  function wasSaved($property = null);
}

