<?php
namespace Sef\WpEntities\Interfaces;       

interface WpEntityTermConstructorInterface extends WpEntityConstructorInterface { 
  function setTaxonomy( $name ); 
  function getTaxonomy(); 
}  
