<?php
namespace Sef\WpEntities\Interfaces;

interface WpEntityInterface {
  function getId();
  function getProperty($property);
  function getRawObject();
}
