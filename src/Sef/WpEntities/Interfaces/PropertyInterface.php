<?php
namespace Sef\WpEntities\Interfaces;       

interface PropertyInterface { 
  function getOptions(); 
  function getOption($option); 
  function getName(); 
  function getGroups(); 
  function getType(); 
  function getEntityClass(); 
  function getRepositoryClass();
}  