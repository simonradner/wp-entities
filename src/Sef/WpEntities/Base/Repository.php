<?php
namespace Sef\WpEntities\Base;

use Sef\WpEntities\Components\Config\RepositoryConfig;
use Sef\WpEntities\Annotation\RepositoryConfigOptions;
use Sef\WpEntities\Components\Converter\WpObject2EntityConverter;
use Sef\WpEntities\Interfaces\Exportable;
use Sef\WpEntities\Interfaces\Fetchable;

use Doctrine\Common\Collections\ArrayCollection;

abstract class Repository implements Exportable {

  public $entityCollection = null;

  protected $returnOnlyIds = false;

  protected $fetchOnlyIds = false;

  protected static $configs = [];

  public static function make()
  {
    return new static();
  }

	protected function __construct()
	{
    $this->entityCollection = new ArrayCollection();
  }

  /**
   * posts2EntityCollection function.
   *
   * convert an array of WP_Posts Or Ids into an ArrayCollection of Entities
   *
   * @access protected
   * @param array $wpObjectsOrIds
   * @return ArrayCollection [Entity]
   */
  protected function wpObjectsOrIds2EntityCollection( array $wpObjectsOrIds, $wpObjectProperty = 'post_type' )
  {
      if( $this->fetchOnlyIds )
      {
        return $this->ids2EntityCollection($wpObjectsOrIds );
      } else
      {
        return $this->wpObjects2EntityCollection($wpObjectsOrIds, $wpObjectProperty );
      }
  }

  /**
   * posts2EntityCollection function.
   *
   * convert an array of WP_Posts into an ArrayCollection of Entities
   *
   * @access protected
   * @param array $wpObjects
   * @return ArrayCollection [Entity]
   */
  protected function wpObjects2EntityCollection( array $wpObjects, $wpObjectProperty = 'post_type' )
  {
    if($wpObjects) {
      $entities = $this->getConfig()->options->get( 'entities' );

      $wpObjects = new ArrayCollection( $wpObjects );
      $validTypes = $entities->getTypes();
      $filteredWpObjects = $wpObjects->filter( function( $wpObject ) use ( $validTypes, $wpObjectProperty ) {
        if(in_array( 'any', $validTypes )) // when one of the posttypes is 'any', pass all
        {
          return true;
        }
        return in_array( $wpObject->$wpObjectProperty, $validTypes );
      });
      $entities = $filteredWpObjects->map( function( $wpObject ) use ( $entities, $wpObjectProperty ) {
        if( $entities->hasType( $wpObject->$wpObjectProperty ))
        {
          $entityClassname =$entities->get( $wpObject->$wpObjectProperty );
        } else {
          // at this point it is certain that a post type 'any' exist
          $entityClassname =$entities->get( 'any' );
        }

        return $entityClassname::make($wpObject);
      });
      return $entities;
    }
    return new ArrayCollection;
  }

  /**
   * posts2EntityCollection function.
   *
   * convert an array of WP_Posts into an ArrayCollection of Entities
   *
   * @access protected
   * @param array $wpObjects
   * @return ArrayCollection [Entity]
   */
  protected function ids2EntityCollection( array $ids )
  {
    if($ids) {
      $entities = $this->getConfig()->options->get( 'entities' );

      $ids = new ArrayCollection( $ids );

      $entities = $ids->map( function( $id ) use ( $entities ) {
        $entityClassname =$entities->getFirst();
        return $entityClassname::make($id);
      });
      return $entities;
    }
    return new ArrayCollection;
  }

  public static final function getConfig()
  {
    if ( ! isset(static::$configs[get_called_class()])) {
      static::$configs[get_called_class()] = new RepositoryConfig(get_called_class(), new RepositoryConfigOptions());
    }
    return static::$configs[get_called_class()];
  }

  public static function configOptions()
  {
    return [];
  }

  public function fetch()
  {
    if( $this->entityCollection instanceof ArrayCollection )
    {
      foreach($this->entityCollection as $entity )
      {
        $entity->fetch();
      }
    }
    return $this;
  }

  public function export($strategy = null)
  {
    if( $this->entityCollection instanceof ArrayCollection )
    {
      $exportedEntities = $this->entityCollection->map( function( $entity ) use ($strategy) {
         return $entity->export($strategy);
      });
    }
    return $exportedEntities->toArray();
  }

  public function setReturnOnlyIds( $value = true )
  {
    $this->returnOnlyIds = $value;
    return $this;
  }

  public function setFetchOnlyIds( $value = true )
  {
    $this->fetchOnlyIds = $value;
    return $this;
  }

  protected function reIndexArray( array $array )
  {
    return array_values($array);
  }

  protected function reIndexArrayCollection( ArrayCollection $arrayCollection )
  {
    return new ArrayCollection(array_values($array->toArray()));
  }

  public function findNothing( )
  {
    $this->entityCollection->clear();
    return new ArrayCollection;
  }
}
