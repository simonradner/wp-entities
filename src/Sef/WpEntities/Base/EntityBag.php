<?php
namespace Sef\WpEntities\Base;
use Sef\WpEntities\Interfaces\TermEntityInterface;
use Sef\WpEntities\Interfaces\PostEntityInterface;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Interfaces\FetchableBagAware;
use Sef\WpEntities\Interfaces\FetcherInterface;
use Sef\WpEntities\Interfaces\WpEntityTermConstructorInterface;
use Sef\WpEntities\Interfaces\ConverterInterface;
use Sef\WpEntities\Interfaces\PropertyConverterInterface;
use Sef\WpEntities\Components\Fetcher\Fetcher;
use Sef\WpEntities\Components\Fetcher\Composite\InheritFetcher;
use Sef\WpEntities\Components\Converter\Proxy\PropertyConverter as ProxyPropertyConverter;
use Sef\WpEntities\Components\Exporter\Exporter;
use Sef\WpEntities\Interfaces\ExporterStrategyInterface;
use Sef\WpEntities\Components\Importer\Importer;
use Sef\WpEntities\Interfaces\ImporterStrategyInterface;
use Sef\WpEntities\Components\Saver\Saver;
use Sef\WpEntities\Interfaces\Saveable;
use Sef\WpEntities\Interfaces\Deleteable;
use Sef\WpEntities\Interfaces\Exportable;
use Sef\WpEntities\Interfaces\Importable;
use Sef\WpEntities\Interfaces\WpEntityInterface;
use Sef\WpEntities\Interfaces\WpEntityKeeperInterface;
use Sef\WpEntities\Components\State\EntityState;
use Sef\WpEntities\Components\OverloadAccessors\GetOverloadAccessor;
use Sef\WpEntities\Components\OverloadAccessors\OverloadFail;
use Sef\WpEntities\Interfaces\OverloadAccessorInterface;
use Sef\WpEntities\Interfaces\OverloadAccessorable;
use Doctrine\Common\Collections\ArrayCollection;
use Sef\WpEntities\Components\Config\EntityConfig;
use Sef\WpEntities\Components\WpEntity\Post;
use Sef\WpEntities\Interfaces\CommentEntityInterface;
use Sef\WpEntities\Interfaces\UserEntityInterface;

/**
 * EntityBag class.
 *
 * a proxy class for an entity
 *
 */
class EntityBag implements Exportable, Importable, OverloadAccessorable {

  protected $entity;

  protected $wpEntity;

  public $config;

  public $state;

  protected $entityConstructor;

  protected $overloadAccessor;

  protected $bagtype = 0;

  private $_temp_post;

  public function __construct( EntityBone $entity = null, $data = null  )
  {
    $this->entity = $entity;

    // class config
    $this->config = $entity::getConfig();

    // state
    $this->state = new EntityState($this->config);

    // terms need the taxonomy set
    if( $entity instanceof TermEntityInterface )
    {
      $this->bagtype = EntityConfig::TYPE_TERM;
      $entity->_setProperty($this->config->getProperty('taxonomy'), $this->getConfigOptions('classOptions')->get('taxonomy'));
    }

    if( $entity instanceof PostEntityInterface )
    {
      $this->bagtype = EntityConfig::TYPE_POST;
    }

    if( $entity instanceof CommentEntityInterface )
    {
      $this->bagtype = EntityConfig::TYPE_COMMENT;
    }

    if( $entity instanceof UserEntityInterface )
    {
      $this->bagtype = EntityConfig::TYPE_USER;
    }

    if( $entity instanceof SimpleEntity )
    {
      $this->bagtype = EntityConfig::TYPE_SIMPLE;
    }

    if( $entity instanceof PostEntityInterface && 'any' !== $this->getConfigOptions('classOptions')->get('post_type'))
    {
      $entity->_setProperty($this->config->getProperty('type'), $this->getConfigOptions('classOptions')->get('post_type'));
    }

    $this->initialize($data);


    if($this->isNew() && $entity instanceof PostEntityInterface )
    {
      // make the current user the author
      $authorProperty = $this->config->getProperty('author');
      $author = $this->entity->_getProperty($authorProperty);
      if( ! $author && is_user_logged_in()) {
        $author = get_current_user_id();
        $this->entity->_setProperty($authorProperty, $author);
      }
    }

    $overloadAccessor = new GetOverloadAccessor;
    $this->setOverloadAccessor($overloadAccessor);

/*
    $converter = new \Sef\WpEntities\Components\Converter\Id2WpUserDisplayNameConverter();
    $proxy = new \Sef\WpEntities\Components\Converter\Proxy\PropertyConverter($this->entity, $this->wpEntity, $this->config->getProperty('author'), $converter );
 echo '<pre>'; print_r($converter->setData('now')->convert()); echo '</pre>';
echo '<pre>'; print_r($proxy->setFlags(\Sef\WpEntities\Components\Converter\Converter::AS_COLLECTION)->convert()); echo '</pre>';
echo '<pre>'; print_r($this->getContent()); echo '</pre>';
*/


  }

  public function export( $strategy = null )
  {
    $exporter = new Exporter($this);
    if( $strategy instanceof ExporterStrategyInterface )
    {
      $exporter->setStrategy( $strategy );
    }
    return $exporter->export();
  }

  public function import( $data, $strategy = null )
  {
    $importer = new Importer( $data, $this );
    if( $strategy instanceof ImporterStrategyInterface )
    {
      $importer->setStrategy( $strategy );
    }
    $importer->import();
  }

  public function getInstance()
  {
    return $this->entity;
  }

  public function __get( $name )
  {
    // returns OverloadResult
    $result = $this->overloadAccessor->overloadAccessors( $this, $name, [] );
    if( $result->success )
    {
      return $result->value;
    }

    //default
    return $this->entity->$name;
  }

  public function __isset( $name )
  {
    // returns OverloadResult
    $result = $this->overloadAccessor->overloadAccessors( $this, $name, [] );
    if( $result->success  && $result->value !== null )
    {
      return true;
    }
    //default
    return isset($this->entity->$name);
  }

  public function __set( $name, $value )
  {
//     return call_user_func_array([$this, 'set' . ucfirst($name)], [$value]);
    if($property = $this->config->getProperty($name))
    {
//       echo '<pre>'; print_r($property); echo '</pre>';
      $this->entity->$name = $value;
    }
  }

	public function __call( $method, $args = [] )
	{
    if(substr($method, 0,3) == 'get')
    {

      $maybePropertyName = lcfirst(substr($method, 3));
      if( $property = $this->config->getProperty($maybePropertyName) )
      {

        // check for a value in the object's property
        $rawValue = $this->entity->_getProperty($property);

        // fetch not yet fetched properties automatically (lazy load)
        if( ( ! $this->state->wasFetched($property) && ! $this->state->wasSet($property) ) && $this->wpEntity instanceof WpEntityInterface)
        {
          // echo "fetching " . $property->getName() . "\n";
          $fetcher = Fetcher::factory($this, $this->wpEntity, $property );

          if($fetcher)
          {
            $this->fetchAndSet($fetcher);
          }
        }
        $passedValueAsArgument = $this->entity->_getProperty($property);

        // check for a converter
        $getterConverter = null;
        if( $converterClass = $property->getOption('getterConverter')  )
        {
          $getterConverter = new $converterClass();
          if( $getterConverter instanceof PropertyConverterInterface )
          {
            $getterConverter->setProperty($property);
          } else {
            // @TODO adjust API of the proxy converterClass
            // no constructor!!
            $getterConverter = new ProxyPropertyConverter($this->entity, $property, new $converterClass());
          }
          $rawValue = $this->entity->_getProperty($property);
          $passedValueAsArgument = $getterConverter->setData($rawValue)->convert();
        }
        // first arg is allways the entity bag for all sorts of Getter
        array_unshift($args, $this, $passedValueAsArgument);

        return call_user_func_array([$this->entity, $method], $args);
      } else { // when no property
        array_unshift($args, $this);
        return call_user_func_array([$this->entity, $method], $args);
      }
    }

    if(substr($method, 0,3) == 'set' )
    {
      $maybePropertyName = lcfirst(substr($method, 3));
      if( isset($args[0]) && $this->config->getProperty( $maybePropertyName ))
      {
        $property = $this->config->getProperty($maybePropertyName);
        $rawValue =  $args[0];


        // check for a converter
        $setterConverter = null;
        if( $converterClass = $property->getOption('setterConverter'))
        {

          // @TODO review Converter system
          // normal converter
          // property converter
          // proxy converter
          // the entity in the proxy ???


          $setterConverter = new $converterClass();
          $value = $setterConverter->setData($rawValue)->setProperty($property)->convert();
          $args[0] = $value;
        }
        array_push($args, $this);
        $result = call_user_func_array([$this->entity, $method], $args);
        $this->state->set($property);
        return $result;
      } else {
        array_push($args, $this);
        return call_user_func_array([$this->entity, $method], $args);
      }
    }

    // ALL NOT GETTER
//     return call_user_func_array([$this->entity, $method], $args);
  }

  public function __toString()
  {
    // make this configureable throug options

    if( $this->entity instanceof TermEntityInterface )
    {
      return $this->getName();
    }

    if( $this->entity instanceof PostEntityInterface )
    {
      return $this->getTitle();
    }
    return '';
  }

  public function save()
  {
    if( ! $this->entity instanceof Saveable )
    {
      // exception
      //
      return $this;
    }

    $saver = Saver::factory($this->entity, $this->state, $this->getId() );
    if($saver)
    {
      $id = $saver->save();

      if( $id instanceof \WP_Error )
      {
        return $id;
      }

      if($this->isNew() && is_int($id))
      {
        $this->initialize($id);
      }
    }
    return $this;
  }


  public function fetch()
  {

    // DEPRICATED
    //
    // fetching is now handeled auto

    if( ! $this->entity instanceof Fetchable )
    {
      // exception
      //
      return $this;
    }

    $fetcher = Fetcher::factory($this, $this->wpEntity );
    if($fetcher)
    {
      $this->fetchAndSet($fetcher);
    }
    return $this;
  }


  public function isNew()
  {
    return (null === $this->wpEntity);
  }

  public function exists()
  {
    if( $this->isNew() )
    {
      return false;
    }
    if( $this->isTypePost())
    {
      // handle cornercase posttype 'any'.
      // those will unlikely be tested for exist

      // fetch the posttype from wp and compare it to the post type set in the entityclass options
      return ($this->wpEntity->getProperty('post_type') === $this->getConfigOptions('classOptions')->get('post_type'));
    }
    if( $this->isTypeTerm())
    {
      // fetch the taxonomy from wp and compare it to the post type set in the entityclass options
      return ($this->wpEntity->getProperty('taxonomy') === $this->getConfigOptions('classOptions')->get('taxonomy'));
    }
    if( $this->isTypeComment())
    {
      return ($this->wpEntity->getRawObject()) ? true: false;
    }
    if( $this->isTypeUser())
    {
      return ($this->wpEntity->getRawObject()) ? true: false;
    }
    if( $this->isTypeSimple())
    {
      // cannot exist
      return false;
    }
  }

  public function isTypePost()
  {
    return ($this->bagtype === EntityConfig::TYPE_POST);
  }

  public function isTypeTerm()
  {
    return ($this->bagtype === EntityConfig::TYPE_TERM);
  }

  public function isTypeSimple()
  {
    return ($this->bagtype === EntityConfig::TYPE_SIMPLE);
  }

  public function isTypeComment()
  {
    return ($this->bagtype === EntityConfig::TYPE_COMMENT);
  }

  public function isTypeUser()
  {
    return ($this->bagtype === EntityConfig::TYPE_USER);
  }

  public function isTypePostOrTerm()
  {
    return ( $this->bagtype === EntityConfig::TYPE_POST | $this->bagtype === EntityConfig::TYPE_TERM );
  }



  public function getConfigOptions( $name = null )
  {
    $options = $this->config->options;
    return $options->get($name);
  }

  public function getEntityConstructor()
  {
    if( $this->entityConstructor)
      return $this->entityConstructor;

    $options = $this->getConfigOptions('classOptions');
    $wpEntityConstructorClass = $options->get('wpEntityConstructor');
    $this->entityConstructor = new $wpEntityConstructorClass();
    return $this->entityConstructor;
  }

  protected function fetchAndSet( FetcherInterface $fetcher )
  {
    $result = $fetcher->fetchAsArrayCollection();
    foreach($result as $fetchresult )
    {
      // set property via the proxy
      $this->entity->_setProperty($fetchresult->property, $fetchresult->value);
      $this->state->fetched($fetchresult->property);
    }
  }

  protected function initialize( $data )
  {

    if ( ! $this->entity instanceof WpEntityKeeperInterface )
      return;

    if( null === $data )
      return;

    if( $data instanceof WpEntityInterface )
    {
      $wpEntity = $data;
    } else
    {
      // construct a Wp Entity
      $wpEntityConstructor = $this->getEntityConstructor();
      $wpEntityConstructor->setData($data);

      // terms need the taxonomy in the constructor
      if( $this->entity instanceof TermEntityInterface && $wpEntityConstructor instanceof WpEntityTermConstructorInterface )
      {
        $wpEntityConstructor->setTaxonomy($this->getConfigOptions('classOptions')->get('taxonomy'));
      }
      $wpEntity = $wpEntityConstructor->get();
    }

    $this->wpEntity = $wpEntity;

    if( $this->wpEntity instanceof WpEntityInterface )
    {
      $this->entity->setWpEntity($this->wpEntity);
      //$inhertitFetcher = new InheritFetcher( $this->entity, $this->wpEntity );
      //$this->fetchAndSet($inhertitFetcher);
    }
  }
  public function setOverloadAccessor( OverloadAccessorInterface $overloadAccessor )
  {
    $this->overloadAccessor = $overloadAccessor;
  }

  public function setGlobalPost()
  {
    if( ! $this->wpEntity instanceof Post )
      return;

    global $post;
    $this->_temp_post = $post;
    $post = $this->wpEntity->getRawObject();
    setup_postdata($post);
  }

  public function resetGlobalPost()
  {
    wp_reset_postdata();
  }

  public function setPreviousGlobalPost()
  {
    if( ! $this->_temp_post )
      retrun;

    global $post;
    $post = $this->_temp_post;
    setup_postdata($post);
  }
}
