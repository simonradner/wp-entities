<?php
namespace Sef\WpEntities\Base;
use Sef\WpEntities\Interfaces\PostEntityInterface;


/**
 */
abstract class PostEntity extends Entity implements PostEntityInterface {

  protected $id;

  protected $author;

  protected $date;

  protected $dateGmt;

  protected $content;

  protected $rawContent;

  protected $title;

  protected $rawTitle;

  protected $excerpt;

  protected $rawExcerpt;

  protected $status;

  protected $name;

  protected $parent;

  protected $menuorder;

  protected $type;

  protected $commentcount;

  protected $link;

  protected $editLink;

  public function getAuthor( EntityBag $entitybag, $author )
  {
    return $this->author;
  }

  public function setAuthor( $author, EntityBag $entitybag ) {
    $this->author = $author;
    return $entitybag;
  }

  public function getDate( EntityBag $entitybag, $date )
  {
    return $this->date;
  }

  public function setDate( $date, EntityBag $entitybag ) {
    $this->date = $date;
    return $entitybag;;
  }

  public function getDateGmt( EntityBag $entitybag, $dateGmt )
  {
    return $this->dateGmt;
  }

  public function setDateGmt( $dateGmt, EntityBag $entitybag) {
    $this->dateGmt = $dateGmt;
    return $entitybag;
  }

  public function getContent( EntityBag $entitybag, $content )
  {
    return $this->content;
  }

  public function setContent( $content, EntityBag $entitybag)
  {
    $entitybag->setRawContent($content);
    return $entitybag;
  }

  public function getRawContent( EntityBag $entitybag, $rawContent )
  {
    return $this->rawContent;
  }

  public function setRawContent( $rawContent, EntityBag $entitybag)
  {
    $this->rawContent = $rawContent;
    return $entitybag;
  }

  public function getTitle( EntityBag $entitybag, $title )
  {
    return $this->title;
  }

  public function setTitle( $title, EntityBag $entitybag)
  {
    $entitybag->setRawTitle($title);
    return $entitybag;
  }

  public function getRawTitle( EntityBag $entitybag, $rawTitle )
  {
    return $this->rawTitle;
  }

  public function setRawTitle( $rawTitle, EntityBag $entitybag)
  {
    $this->rawTitle = $rawTitle;
    return $entitybag;
  }

  public function getExcerpt( EntityBag $entitybag, $excerpt )
  {
    return $this->excerpt;
  }

  public function setExcerpt( $excerpt, EntityBag $entitybag)
  {
    $entitybag->setRawExcerpt($excerpt);
    return $entitybag;
  }

  public function getRawExcerpt( EntityBag $entitybag, $rawExcerpt )
  {
    return $this->rawExcerpt;
  }

  public function setRawExcerpt( $rawExcerpt, EntityBag $entitybag )
  {
    $this->rawExcerpt = $rawExcerpt;
    return $entitybag;
  }

  public function getStatus( EntityBag $entitybag, $status )
  {
    return $this->status;
  }

  public function setStatus( $status, EntityBag $entitybag)
  {
    $this->status = $status;
    return $entitybag;
  }

  public function getName( EntityBag $entitybag, $name )
  {
    return $this->name;
  }

  public function setName( $name, EntityBag $entitybag)
  {
    $this->name = $name;
    return $entitybag;
  }

  public function getParent( EntityBag $entitybag, $parent )
  {
    return $this->parent;
  }

  public function setParent( $parent, EntityBag $entitybag)
  {
    $this->parent = $parent;
    return $entitybag;
  }

  public function getMenuorder( EntityBag $entitybag, $menuorder )
  {
    return $this->menuorder;
  }

  public function setMenuorder( $menuorder, EntityBag $entitybag)
  {
    $this->menuorder = $menuorder;
    return $entitybag;
  }

  public function getType( EntityBag $entitybag, $type )
  {
    return $this->type;
  }

  // type cant be set or changed

  // cached
  public function getCommentcount( EntityBag $entitybag, $commentcount )
  {
    return $this->commentcount;
  }

  // not in db
  public function getLink( EntityBag $entitybag, $link )
  {
    return $this->link;
  }

  public function setLink( $link, EntityBag $entitybag)
  {
    $this->link = $link;
    return $entitybag;
  }

  public function getEditLink( EntityBag $entitybag, $editLink )
  {
    return $this->editLink;
  }


  //@TODO in entity Bag, or deleter Class
  public function delete()
  {
    if( $this->getId()) {
      wp_delete_post($this->getId(), $force_delete = false );
    }
  }

  public static function configOptions()
  {
    return [
      'foo' => 2,
      'optionClass'       => 'Sef\\WpEntities\\Annotation\\PostOptions',
      'classOptions' => [],
      'propertyOptions'   => [
        'id'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'author'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'date'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'dateGmt'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'rawContent'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'content'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\FilteredContentFetcher'
        ],
        'rawTitle'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'title'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\FilteredTitleFetcher'
        ],
        'rawExcerpt'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'excerpt'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\FilteredExcerptFetcher'
        ],
        'status'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'name'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'parent'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'menuorder'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'type'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'commentcount'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\InheritFetcher'
        ],
        'link'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\PermalinkFetcher'
        ],
        'editLink'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Post\\EditLinkFetcher'
        ]
      ]
    ];
  }
}
