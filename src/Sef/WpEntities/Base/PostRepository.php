<?php
namespace Sef\WpEntities\Base;
use Sef\WpEntities\Interfaces\TermEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;


abstract class PostRepository extends Repository {

  protected $query;

	protected function __construct()
	{
    parent::__construct();
  }

  public function find( $args = [] )
  {
    $this->entityCollection->clear();
    $args = wp_parse_args( $args, $this->getConfig()->options->get('queryDefaults'));
    if( $this->fetchOnlyIds || $this->returnOnlyIds )
    {
      $args['fields'] = 'ids';
    }
    $this->query = new \WP_Query($args);

    if($this->query->have_posts()) {
      if($this->returnOnlyIds)
      {
        return new ArrayCollection($this->query->posts);
      } else
      {
        $this->entityCollection = $this->wpObjectsOrIds2EntityCollection($this->query->posts, 'post_type');
        return $this->entityCollection;
      }
    }
    return new ArrayCollection;
  }

  public function findNothing( )
  {
    $this->entityCollection->clear();
    $this->query = new \WP_Query();
    return new ArrayCollection;
  }

  public function findAll( $args = [] )
  {
    $args['posts_per_page'] = -1;
    return $this->find($args);
  }

  public function findOne( $args = [] )
  {
    $args['posts_per_page'] = 1;
    $posts =  $this->find($args);
    return $posts->first();
  }

  public function findByTerm( EntityBag $entity, $args = [] )
  {
    if( ! $entity->getInstance() instanceof TermEntityInterface )
      return $this->findNothing();

  	$args['tax_query'] = [
  		[
  			'taxonomy' => $entity->getTaxonomy(),
  			'field'    => 'term_id',
  			'terms'    => array( $entity->getId() )
  		]
  	];
    return $this->find($args);
  }

  public function findOneByTerm( EntityBag $entity, $args = [] )
  {
    if( ! $entity->getInstance() instanceof TermEntityInterface )
      return $this->findNothing();

  	$args ['posts_per_page'] = 1;
  	$args ['tax_query'] = [
  		[
  			'taxonomy' => $entity->getTaxonomy(),
  			'field'    => 'term_id',
  			'terms'    => array( $entity->getId() )
  		]
  	];
    $posts = $this->find($args);
    return $posts->first();
  }

  public function findByIds( $ids = [], $args = [] )
  {
    if ( ! is_array($ids) || empty($ids))
      return new ArrayCollection;

    $cleanIds = array_map("absint", $ids);

    $args = wp_parse_args( $args, [
      'posts_per_page' => count($ids),
      'post__in'  => $ids,
	    'orderby'   => 'post__in'
    ]);

    return $this->find($args);
  }

  public function findById( $id = 0, $args = []  )
  {
    if ( ! is_numeric($id) )
      return false; // shouldnt it be null

    $cleanId = absint($id);

    $args = wp_parse_args( $args, [
      'posts_per_page' => 1,
      'post__in'  => [$id]
    ]);

    $posts = $this->find($args);
    return $posts->first();
  }

  public function findByForeignKey( EntityBag $entity, $foreignKey = '', $args = [] )
  {
    if ( ! $foreignKey )
      return new ArrayCollection;

    $args['meta_query'] = [
  		[
  			'key'     => $foreignKey,
  			'value'   => $entity->getId(),
  			'compare' => '=',
  		]
    ];
    return $this->find($args);
  }

  public function getQuery()
  {
    return $this->query;
  }

  public function setQuery( \WP_Query $query )
  {
    $this->entityCollection->clear();
    $this->query = $query;

    if($this->query->have_posts()) {
      $this->entityCollection = $this->wpObjectsOrIds2EntityCollection($this->query->posts, 'post_type');
      return $this->entityCollection;
    }
    return new ArrayCollection;
  }
}
