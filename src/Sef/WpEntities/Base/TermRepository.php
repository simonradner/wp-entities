<?php
namespace Sef\WpEntities\Base;
use Sef\WpEntities\Interfaces\PostEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;

abstract class TermRepository extends Repository {

  protected $taxonomies = null;

	protected function __construct()
	{
    parent::__construct();

    $entities = $this->getConfig()->options->get( 'entities' );
    $this->taxonomies = $entities->getTaxonomies();
  }

  public function getTaxonomies()
  {
    return $this->taxonomies;
  }

  public function find( $args = [] )
  {
    $this->entityCollection->clear();
    $args = wp_parse_args( $args, $this->getConfig()->options->get('queryDefaults'));
    if( $this->fetchOnlyIds || $this->returnOnlyIds )
    {
      $args['fields'] = 'ids';
    }
    if ( version_compare(get_bloginfo('version'), '4.5.0', '<'))
    {
      $wpTerms = get_terms( $this->taxonomies, $args);
    } else {
      $args['taxonomy'] = $this->taxonomies;
      $wpTerms = get_terms( $args );
    }
    if(is_array($wpTerms))
    {
	  $wpTerms = array_values($wpTerms); // forces to numeric array
      $this->entityCollection = $this->wpObjectsOrIds2EntityCollection($wpTerms, 'taxonomy');
      return $this->entityCollection;
    }
    return new ArrayCollection;
  }

  public function findAll()
  {
    return $this->find();
  }

  public function findOne($args = [])
  {
    $args['number'] = 1;
    $terms =  $this->find($args);
    return $terms->first();
  }

  public function findByPost( EntityBag $entity, $args = [] )
  {
    if( ! $entity->getInstance() instanceof PostEntityInterface )
      return $this->findNothing();

    $terms = array();
    //get_the_terms
    foreach($this->getTaxonomies() as $taxonomy)
    {
      $nextterms = get_the_terms( $entity->getId(), $taxonomy );
      if( is_array($nextterms))
      {
        $terms = array_merge( $terms, $nextterms );
      }
    }
    $this->entityCollection = $this->wpObjects2EntityCollection($terms, 'taxonomy');

    return $this->entityCollection;
  }

  public function findOneByPost( EntityBag $entity, $args = [])
  {
    if( ! $entity->getInstance() instanceof PostEntityInterface )
      return $this->findNothing();

    //get_the_terms
    $terms = array();
    //get_the_terms
    foreach($this->getTaxonomies() as $taxonomy)
    {
      $nextterms = get_the_terms( $entity->getId(), $taxonomy );
      if( is_array($nextterms))
      {
        $terms = array_merge( $terms, $nextterms );
      }
    }

    if($terms) {
      $term = $terms[0];
      $this->entityCollection = $this->wpObjects2EntityCollection([$term], 'taxonomy');
      return $this->entityCollection->first();
    }
    return;
  }

  public function findOneByPostNoChildren( EntityBag $entity, $args = null )
  {
    if( ! $entity->getInstance()  instanceof PostEntityInterface )
      return $this->findNothing();

    //get_the_terms
    $terms = array();
    //get_the_terms
    foreach($this->getTaxonomies() as $taxonomy)
    {
      $foundterms = get_the_terms( $entity->getId(), $taxonomy );

      if( is_array($foundterms))
      {
        // find the terms with no children
        $parent_ids = array_map( function($term){
          return $term->parent;
        }, $foundterms);
        $term_ids = array_map( function($term){
          return $term->term_id;
        }, $foundterms);
        $parent_ids_no_null = array_filter($parent_ids);

        $passed_term_ids = array_diff( $term_ids, $parent_ids_no_null );
        $nextterms = array_filter( $foundterms, function($term) use ($passed_term_ids) {
          return ( in_array($term->term_id, $passed_term_ids ));
        });

        $terms = array_merge( $terms, $nextterms );
      }
    }

    if($terms) {
      $term = $terms[0];
      $this->entityCollection = $this->wpObjects2EntityCollection([$term], 'taxonomy');
      return $this->entityCollection->first();
    }
    return;
  }

  public function findByIds( $ids = [], $args = [] )
  {
    if ( ! is_array($ids) || empty($ids))
      return new ArrayCollection;

    $cleanIds = array_map("absint", $ids);
    $args['include'] = $ids;
    return $this->find($args);
  }

  public function findById( $id = 0 )
  {
    if ( ! is_numeric($id) )
      return false;

    $cleanId = absint($id);

    $posts = $this->find([
      'number' => 1,
      'include'  => $id
    ]);
    return $posts->first();
  }

  public static function configOptions()
  {
    return [
      'queryDefaults'       => ['hide_empty' => false],
    ];
  }

}
