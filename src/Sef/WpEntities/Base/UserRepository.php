<?php
namespace Sef\WpEntities\Base;
use Sef\WpEntities\Interfaces\PostEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;

abstract class UserRepository extends Repository {

  public function find( $args = [] )
  {
    $this->entityCollection->clear();
    $args = wp_parse_args( $args, $this->getConfig()->options->get('queryDefaults'));
    if( $this->fetchOnlyIds || $this->returnOnlyIds )
    {
      $args['fields'] = 'ID';
    }
    $query = new \WP_User_Query( $args );
    $wpUsers = $query->get_results();
    if(is_array($wpUsers))
    {
  	  $wpUsers = array_values($wpUsers); // forces to numeric array
      $this->entityCollection = $this->wpObjectsOrIds2EntityCollection($wpUsers, 'random');
      return $this->entityCollection;
    }
    return new ArrayCollection;
  }

  public function findAll()
  {
    return $this->find();
  }

  public function findByIds( $ids = [], $args = [] )
  {
    if ( ! is_array($ids) || empty($ids))
      return new ArrayCollection;

    $cleanIds = array_map("absint", $ids);

    $args = wp_parse_args( $args, [
      'number' => count($ids),
      'include'  => $ids,
	    'orderby'   => 'include'
    ]);

    return $this->find($args);
  }

  public function findById( $id = 0, $args = []  )
  {
    if ( ! is_numeric($id) )
      return false; // shouldnt it be null

    $cleanId = absint($id);

    $args = wp_parse_args( $args, [
      'number' => 1,
      'include'  => [$id]
    ]);

    $users = $this->find($args);
    return $users->first();
  }


  public static function configOptions()
  {
    return [
    ];
  }

}
