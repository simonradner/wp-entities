<?php
namespace Sef\WpEntities\Base;       
use Sef\WpEntities\Interfaces\TermEntityInterface;       

/**
 */
abstract class TermEntity extends Entity implements TermEntityInterface {  

  protected $id;

  protected $slug;

  protected $name;
 
  protected $taxonomy;
 
  protected $description;
  
  protected $termGroup;

  protected $termTaxonomyId;

  protected $parent;

  protected $count;

  protected $link;
  
  public function getSlug( EntityBag $entityBag, $slug ) 
  {
    return $this->slug;
  }
  
  public function setSlug( $slug, EntityBag $entitybag ) 
  {
    $this->slug = $slug;
    return $entitybag;
  }
  
  public function getName( EntityBag $entityBag, $name ) 
  {
    return $this->name;
  }
  
  public function setName( $name, EntityBag $entitybag ) 
  {
    $this->name = $name;
    return $entitybag;
  }

  public final function getTaxonomy() 
  {
    return $this->taxonomy;
  }
  
  // taxonomy cant be set or changed


  public function getDescription( EntityBag $entityBag, $description ) 
  {
    return $this->description;
  }
  
  public function setDescription( $description, EntityBag $entitybag ) 
  {
    $this->description = $description;
    return $entitybag;
  }
  
  public function getTermGroup( EntityBag $entityBag, $termGroup ) 
  {
    return $this->termGroup;
  }
  
  public function setTermGroup( $termGroup, EntityBag $entitybag ) 
  {
    $this->termGroup = $termGroup;
    return $entitybag;
  }
    
  public function getTermTaxonomyId( EntityBag $entityBag, $termTaxonomyId ) 
  {
    return $this->termTaxonomyId;
  }
  
  public function setTermTaxonomyId( $termTaxonomyId, EntityBag $entitybag ) 
  {
    $this->termTaxonomyId = $termTaxonomyId;
    return $entitybag;
  }
   
  public function getParent( EntityBag $entityBag, $parent ) 
  {
    return $this->parent;
  }

  public function setParent( $parent, EntityBag $entitybag ) 
  {
    $this->parent = $parent;
    return $entitybag;
  }   
  
  // cached
  public function getCount( EntityBag $entityBag, $count ) 
  {
    return $this->count;
  }

  public function setCount( $count, EntityBag $entitybag ) 
  {
    $this->count = $count;
    return $entitybag;
  }
  
  public function getLink( EntityBag $entityBag, $link )
  {
    return $this->link;
  }

  public function setLink( $link, EntityBag $entitybag)
  {
    $this->link = $link;
    return $entitybag;
  }
 
 
  
  // @TODO validate before safe ??
  public function OLDsave() 
  {
    // validate first
    $validator = $this->options['validator'];
    $errors = $validator->validate($this);
    if (count($errors) > 0) {
      return false;
    }  
  }
  


  public static function configOptions()
  {
    return [
      'foo' => 2,
      'optionClass'       => 'Sef\\WpEntities\\Annotation\\TermOptions',
      'classOptions' => [
      ],
      'propertyOptions'   => [
        'id'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Term\\InheritFetcher'
        ],
        'name'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Term\\InheritFetcher'
        ],
        'slug'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Term\\InheritFetcher'
        ],
        'taxonomy'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Term\\InheritFetcher'
        ],
        'description'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Term\\InheritFetcher'
        ],
        'termGroup'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Term\\InheritFetcher'
        ],
        'termTaxonomyId'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Term\\InheritFetcher'
        ],
        'parent'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Term\\InheritFetcher'
        ],
        'count'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Term\\InheritFetcher'
        ],
        'link'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Term\\PermalinkFetcher'
        ]
      ]
    ];
  }
}
