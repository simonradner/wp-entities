<?php
namespace Sef\WpEntities\Base;
use Sef\WpEntities\Interfaces\PostEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;

abstract class CommentRepository extends Repository {

  public function find( $args = [] )
  {
    $this->entityCollection->clear();
    $args = wp_parse_args( $args, $this->getConfig()->options->get('queryDefaults'));
    if( $this->fetchOnlyIds || $this->returnOnlyIds )
    {
      $args['fields'] = 'ids';
    }
    $query = new \WP_Comment_Query;
    $wpComments = $query->query( $args );

    if(is_array($wpComments))
    {
  	  $wpComments = array_values($wpComments); // forces to numeric array
      $this->entityCollection = $this->wpObjectsOrIds2EntityCollection($wpComments, 'random');
      return $this->entityCollection;
    }
    return new ArrayCollection;
  }

  public function findAll()
  {
    return $this->find();
  }


  public function findByPost( EntityBag $entity, $args = [] )
  {
    if( ! $entity->getInstance() instanceof PostEntityInterface )
      return $this->findNothing();

    $args['post_id'] = $entity->getId();
    return $this->find($args);
  }

  public static function configOptions()
  {
    return [
      'queryDefaults'       => ['hierarchical' => false],
    ];
  }

}
