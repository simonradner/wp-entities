<?php
namespace Sef\WpEntities\Base;

use Sef\WpEntities\WpEntities;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sef\WpEntities\Components\EntityPropertyFactory;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Sef\WpEntities\Annotation\EntityConfigOptions;
use Sef\WpEntities\Components\Config\EntityConfig;
use Sef\WpEntities\Components\WpEntity\WpEntity;

// @TODO Entityinterface !!!
abstract class SimpleEntity extends EntityBone {

  protected static $configs;

  public static function make($data = null)
  {
    $entity = new static();
    return new EntityBag($entity, $data );
  }

  protected function __construct( $entity = null ) {}

  // final public function _setProperty( PropertyInterface $property, $value )
  // {
  //   $propertyName = $property->getName();
  //   $this->$propertyName = $value;
  // }
  //
  // final public function _getProperty( PropertyInterface $property )
  // {
  //   $propertyName = $property->getName();
  //   return $this->$propertyName;
  // }
  //
  // final public function _unsetProperty( PropertyInterface $property )
  // {
  //   $propertyName = $property->getName();
  //   unset($this->{$propertyName});
  // }
  //
  // public static final function getConfig()
  // {
  //   if ( ! isset(static::$configs[get_called_class()])) {
  //     static::$configs[get_called_class()] = new EntityConfig(get_called_class(), new EntityConfigOptions());
  //   }
  //   return static::$configs[get_called_class()];
  // }

  public static function configOptions()
  {
    return [
      'foo' => 2,
      'optionClass'       => 'Sef\\WpEntities\\Annotation\\SimpleEntityOptions',
      'classOptions' => [
        'importerStrategy' => 'Sef\\WpEntities\\Components\\Importer\\SetterImporterStrategy'
      ],
      'propertyOptions'   => [
      ]
    ];
  }
}
