<?php
namespace Sef\WpEntities\Base;
use Sef\WpEntities\Interfaces\CommentEntityInterface;


/**
 */
abstract class CommentEntity extends Entity implements CommentEntityInterface {


  protected $id;

  protected $postId;

  protected $authorName;

  protected $authorEmail;

  protected $authorUrl;

  protected $authorIP;

  protected $date;

  protected $dateGmt;

  protected $content;

  protected $karma;

  protected $approved;

  protected $agent;

  protected $type;

  protected $parent;

  protected $userId;


  public function getPostId( EntityBag $entityBag, $postId )
  {
    return $postId;
  }

  public function setPostId( $postId, EntityBag $entityBag )
  {
    $this->postId = $postId;
    return $this;
  }

  public function getAuthorName( EntityBag $entitybag, $authorName )
  {
    return $this->authorName;
  }

  public function setAuthorName( $authorName, EntityBag $entitybag ) {
    $this->authorName = $authorName;
    return $entitybag;
  }

  public function getAuthorEmail( EntityBag $entityBag, $authorEmail )
  {
    return $authorEmail;
  }

  public function setAuthorEmail( $authorEmail, EntityBag $entityBag )
  {
    $this->authorEmail = $authorEmail;
    return $this;
  }

  public function getAuthorUrl( EntityBag $entityBag, $authorUrl )
  {
    return $authorUrl;
  }

  public function setAuthorUrl( $authorUrl, EntityBag $entityBag )
  {
    $this->authorUrl = $authorUrl;
    return $this;
  }

  public function getAuthorIP( EntityBag $entityBag, $authorIP )
  {
    return $authorIP;
  }

  public function setAuthorIP( $authorIP, EntityBag $entityBag )
  {
    $this->authorIP = $authorIP;
    return $this;
  }

  public function getDate( EntityBag $entitybag, $date )
  {
    return $this->date;
  }

  public function setDate( $date, EntityBag $entitybag ) {
    $this->date = $date;
    return $entitybag;;
  }

  public function getDateGmt( EntityBag $entitybag, $dateGmt )
  {
    return $this->dateGmt;
  }

  public function setDateGmt( $dateGmt, EntityBag $entitybag) {
    $this->dateGmt = $dateGmt;
    return $entitybag;
  }

  public function getContent( EntityBag $entitybag, $content )
  {
    return $this->content;
  }

  public function setContent( $content, EntityBag $entitybag)
  {
    $this->content = $content;
    return $entitybag;
  }

  public function getKarma( EntityBag $entityBag, $karma )
  {
    return $karma;
  }

  public function setKarma( $karma, EntityBag $entityBag )
  {
    $this->karma = $karma;
    return $this;
  }

  public function getApproved( EntityBag $entityBag, $approved )
  {
    return $approved;
  }

  public function setApproved( $approved, EntityBag $entityBag )
  {
    $this->approved = $approved;
    return $this;
  }

  public function getAgent( EntityBag $entityBag, $agent )
  {
    return $agent;
  }

  public function setAgent( $agent, EntityBag $entityBag )
  {
    $this->agent = $agent;
    return $this;
  }

  public function getType( EntityBag $entityBag, $type )
  {
    return $type;
  }

  public function setType( $type, EntityBag $entityBag )
  {
    $this->type = $type;
    return $this;
  }

  public function getParent( EntityBag $entitybag, $parent )
  {
    return $this->parent;
  }

  public function setParent( $parent, EntityBag $entitybag)
  {
    $this->parent = $parent;
    return $entitybag;
  }

  public function getUserId( EntityBag $entityBag, $userId )
  {
    return $userId;
  }

  public function setUserId( $userId, EntityBag $entityBag )
  {
    $this->userId = $userId;
    return $this;
  }

  public static function configOptions()
  {
    return [
      'foo' => 2,
      'optionClass'       => 'Sef\\WpEntities\\Annotation\\CommentOptions',
      'classOptions' => [],
      'propertyOptions'   => [
        'id'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'postId'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'authorName'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'authorEmail'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'authorUrl'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'authorIP'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'date'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'dateGmt'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'content'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'karma'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'approved'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'agent'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'type'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'parent'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ],
        'userId'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\Comment\\InheritFetcher'
        ]
      ]
    ];
  }
}
