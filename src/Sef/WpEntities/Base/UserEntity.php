<?php
namespace Sef\WpEntities\Base;
use Sef\WpEntities\Interfaces\UserEntityInterface;


/**
 */
abstract class UserEntity extends Entity implements UserEntityInterface {

  protected $id;

  protected $firstName;

  protected $lastName;

  protected $roles; // only getter

  protected $role; // only setter
  
  protected $login;

  protected $pass;

  protected $nicename;

  protected $email;

  protected $url;

  protected $displayName;

  protected $registered;

  protected $description;

  protected $status;

  protected $activationKey;

  protected $spam;

  protected $deleted;

  protected $nickname;

  protected $richEditing;

  protected $locale;

  protected $caps;

  protected $allCaps;

  public function getFirstName( EntityBag $entityBag, $firstName )
  {
    return $firstName;
  }

  public function setFirstName( $firstName, EntityBag $entityBag )
  {
    $this->firstName = $firstName;
    return $entityBag;
  }

  public function getLastName( EntityBag $entityBag, $lastName )
  {
    return $lastName;
  }

  public function setLastName( $lastName, EntityBag $entityBag )
  {
    $this->lastName = $lastName;
    return $entityBag;
  }

  // you can only get all roles
  public function getRoles( EntityBag $entityBag, $roles )
  {
    return $roles;
  }
  // no roles setter

  // for saving WP suports only one role, all previous roles get overwritten
  public function setRole( $role, EntityBag $entityBag )
  {
    $this->role = $role;
    return $entityBag;
  }
  // no roles getter

  public function getLogin( EntityBag $entityBag, $login )
  {
    return $login;
  }

  public function setLogin( $login, EntityBag $entityBag )
  {
    $this->login = $login;
    return $entityBag;
  }

  public function getPass( EntityBag $entityBag, $pass )
  {
    return $pass;
  }

  public function setPass( $pass, EntityBag $entityBag )
  {
    $this->pass = $pass;
    return $entityBag;
  }

  public function getNicename( EntityBag $entityBag, $nicename )
  {
    return $nicename;
  }

  public function setNicename( $nicename, EntityBag $entityBag )
  {
    $this->nicename = $nicename;
    return $entityBag;
  }

  public function getEmail( EntityBag $entityBag, $email )
  {
    return $email;
  }

  public function setEmail( $email, EntityBag $entityBag )
  {
    $this->email = $email;
    return $entityBag;
  }

  public function getUrl( EntityBag $entityBag, $url )
  {
    return $url;
  }

  public function setUrl( $url, EntityBag $entityBag )
  {
    $this->url = $url;
    return $entityBag;
  }

  public function getDisplayName( EntityBag $entityBag, $displayName )
  {
    return $displayName;
  }

  public function setDisplayName( $displayName, EntityBag $entityBag )
  {
    $this->displayName = $displayName;
    return $entityBag;
  }

  public function getRegistered( EntityBag $entityBag, $registered )
  {
    return $registered;
  }

  public function setRegistered( $registered, EntityBag $entityBag )
  {
    $this->registered = $registered;
    return $entityBag;
  }

  public function getDescription( EntityBag $entityBag, $description )
  {
    return $description;
  }

  public function setDescription( $description, EntityBag $entityBag )
  {
    $this->description = $description;
    return $entityBag;
  }

  public function getStatus( EntityBag $entityBag, $status )
  {
    return $status;
  }

  public function getActivationKey( EntityBag $entityBag, $activationKey )
  {
    return $activationKey;
  }

  public function getSpam( EntityBag $entityBag, $spam )
  {
    return $spam;
  }

  public function getDeleted( EntityBag $entityBag, $deleted )
  {
    return $deleted;
  }

  public function getNickname( EntityBag $entityBag, $nickname )
  {
      return $nickname;
  }

  public function setNickname( $nickname, EntityBag $entityBag )
  {
      $this->nickname = $nickname;
      return $entityBag;
  }

  public function getRichEditing( EntityBag $entityBag, $richEditing )
  {
      return $richEditing;
  }
  
  public function setRichEditing( $richEditing, EntityBag $entityBag )
  {
      $this->richEditing = $richEditing;
      return $entityBag;
  }

  public function getLocale( EntityBag $entityBag, $locale )
  {
      return $locale;
  }
  
  public function setLocale( $locale, EntityBag $entityBag )
  {
      $this->locale = $locale;
      return $entityBag;
  }
  
  public function getCaps( EntityBag $entityBag, $caps )
  {
      return $caps;
  }

  public function getAllCaps( EntityBag $entityBag, $allCaps )
  {
      return $allCaps;
  }

  public static function configOptions()
  {
    return [
      'foo' => 2,
      'optionClass'       => 'Sef\\WpEntities\\Annotation\\UserOptions',
      'classOptions' => [],
      'propertyOptions'   => [
        'id'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'firstName'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\MetaFetcher',
          'wpname' => 'first_name'
        ],
        'lastName'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\MetaFetcher',
          'wpname' => 'last_name'
        ],
        'roles'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'role'  => [
            // no fetcher
        ],
        'login'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'pass'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'nicename'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'email'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'url'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'displayName'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'registered'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'description'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\MetaFetcher',
          'wpname' => 'description'
        ],
        'status'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'activationKey'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'spam'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'deleted'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'nickname'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\MetaFetcher',
          'wpname' => 'nickname'          
        ],
        'richEditing'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\MetaFetcher',
          'wpname' => 'rich_editing'                    
        ],
        'locale'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\MetaFetcher',
          'wpname' => 'locale'                    
        ],
        'caps'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher'
        ],
        'allCaps'  => [
          'fetcher' => 'Sef\\WpEntities\\Components\\Fetcher\\Leaf\\User\\InheritFetcher',
          'wpname' => 'allcaps'                    
        ],
      ]
    ];
  }
}
