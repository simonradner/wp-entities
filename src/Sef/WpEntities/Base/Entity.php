<?php
namespace Sef\WpEntities\Base;
use Sef\WpEntities\WpEntities;
use Sef\WpEntities\Components\EntityPropertyFactory;
use Sef\WpEntities\Components\Config\EntityConfig;
use Sef\WpEntities\Components\WpEntity\WpEntity;
use Sef\WpEntities\Interfaces\PropertyInterface;
use Sef\WpEntities\Interfaces\Fetchable;
use Sef\WpEntities\Interfaces\Saveable;
use Sef\WpEntities\Interfaces\WpEntityKeeperInterface;
use Sef\WpEntities\Annotation\EntityConfigOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;


abstract class Entity extends EntityBone implements WpEntityKeeperInterface, Fetchable, Saveable {

  private $wpEntity;

  protected $id;

  public final function getId( EntityBag $entityBag, $id )
  {
    return $this->id;
  }

  public final function setId($id)
  {
    $this->id = $id;
  }

  final public function setWpEntity( WpEntity $wpEntity )
  {
    $this->wpEntity = $wpEntity;
  }

  final public function getWpEntity()
  {
    return $this->wpEntity;
  }

  public function __clone()
  {
//     static::$configs = new \stdClass;
    unset($this->wpEntity);
  }

}
