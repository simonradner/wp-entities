<?php


define('WP_ENTITIES_TESTS_PATH', dirname( __FILE__ ) );

define('WP_ENTITIES_TESTS_CACHE_PATH', WP_ENTITIES_TESTS_PATH . '/cache' );

define('WP_ENTITIES_TESTS_VENDOR_PATH', dirname(dirname(dirname(dirname( __FILE__ )))));

require WP_ENTITIES_TESTS_VENDOR_PATH . '/autoload.php';


add_action('plugins_loaded', function(){

   Sef\WpEntities\WpEntities::build([
     'cacheDir' => WP_ENTITIES_TESTS_CACHE_PATH
   ]);
  
});

   

/*

add_action('wp_footer', function(){

      $entity = Sef\WpEntities\Entities\Post::make(1);
//	$entity->setCategory('Music');
	$entity->setCategories(['Video', 'Music']);

//	$entity->save();
$post = $entity->export();
//     echo '<pre>'; print_r($post); echo '</pre>';

    
    

});
*/