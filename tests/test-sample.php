<?php
/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
class SampleTest extends WP_UnitTestCase {

  
  function setUp()
  {
    parent::setUp();
    //register_taxonomy( 'wptests_tax', 'post' );

  	echo '<pre>'; print_r(get_bloginfo('version')); echo '</pre>';


  }

	/**
	 * A single example test.
	 */
	function test_createPost() 
	{
  		
    $entity = \Sef\WpEntities\Entities\Post::make();
    $entity->setTitle('Foo');
    
		$this->assertEquals( $entity->getRawTitle(), 'Foo');
 		$this->assertEquals( 'post', $entity->getType());
		$this->assertInstanceOf('Sef\\WpEntities\\Base\\EntityBag', $entity);
		$this->assertInstanceOf('Sef\\WpEntities\\Base\\Entity', $entity->getInstance());
 		$this->assertTrue( $entity->isNew());

    $entity->save();
    
 		$this->assertFalse( $entity->isNew());
		$this->assertInternalType( 'integer', $entity->getId());
		$this->assertEquals( 'Foo', $entity->getTitle());
//     		echo '<pre>'; print_r($entity->export()); echo '</pre>';

    return $entity;

	}

	function test_usePost() 
	{

$post_id_array = $this->factory->post->create_many( 4 );

    $post_id = $this->factory->post->create( array( 'post_title' => 'Bar') );
    $entity = \Sef\WpEntities\Entities\Post::make($post_id);
		$this->assertEquals( 'Bar', $entity->getTitle());
		$this->assertInstanceOf('Sef\\WpEntities\\Base\\Entity', $entity->export());
//  		echo '<pre>'; print_r($entity->export()); echo '</pre>';

	}

	function test_postAssignCats() 
	{
    $post_id = $this->factory->post->create( array( 'post_title' => 'Music Post')); 
    $cat_id1 = $this->factory->term->create( array( 'name' => 'Music', 'taxonomy' => 'category') );
    $cat_id2 = $this->factory->term->create( array( 'name' => 'Video', 'taxonomy' => 'category') );

//       		echo '<pre>'; print_r($cat_id2); echo '</pre>';
//  		echo '<pre>'; print_r(get_term($cat_id2, 'category')); echo '</pre>';


//     $entity = \Sef\WpEntities\Entities\Post::make($post_id);
/*
    $entity = \Sef\WpEntities\Entities\Category::make(1);


    $c = new Sef\WpEntities\Components\WpEntity\Constructor\PostConstructor;
    $c->setData($post_id);
    $wpE = $c->get();
*/
//       		echo '<pre>'; print_r($wpE); echo '</pre>';

    
    
    $c = new Sef\WpEntities\Components\WpEntity\Constructor\TermConstructorByName;
    
    $c->setData('Video');
    $c->setTaxonomy('category');
    $wpE = $c->get();

    
// 		echo '<pre>'; print_r(get_term(1, 'category')); echo '</pre>';

//     $entity->setCategory('Music');
//     $entity->save();

//     $entity->export();
    
/*
    $category = $entity->getCategory();
		$this->assertEquals( 'music', $entity->getSlug());
		$this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $category);
		$this->assertInstanceOf( 'Sef\\WpEntities\\Entities\\Category', $category->getInstance());
		echo '<pre>'; print_r($category->getInstance()); echo '</pre>';
		
    $categories = $entity->getCategories();
		echo '<pre>'; print_r($categories); echo '</pre>';
		$this->assertInstanceOf( 'Doctrine\\Common\\Collections\\ArrayCollection', $$categories);
		
*/
		
	}

}

