<?php
/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
abstract class TestGeneralConstructor extends WP_UnitTestCase {
  
  
  function setUp()
  {
    parent::setUp();
  }

  static function makeConstructor()
  {
     // implement it
     throw new Exception('static method makeConstructor must be implemented');
  }


  function test_Interface() 
	{
    $c = static::makeConstructor();

		$this->assertInstanceOf( "Sef\WpEntities\Interfaces\WpEntityConstructorInterface", $c);	
	}

}

