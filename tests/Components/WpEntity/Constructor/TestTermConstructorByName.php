<?php
/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
class TestTermConstructorByName extends TestGeneralConstructor {

  
  function setUp()
  {
    parent::setUp();
  }

  static function makeConstructor() 
  {
  	return new Sef\WpEntities\Components\WpEntity\Constructor\TermConstructorByName;
	}
	
  function test_constructByName() 
	{

    $cat_id1 = $this->factory->term->create( array( 'name' => 'Music', 'taxonomy' => 'category') );
      
    $wpE = static::makeConstructor()->setData('Music')->setTaxonomy('category')->get();

		$this->assertInstanceOf( 'Sef\\WpEntities\\Components\\WpEntity\\Term', $wpE);	
		
	}

  function test_constructById() 
	{

    // construct WPEntity by id  

    $cat_id1 = $this->factory->term->create( array( 'name' => 'Music', 'taxonomy' => 'category') );
    
    $wpE = static::makeConstructor()->setData($cat_id1)->setTaxonomy('category')->get();

		$this->assertInstanceOf( 'Sef\\WpEntities\\Components\\WpEntity\\Term', $wpE);
		
	}

	function test_createByObject() 
	{
    $cat_id1 = $this->factory->term->create( array( 'name' => 'Music', 'taxonomy' => 'category') );
    $term = get_term($cat_id1);
      
    // construct WPEntity by WP_Post  
    $queryCountBefore = get_num_queries();
    $wpE = static::makeConstructor()->setData($term)->get();
    $queryCountAfter = get_num_queries();
		$this->assertInstanceOf( 'Sef\\WpEntities\\Components\\WpEntity\\Term', $wpE);
    
    // value must be cached
    $this->assertEquals( $queryCountBefore, $queryCountAfter);				
	}
	
  function test_constructFail() 
	{

    // construct WPEntity by id  

    $cat_id1 = $this->factory->term->create( array( 'name' => 'Music', 'taxonomy' => 'category') );
    
    $wpE = static::makeConstructor()->setData('foo')->setTaxonomy('category')->get();

    // @TODO
    // as we are lazy loading all, we dont know right after setting the data in the constructor if the term exists
    
		//$this->assertNull($wpE);
		
	}

  function test_ExtendedInterface() 
	{
    $c = static::makeConstructor();

		$this->assertInstanceOf( "Sef\WpEntities\Interfaces\WpEntityTermConstructorInterface", $c);	
	}
}

