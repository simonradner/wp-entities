<?php
/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
class TestPostConstructor extends TestGeneralConstructor {

  
  function setUp()
  {
    parent::setUp();
  }

  static function makeConstructor() 
  {
  	return new Sef\WpEntities\Components\WpEntity\Constructor\PostConstructor;
	}

	function test_construct() 
	{
    $post_id = $this->factory->post->create( array( 'post_title' => 'Music Post')); 
    
    // construct WPEntity by id  
      
    $c = static::makeConstructor()->setData($post_id);
    $wpE = $c->get();
    
		$this->assertInstanceOf( 'Sef\\WpEntities\\Components\\WpEntity\\Post', $wpE);		
	}

	function test_constructCollection() 
	{
    $post_id1 = $this->factory->post->create( array( 'post_title' => 'Music Post')); 
    $post_id2 = $this->factory->post->create( array( 'post_title' => 'Video Post')); 
    
    // construct WPEntity by ids  
      
    $c = static::makeConstructor()->setData([$post_id1,$post_id2 ]);
    $wpEs = $c->get();
    
		$this->assertInstanceOf( 'Doctrine\\Common\\Collections\\ArrayCollection', $wpEs);
		
		foreach($wpEs as $wpE) 
		{
		  $this->assertInstanceOf( 'Sef\\WpEntities\\Components\\WpEntity\\Post', $wpE);				
		}		
	}

	function test_constructFail() 
	{
    $post_id = $this->factory->post->create( array( 'post_title' => 'Music Post')); 
    
    // construct WPEntity by id  
      
    $c =static::makeConstructor()->setData('foo');
    $wpE = $c->get();
    
		$this->assertNull($wpE);	
	}

	function test_constructCollectionFail() 
	{
    $post_id1 = $this->factory->post->create( array( 'post_title' => 'Music Post')); 
    $post_idBad = 'bad';
    
    // construct WPEntity by ids  
      
    $c = static::makeConstructor()->setData([$post_id1,$post_idBad ]);
    $wpEs = $c->get();
    
		$this->assertInstanceOf( 'Doctrine\\Common\\Collections\\ArrayCollection', $wpEs);
		
    $this->assertInstanceOf( 'Sef\\WpEntities\\Components\\WpEntity\\Post', $wpEs->get(0));				
    $this->assertNull( $wpEs->get(1));				

	}

	function test_createByObject() 
	{
    $post_id = $this->factory->post->create( array( 'post_title' => 'Music Post')); 
    $post = get_post($post_id);
      
    // construct WPEntity by WP_Post  
    $queryCountBefore = get_num_queries();
    $c = static::makeConstructor()->setData($post_id);
    $wpE = $c->get();
    $queryCountAfter = get_num_queries();
    $this->assertInstanceOf( 'Sef\\WpEntities\\Components\\WpEntity\\Post', $wpE);				
    
    // value must be cached
    $this->assertEquals( $queryCountBefore, $queryCountAfter);				
	}
}

