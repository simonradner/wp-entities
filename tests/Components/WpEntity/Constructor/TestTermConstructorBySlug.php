<?php
/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
class TestTermConstructorBySlug extends TestGeneralConstructor {

  
  function setUp()
  {
    parent::setUp();
  }

  static function makeConstructor() {
  	return new Sef\WpEntities\Components\WpEntity\Constructor\TermConstructorBySlug;
	}
	
  function test_constructBySlug() 
	{

    $cat_id1 = $this->factory->term->create( array( 'name' => 'Music My', 'taxonomy' => 'category', 'slug' => 'my-music') );
  
    
    $wpE = static::makeConstructor()->setData('my-music')->setTaxonomy('category')->get();

		$this->assertInstanceOf( 'Sef\\WpEntities\\Components\\WpEntity\\Term', $wpE);	
		$this->assertInstanceOf( 'WP_Term', $wpE->getRawObject());	
		$this->assertEquals( 'Music My', $wpE->getRawObject()->name);	
		
	}

}

