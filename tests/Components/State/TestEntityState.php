<?php
use Sef\WpEntities\Components\EntityProperty;     
use Sef\WpEntities\Entities\Post;
use Sef\WpEntities\Entities\Category;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
class TestEntityState extends WP_UnitTestCase {
  
  protected $postId;
  protected $categoryCollection;
  
  function setUp()
  {
    parent::setUp();

    $this->postId = $this->factory->post->create([
      'post_title' => 'My Blog Post', 
      'post_type' => 'post',
    ]);
    
    $categories = new ArrayCollection;
    $category_ids = $this->factory->term->create_many(3, ['taxonomy' => 'category']);
    foreach($category_ids as $category_id ) 
    {
      $categories->add(Category::make($category_id));
    }
    $this->categoryCollection = $categories;
  }
  
  function test_FetchStateOnNewEntity()
  {
    $e = Post::make();
    $this->assertFalse($e->state->wasFetched($e->config->getProperty('title')));	
    $this->assertFalse($e->state->wasFetched($e->config->getProperty('date')));	
    $this->assertFalse($e->state->wasFetched($e->config->getProperty('content')));	
  }
  
  function test_FetchStateOnInitEntity()
  {
    $e = Post::make($this->postId);
    $e->getId();
    
    $this->assertTrue($e->state->wasFetched($e->config->getProperty('id')));	
    $this->assertFalse($e->state->wasFetched($e->config->getProperty('date')));	
    
     $this->assertFalse($e->state->wasFetched($e->config->getProperty('content')));	
     $this->assertFalse($e->state->wasFetched($e->config->getProperty('title')));
  }

  function test_FetchStateOnLacyLoad()
  {
    $e = Post::make($this->postId);
    $e->getContent();
    $this->assertTrue($e->state->wasFetched($e->config->getProperty('content')));	
  }

  function test_SetStateOnNewEntity()
  {
    $e = Post::make();
    $this->assertFalse($e->state->wasSet($e->config->getProperty('title')));	
    $this->assertFalse($e->state->wasSet($e->config->getProperty('date')));	
    $this->assertFalse($e->state->wasSet($e->config->getProperty('content')));	
    $this->assertFalse($e->state->wasSet($e->config->getProperty('categories')));	
    
    $e->setRawTitle('new Title');
    $e->setRawContent('new Content');
    $e->setCategories($this->categoryCollection);

    $this->assertTrue($e->state->wasSet($e->config->getProperty('rawTitle')));	
    $this->assertTrue($e->state->wasSet($e->config->getProperty('rawContent')));
    $this->assertTrue($e->state->wasSet($e->config->getProperty('categories')));
    $this->assertFalse($e->state->wasSet($e->config->getProperty('date')));	

  }

  function test_SetStateOnInitEntity()
  {
    $e = Post::make($this->postId);
    
    $this->assertFalse($e->state->wasSet($e->config->getProperty('categories')));	
    $this->assertFalse($e->state->wasSet($e->config->getProperty('title')));	
    $this->assertFalse($e->state->wasSet($e->config->getProperty('date')));	
    $this->assertFalse($e->state->wasSet($e->config->getProperty('content')));	
    
    $e->setRawTitle('new Title');
    $e->setRawContent('new Content');
    $e->setCategories($this->categoryCollection);
    
    $this->assertTrue($e->state->wasSet($e->config->getProperty('rawTitle')));	
    $this->assertTrue($e->state->wasSet($e->config->getProperty('rawContent')));
    $this->assertTrue($e->state->wasSet($e->config->getProperty('categories')));
    $this->assertFalse($e->state->wasSet($e->config->getProperty('date')));	

  }

}
