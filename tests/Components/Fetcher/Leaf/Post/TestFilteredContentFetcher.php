<?php
use Sef\WpEntities\Components\EntityProperty;     
use Sef\WpEntities\Entities\Post;
use Sef\WpEntities\Components\Fetcher\Leaf\Post\FilteredContentFetcher;

class TestFilteredContentFetcher extends WP_UnitTestCase 
{
  protected $entity;
  
  protected $wpEntity;
  
  function setUp()
  {
    parent::setUp();

    $postId = $this->factory->post->create([
      'post_title' => 'My Blog Post', 
      'post_type' => 'post',
      'post_content' => 'My Post Content', 
    ]);
    
    $this->entity = Post::make($postId);
    $this->wpEntity = $this->entity->getWpEntity();
  }
  
  function test_Fetch()
  {
    $entity = $this->entity;
    $config = $entity->config;
    $fetcher = new FilteredContentFetcher( $entity, $this->wpEntity, $config->getProperty('content'));
    $result = $fetcher->fetch();
    
    
    $this->assertInstanceOf( 'Sef\\WpEntities\\Components\\Fetcher\\FetchResult', $result);	
    $this->assertEquals( '<p>My Post Content</p>', trim($result->value));	

  }
}
