<?php
use Sef\WpEntities\Entities\Tag;       
use Sef\WpEntities\Annotation\TermOptions as Options;

 /**
   * @Options(
   *  taxonomy="test_route_tax"
   * )
   */
class RouteEntity extends Tag {

  protected $id;

  protected $slug;

  protected $name;
 
  protected $taxonomy;
 
  protected $description;
  
  protected $termGroup;

  protected $termTaxonomyId;

  protected $parent;

  protected $count;

  protected $link;
  
}

