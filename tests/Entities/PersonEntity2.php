<?php

use Sef\WpEntities\Entities\Post;
use Sef\WpEntities\Annotation\PostOptions as Options;
use Doctrine\Common\Collections\ArrayCollection;

/**
   * @Options(
   *  post_type="test_person"
   * )
   */
class PersonEntity2 extends Post {

  protected $id;

  protected $type;

  protected $date;

  protected $author;

  protected $title;

  protected $status;

  protected $excerpt;

  protected $editExcerpt;

  protected $content;

  protected $editContent;

  protected $link;


 /**
   * @Options(
   *  type="entity",
   *  entity="RouteEntity",
   *  repository="RouteRepository",
   *  setterConverter="Sef\WpEntities\Components\Converter\PropertyConverter\Name2TermPropertyConverter"
   * )
   */
  protected $route;


 /**
   * @Options(
   *  type="meta",
   * )
   */
  protected $routemeta;

  protected function __construct( $post = null ) 
  {
    parent::__construct($post);
  }



  public function getRoute()
  {
    return $this->route;
  }

  public function setRoute($route)
  {
      $this->route = $route;
      return $this;        
  }  

  public function getRoutemeta()
  {
    return $this->routemeta;
  }

  public function setRoutemeta($routemeta)
  {
      $this->routemeta = $routemeta;
      return $this;        
  }    
  

  public static function configOptions()
  {
    return [
      'foo' => 4,
      'classOptions' => [
        'foo' => 3
      ],
      'propertyOptions'   => [
        'date'  => [
        ]
      ]
    ];
  }

}

