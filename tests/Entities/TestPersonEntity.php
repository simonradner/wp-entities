<?php
/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
class TestPersonEntity extends WP_UnitTestCase {
  
  const POST_TYPE = 'test_person';
  
  private $postId;
  
  function setUp()
  {
    parent::setUp();
    register_post_type( self::POST_TYPE );
    $this->postId = $this->factory->post->create([
      'post_title' => 'John Doe', 
      'post_type' => self::POST_TYPE,
    ]);
    
    // add categories and tags to post
    $category_ids = $this->factory->term->create_many(3, ['taxonomy' => 'category']);
    $tag_ids = $this->factory->term->create_many(4, ['taxonomy' => 'post_tag']);
    wp_set_object_terms( $this->postId, $category_ids, 'category', false );
    wp_set_object_terms( $this->postId, $tag_ids, 'post_tag', false );

  }

  function test_initEntity()
  {

    // empty entity
    $e = PersonEntity::make();   
    $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $e);
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $e->getInstance());
    $this->assertEquals( self::POST_TYPE , $e->getType());
    $this->assertNull( $e->getId());
    $this->assertTrue( $e->isNew());
    
    // setting and getting
    $e->setEmail('foo@bar.com');
    $this->assertEquals( 'foo@bar.com' , $e->getEmail());
   

    // entity from db, getting
    $e = PersonEntity::make($this->postId);
    $this->assertEquals( $this->postId , $e->getId());
    $this->assertEquals( 'John Doe' , $e->getRawTitle());
    $this->assertEquals( 'John Doe' , $e->getTitle());


    // saving meta
    $e = PersonEntity::make($this->postId);
    $e->setEmail('foo@bar.com');
    $e->save();
    $this->assertEquals( 'foo@bar.com' , get_post_meta($this->postId, 'email', true));
    $e = PersonEntity::make($this->postId);
    $this->assertEquals( 'foo@bar.com' , $e->getEmail());
  }
}

