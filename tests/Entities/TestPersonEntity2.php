<?php
/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
class TestPersonEntity2 extends WP_UnitTestCase 
{
  
  const POST_TYPE = 'test_person';

  const CUSTOM_TAXONOMY = 'test_route_tax';

  
  private $postId;
  
  function setUp()
  {
    parent::setUp();
    register_post_type( self::POST_TYPE );
    register_taxonomy(self::CUSTOM_TAXONOMY, self::POST_TYPE);

    $this->postId = $this->factory->post->create([
      'post_title' => 'John Doe 2', 
      'post_type' => self::POST_TYPE,
    ]);

    // many posts
    $this->factory->post->create_many(100);
    
    
    // add categories and tags to post
    $route_id = $this->factory->term->create(['taxonomy' => self::CUSTOM_TAXONOMY]);
    wp_set_object_terms( $this->postId, $route_id, self::CUSTOM_TAXONOMY, false );

  }


  function test_savingTermEntity()
  {
    $eRoute = RouteEntity::make();
    $eRoute->setName('mytestroute');
    $eRoute->save();    
    
    $wp_term_object = get_term_by('name', 'mytestroute', self::CUSTOM_TAXONOMY);
    $this->assertInstanceOf( 'WP_Term', $wp_term_object);
    $this->assertEquals( 'mytestroute', $wp_term_object->name);
  }

  function test_oneToOneTermEntity()
  {
    $e = PersonEntity2::make($this->postId);
    $eRoute = RouteEntity::make();
    $eRoute->setName('mytestroute');
    $eRoute->save();  

    $e->setRoute('mytestroute');  // error when passing the object, check term name converter
    $e->setRoutemeta('mytestroute');
    $e->save();
    
    // load the post by term and by meta
    
    $queryTax = new \WP_Query([
      'post_type' => self::POST_TYPE,
      'posts_per_page'  => 1,
      'tax_query' => [
        [
          'taxonomy' => self::CUSTOM_TAXONOMY,
          'field'    => 'name',
          'terms'    => 'mytestroute',
        ],
      ],
    ]);
      
    $this->assertEquals( 1, count($queryTax->posts));
    $this->assertInstanceOf( 'WP_Post', $queryTax->posts[0]);
    $this->assertEquals( 'John Doe 2', $queryTax->posts[0]->post_title);



    $queryMeta = new \WP_Query([
      'post_type' => self::POST_TYPE,
      'posts_per_page'  => 1,
      'meta_query' => [
        [
          'key' => 'routemeta',
          'value'    => 'mytestroute',
        ],
      ],
    ]);
      
    $this->assertEquals( 1, count($queryMeta->posts));
    $this->assertInstanceOf( 'WP_Post', $queryMeta->posts[0]);
    $this->assertEquals( 'John Doe 2', $queryMeta->posts[0]->post_title);


  }
}

