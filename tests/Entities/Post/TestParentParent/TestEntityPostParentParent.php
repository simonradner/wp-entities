<?php
/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
class TestEntityPostParentParent extends AbstractEntityPostTest {
  
  const POST_TYPE1 = 'testposttype';

  const POST_TYPE2 = 'testposttype';
  
  protected $postId1;

  protected $postId2;

  protected $postId3;

  protected $categories;

  protected $tags;

  protected $category;

  protected $tag;
  
  function setUp()
  {
    parent::setUp();

    $this->postId3 = $this->factory->post->create([
      'post_title' => 'Post Title 3', 
      'post_type' => static::POST_TYPE2,
    ]);
  }

  function test_setEntity()
  {
    // set by entity
    $parentEntity = ParentEntity::make($this->postId1);
    $childEntity = ParentEntity::make($this->postId2);
    $child2Entity = ParentEntity::make($this->postId3);

    $parentEntity->setParent($childEntity);
    $childEntity->setParent($child2Entity);
    $childE = $parentEntity->getParent();
    
    
    $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $childE);
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $childE->getInstance());
    $this->assertEquals( static::POST_TYPE2 , $childE->getType());
    $this->assertEquals( 'Post Title 2' , $childE->getTitle());    

    $child2E = $childE->getParent();


    $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $child2E);
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $child2E->getInstance());
    $this->assertEquals( static::POST_TYPE2 , $child2E->getType());
    $this->assertEquals( 'Post Title 3' , $child2E->getTitle());    

    // set by ID
    
    // the following would only work if an entity and repostory is set via annotations
/*
    $parentEntity = ParentEntity::make($this->postId1);

    $parentEntity->setParent($this->postId2);
    $e = $parentEntity->getParent();
        
    $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $e);
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $e->getInstance());
    $this->assertEquals( static::POST_TYPE2 , $e->getType());
    $this->assertEquals( 'Post Title 2' , $e->getTitle()); 
*/
  }

  function test_fetchEntity()
  {
    $postId2 = $this->factory->post->create([
      'post_title' => 'Post Title 2', 
      'post_type' => static::POST_TYPE1,
      'post_parent' => $this->postId3
    ]);
    $postId = $this->factory->post->create([
      'post_title' => 'Post Title', 
      'post_type' => static::POST_TYPE1,
      'post_parent' => $postId2
    ]);
    $parentEntity = ParentEntity::make($postId);
    
    $childE = $parentEntity->getParent();
        
    $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $childE);
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $childE->getInstance());
    $this->assertEquals( static::POST_TYPE2 , $childE->getType());
    $this->assertEquals( 'Post Title 2' , $childE->getTitle());    

    $child2E = $childE->getParent();


    $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $child2E);
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $child2E->getInstance());
    $this->assertEquals( static::POST_TYPE2 , $child2E->getType());
    $this->assertEquals( 'Post Title 3' , $child2E->getTitle());    
          
  }

  function test_saveEntity()
  {
    // no recursive saving supported
  }  
}

