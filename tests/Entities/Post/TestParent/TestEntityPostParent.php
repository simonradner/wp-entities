<?php
/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
class TestEntityPostParent extends AbstractEntityPostTest {
  
  const POST_TYPE1 = 'testposttype';

  const POST_TYPE2 = 'testposttype';
  
  protected $postId1;

  protected $postId2;

  protected $categories;

  protected $tags;

  protected $category;

  protected $tag;
  
  function setUp()
  {
    parent::setUp();
  }

  function test_setEntity()
  {
    // set by entity
    $parentEntity = ParentEntity::make($this->postId1);
    $childEntity = ParentEntity::make($this->postId2);

    $parentEntity->setParent($childEntity);
    $e = $parentEntity->getParent();
    $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $e);
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $e->getInstance());
    $this->assertEquals( static::POST_TYPE2 , $e->getType());
    $this->assertEquals( 'Post Title 2' , $e->getTitle());    

    // set by ID
    
    // the following would only work if an entity and repostory is set via annotations
/*
    $parentEntity = ParentEntity::make($this->postId1);

    $parentEntity->setParent($this->postId2);
    $e = $parentEntity->getParent();
        
    $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $e);
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $e->getInstance());
    $this->assertEquals( static::POST_TYPE2 , $e->getType());
    $this->assertEquals( 'Post Title 2' , $e->getTitle()); 
*/
  }

  function test_fetchEntity()
  {
    $postId = $this->factory->post->create([
      'post_title' => 'Post Title', 
      'post_type' => static::POST_TYPE1,
      'post_parent' => $this->postId2
    ]);
    $parentEntity = ParentEntity::make($postId);
    $e = $parentEntity->getParent();
        
    $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $e);
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $e->getInstance());
    $this->assertEquals( static::POST_TYPE2 , $e->getType());
    $this->assertEquals( 'Post Title 2' , $e->getTitle());      
  }

  function test_saveEntity()
  {

    $parentEntity = ParentEntity::make($this->postId1);
    $childEntity = ParentEntity::make($this->postId2);

    $parentEntity->setParent($childEntity);
    $parentEntity->save();
    
    $post = get_post($this->postId1);        
    $this->assertEquals( $this->postId2, $post->post_parent);
  }  
}

