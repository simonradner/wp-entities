<?php

use Sef\WpEntities\Entities\Post;
use Sef\WpEntities\Annotation\PostOptions as Options;
use Doctrine\Common\Collections\ArrayCollection;

/**
   * @Options(
   *  post_type="testposttype"
   * )
   */
class ParentEntity extends Post {

  protected $id;

/**
   * @Options(
   * )
  */
  protected $parent;

  protected function __construct( $post = null ) 
  {
    parent::__construct($post);
  }

  public static function configOptions()
  {
    return [];
  }

}

