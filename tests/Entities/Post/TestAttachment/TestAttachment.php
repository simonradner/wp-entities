<?php
use Sef\WpEntities\Entities\Attachment;
/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
class TestAttachment extends AbstractEntityPostTest {
  
  const POST_TYPE1 = 'attachment';

  const POST_TYPE2 = null;
  
  protected $postId1;
  
  function setUp()
  {    
    parent::setUp();
    
    $this->postId1 = $this->createAttachment();    
  }

  function test_setEntity()
  {
    // set by entity
    $e = Attachment::make($this->postId1);
    
//      echo '<pre>'; print_r($e->getAttachmentUrl()); echo '</pre>';
/*

    $parentEntity->setParent($childEntity);
    $e = $parentEntity->getParent();
    $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $e);
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $e->getInstance());
    $this->assertEquals( static::POST_TYPE2 , $e->getType());
    $this->assertEquals( 'Post Title 2' , $e->getTitle());    
*/

  }

  function test_fetchEntity()
  {
   
  }

  function test_saveEntity()
  {

  } 
  
  function createAttachment()
  {
      $filename = ( dirname(__FILE__). '/image.jpeg' );
    $contents = file_get_contents($filename);
    
    $upload = wp_upload_bits(basename($filename), null, $contents);

    $wp_filetype = wp_check_filetype( basename( $upload['file'] ), null );
    $wp_upload_dir = wp_upload_dir();

    $attachment = array(
        'guid' => $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $upload['file'] ),
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => preg_replace('/\.[^.]+$/', '', basename( $upload['file'] )),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    
    $attach_id = wp_insert_attachment( $attachment, $upload['file'], 0 );

    require_once(ABSPATH . 'wp-admin/includes/image.php');

    $attach_data = wp_generate_attachment_metadata( $attach_id, $upload['file'] );
    wp_update_attachment_metadata( $attach_id, $attach_data );
    return $attach_id;
  }
}

