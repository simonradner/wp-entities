<?php
/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
abstract class AbstractEntityPostTest extends WP_UnitTestCase {
  
  const POST_TYPE1 = 'post';

  const POST_TYPE2 = null;
  
  protected $postId1;

  protected $postId2;

  protected $categories;

  protected $tags;

  protected $category;

  protected $tag;
  
  function setUp()
  {
    parent::setUp();
    
    if( static::POST_TYPE1 &&! post_type_exists(static::POST_TYPE1))
    {
      register_post_type( static::POST_TYPE1 );
    }
    if( static::POST_TYPE2 &&! post_type_exists(static::POST_TYPE2))
    {
      register_post_type( static::POST_TYPE2 );
    }
    
    $this->postId1 = $this->factory->post->create([
      'post_title' => 'Post Title', 
      'post_type' => static::POST_TYPE1,
    ]);

    $this->postId2 = $this->factory->post->create([
      'post_title' => 'Post Title 2', 
      'post_type' => static::POST_TYPE2,
    ]);
    
    // create categories and tags 
    $this->categories = $this->factory->term->create_many(3, ['taxonomy' => 'category']);
    $this->tags = $this->factory->term->create_many(3, ['taxonomy' => 'post_tag']);

    $this->category = $this->factory->term->create(['taxonomy' => 'category']);
    $this->tag = $this->factory->term->create(['taxonomy' => 'post_tag']);
  }

  function test_setEntity()
  {
    $this->assertEquals( 'test_setEntity not implemented' , null);
  }

  function test_fetchEntity()
  {
    $this->assertEquals( 'test_fetchEntity not implemented' , null);
  }

  function test_saveEntity()
  {
    $this->assertEquals( 'test_saveEntity not implemented' , null);
  }  
}

