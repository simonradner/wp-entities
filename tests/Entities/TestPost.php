<?php
use Sef\WpEntities\Entities\Post;

/**
 * Class SampleTest
 *
 * @package 
 */

/**
 * Sample test case.
 */
class TestPost extends WP_UnitTestCase {
  
  
  private $postId;
  private $postIdNoCategories;
  
  function setUp()
  {
    parent::setUp();
    $this->postId = $this->factory->post->create([
      'post_title' => 'My Blog Post', 
      'post_type' => 'post',
    ]);

    $this->postIdNoCategories = $this->factory->post->create([
      'post_title' => 'My Blog Post 2', 
      'post_type' => 'post',
    ]);
    
    
    // add categories and tags to post
    $category_ids = $this->factory->term->create_many(3, ['taxonomy' => 'category']);
    $tag_ids = $this->factory->term->create_many(4, ['taxonomy' => 'post_tag']);
    wp_set_object_terms( $this->postId, $category_ids, 'category', false );
    wp_set_object_terms( $this->postId, $tag_ids, 'post_tag', false );

  }

  function test_initPost()
  {

    // empty post
    $e = Post::make();   
    $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $e);
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $e->getInstance());
    $this->assertEquals( 'post' , $e->getType());
    $this->assertNull( $e->getId());
    $this->assertTrue( $e->isNew());
    
    // setting and getting
    $e->setStatus('publish');
    $this->assertEquals( 'publish' , $e->getStatus());
   

    // entity from db, getting
    $e = Post::make($this->postId);
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $e->getInstance());
    $this->assertEquals( $this->postId , $e->getId());
    $this->assertEquals( 'My Blog Post' , $e->getRawTitle());
    $this->assertEquals( get_the_title($this->postId) , $e->getTitle());
    
    // construct with WP_Post
    $e = Post::make(get_post($this->postId));
    $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\PostEntityInterface', $e->getInstance());
    
    // saving post
    $e = Post::make($this->postId);
    $e->setTitle('My Changed Blog Post');
    $e->save();
    $saved_post = get_post($this->postId);
    $this->assertEquals( 'My Changed Blog Post' , $saved_post->post_title);
   
    $e = Post::make($this->postId);
    $this->assertEquals( 'My Changed Blog Post' , $e->getRawTitle());
  }

  function test_attachedTerms()
  {

    $e = Post::make($this->postId);   
    
    // get categories
    $catEs = $e->getCategories(); 
    $this->assertInstanceOf( 'Doctrine\\Common\\Collections\\ArrayCollection', $catEs);
    $this->assertEquals( 3, $catEs->count());
    
    foreach($catEs as $catE)
    {
      $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $catE);
      $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\TermEntityInterface', $catE->getInstance());
      $this->assertEquals( 'category', $catE->getTaxonomy());
      $this->assertStringMatchesFormat( '%s', $catE->getName());
      $this->assertStringMatchesFormat( '%s', $catE->getSlug());
      $this->assertStringMatchesFormat( '%s', $catE->getDescription());
      $this->assertEquals( 0, $catE->getParent());
    }

    // get tags
    $catEs = $e->getTags(); 
    $this->assertInstanceOf( 'Doctrine\\Common\\Collections\\ArrayCollection', $catEs);
    $this->assertEquals( 4, $catEs->count());
    
    foreach($catEs as $catE)
    {
      $this->assertInstanceOf( 'Sef\\WpEntities\\Base\\EntityBag', $catE);
      $this->assertInstanceOf( 'Sef\\WpEntities\\Interfaces\\TermEntityInterface', $catE->getInstance());
      $this->assertEquals( 'post_tag', $catE->getTaxonomy());
    } 
    
    // a post with no categories
    $e = Post::make($this->postIdNoCategories);
    $catEs = $e->getCategories(); 
    
    // ech post has the category uncategorized, when no other cats has been assigned
    $uncatigorizedCat = $catEs->first();
    $this->assertEquals( 1, $uncatigorizedCat->getId());
    $this->assertEquals( 'uncategorized', $uncatigorizedCat->getSlug());
    
  }

  function test_clearTerms()
  {
    


    $e = Post::make($this->postId);   
    
    // clear categories
    $e->setCategories(new Doctrine\Common\Collections\ArrayCollection); 
    $e->save(); 

    $e = Post::make($this->postId);   
    $catEs = $e->getCategories(); 
    $this->assertInstanceOf( 'Doctrine\\Common\\Collections\\ArrayCollection', $catEs);
    $this->assertEquals( 0, $catEs->count());    // this is uncategorized
//     $uncatigorizedCat = $catEs->first();
//     $this->assertEquals( 1, $uncatigorizedCat->getId());
    

    // clear tags
    $e->setTags(new Doctrine\Common\Collections\ArrayCollection); 
    $e->save(); 

    $e = Post::make($this->postId);   
    $catEs = $e->getTags(); 

    $this->assertInstanceOf( 'Doctrine\\Common\\Collections\\ArrayCollection', $catEs);
    $this->assertEquals( 0, $catEs->count());    
    
        
  
  }
}

